<style>

    .item-panel {
        text-align: left; 
        box-shadow: 0 1px 3px 0 #6c757d; 
        /*border-radius: 8px;*/
        margin-left: 10px;
        margin-right: 10px;
        margin-bottom: 10px;
        /* height: 600px; */
        /* overflow-y: auto; */
        /* width: 330px; */
    }
    
    .item-pict {
        text-align: center; 
        font-size: 60px; 
        background-color: white; 
        /*border-radius: 8px;*/
        margin-bottom: 10px; 
        height: 200px;
    }

    .div-pict {
        overflow-y: auto; 
        max-height: 500px;
    }
    
    .item-img {
        border-radius: 8px; 
        margin-bottom: 10px;
        height: 100%; 
        cursor: pointer;
        transition: 0.3s;
    }
    
    .item-img:hover {opacity: 0.7;}
    
    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }
    
    /* Modal Content (image) */
    .modal-content {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
    }
    
    /* Caption of Modal Image */
    #caption {
      margin: auto;
      display: block;
      width: 80%;
      max-width: 700px;
      text-align: center;
      color: #ccc;
      padding: 10px 0;
      height: 150px;
    }
    
    /* Add Animation */
    .modal-content, #caption {  
      -webkit-animation-name: zoom;
      -webkit-animation-duration: 0.6s;
      animation-name: zoom;
      animation-duration: 0.6s;
    }
    
    @-webkit-keyframes zoom {
      from {-webkit-transform:scale(0)} 
      to {-webkit-transform:scale(1)}
    }
    
    @keyframes zoom {
      from {transform:scale(0)} 
      to {transform:scale(1)}
    }
    
    /* The Close Button */
    .closeModal {
      position: absolute;
      top: 15px;
      right: 35px;
      color: #f1f1f1;
      font-size: 40px;
      font-weight: bold;
      transition: 0.3s;
    }
    
    .closeModal:hover,
    .closeModal:focus {
      color: #bbb;
      text-decoration: none;
      cursor: pointer;
    }
    
    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }

        .items {
            /* width: 300px;
            height: 500px; */
        }

        .div-pict {
            max-height: 400px;
        }
    }
    
    </style>

<div class="main-card mb-3 mt-3 card" style="border-radius: 10px;">
    <div class="card-header" style="background-color: #fc6231; border-radius: 10px; color: white;"> 
        <div class="row" style="width: 100%;">  
            <div class="col-md-10 mb-2 mt-2">
                <span style="">Promosi Terbaru</span>
            </div>
            @if(($role == 1 || $role == 3) && $promo_btn == 1)
            <div class="col-md-2" style="text-align: center; border-radius: 30px !important;">
                <a href="{{ route('addPromoPage') }}" class="btn btn-warning" style="border-radius: 30px;"><i class="fa fa-plus-circle mr-1"></i>  Buat Promosi</a>
            </div>
            @endif
        </div>
    </div>
    <div class="card-body">
        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" style="overflow-y: auto;">
                @php
                    $tot = count($promosis);
                    $n = 0;
                    $j = 1;
                @endphp

                @for($i = 0; $i < $tot; $i++)

                    @php
                        $isactive = ($i == 0) ? ' active' : '';                        
                    @endphp
                
                    @if($j == 1)
                        <div class="carousel-item{{ $isactive }}"  data-interval="8000">
                            <div class='row pb-3 justify-content-md-center'>
                    @endif
                            <div class='col-md-4'>
                                <div class='panel panel-default item-panel'>
                                    <div style='padding: 10px;'>
                                        <div class="row">
                                            <div class='col-md-10'>
                                                <span><img src='{{ asset('assets/images/users/thumb/'.$promosis[$i]->penjual->foto) }}' height="50px;"></span>
                                                <a href="listShopPenjual/{{ $promosis[$i]->penjual->id }}">
                                                    <span class="ml-2"><strong>{{ $promosis[$i]->penjual->name }}</strong></span>
                                                </a>
                                            </div>
                                            <div class='col-md-4'>

                                                @if(Auth::user()->id == $promosis[$i]->penjual->id)
                                                    <a href="editPromoPage/{{ $promosis[$i]->id }}" class="float-right btn btn-warning"><i class="fa fa-edit"></i> Ubah</a>
                                                @endif
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class='col-md-12'>
                                                <font size="2px" color="grey">{{ date('d-m-Y', strtotime($promosis[$i]->created_at)) }}</font>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-pict" style="">
                                        <div class='item-pict'>
                                            <img src='{{ asset('assets/images/promosis/thumb/'.$promosis[$i]->image) }}' class="img-fluid item-img" onclick="klik(this);">
                                        </div>
                                        <div style='padding: 10px;'>
                                            {!!html_entity_decode(urldecode($promosis[$i]->deskripsi))!!}
                                        </div> 
                                    </div> 
                                </div>
                            </div>

                    @if($j++ == 3)
                        @php
                            $j = 1;
                        @endphp
                                </div>
                        </div>
                    @elseif($i + 1 == $tot)
                            </div>
                        </div>
                    @endif
                @endfor
                
                <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev" style="width: 50px;">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next" style="width: 50px;">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">
    <span id="closeBtn" class="closeModal">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>


  <script>

    $('#myModal').appendTo("body");

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nName: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    var modal = document.getElementById("myModal");

    function klik(img) {
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

    }

    $('#closeBtn').click(function() {
        modal.style.display = "none";
    });

</script>