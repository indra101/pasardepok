@extends('layouts.app')

@section('content')

@include('notif')

<style>

.card-kategori:hover {
  bottom: 10px;
}

.kategori {
    width: 150px; 
    height: 130px;
    margin-bottom: 15px;
    margin-right: 10px;
}

.carousel-item {
    height: 500px;
}

@media only screen and (max-width: 1024px){
    .kategori {
        width: 100px; 
        height: 100px;
        margin-bottom: 0px;
    }

    .text-kat {
        font-size: smaller;
    }

    .card-body {
        padding: 0;
    }

}

@media only screen and (max-width: 700px){
    .logo-src {
        display: none;
    }

    .carousel-item {
        height: 220px;
    }
}

</style>

@php
$role = Auth::user()->role_id;
@endphp

@if (session('status'))
    <div class="alert alert-success fade show">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times" style="font-size: medium;"></span></a>
        <p>{{ session('status') }}</p>
    </div>
@endif

<div id="carouselExample" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" style="border-radius: 10px;">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('assets/images/slides/carousel5.jpg') }}" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('assets/images/slides/carousel1.jpg') }}" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('assets/images/slides/carousel2.jpeg') }}"  alt="Third slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('assets/images/slides/carousel3.jpeg') }}"  alt="Fourth slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('assets/images/slides/carousel4.jpg') }}"  alt="Fifth slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

{{-- <div class="main-card mb-3 card" style="border-radius: 25px;">
    <div class="card-header" style="background-color: #ffcb4e; border-radius: 25px;">
        <span style="">Pilih Kategori Produk</span>
    </div>
</div> --}}

<div class="row justify-content-center pt-4">

    
    
    @foreach($kategoris as $kt)

    <div class="kategori" style="">
        <div class="main-card mb-3 card text-center card-kategori" style="border-radius: 30px;">
            <div class="card-body">
                <a href="{{route('listShop', ['kategori' => $kt->id])}}" class="btn">
                    <div>
                        {{-- <i class="fa fa-{{ $kt->icon }}" style="font-size: 50px;"></i> --}}
                        <img src='{{ asset('assets/images/icons/'.$kt->icon) }}' class="" height="60px">
                    </div>
                    <div class="text-kat">
                        <strong>{{ $kt->nama }}</strong>
                    </div>
                </a>
            </div>
        </div>
    </div>

    @endforeach

</div>
            
@include('promosi')

@endsection
