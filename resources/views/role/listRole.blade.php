@extends('layouts.app')

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Role
                <div class="page-title-subheading">Badges and labels are used to offer extra small pieces of info for your content.
                </div>
            </div>
        </div>
        
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    <div class="card-body">
        <a href="{{route('addRole.page')}}" class="btn btn-xs btn-alternate"><i class="fa fa-plus-circle"></i> Tambah Role</a>    
        <br/><br/>
        <div class="overflow-auto">
            <table class="table table-striped table-hover" id="roles-table" style="background-color: white;">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>

        <script>
        $(function() {

            var table = $('#roles-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('getRoles.datas') !!}',
                columns: [
                    { data: 'no', name: 'no' , searchable: true},
                    { data: 'name', name: 'name' , searchable: true},                
                    { data: null, name: 'action', render: function ( data, type, row ) {
                        return '<a href="editRolePage/' + data.id + '" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a> ' +
                                '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger" onclick=\'confirmDel(' + data.id + ',"' + data.name + '")\' title="Delete"><i class="fa fa-trash"></i></a>' +
                                '<form class="delete" action="delRole/' + data.id + '" method="get">' +
                                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' +
                                '</form></div>';
                    } },
                ],
                columnDefs: [{
                    "defaultContent": "-",
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }]
            });
            
        });

        function confirmDel(id, name) {
            var txt;
            var r = confirm("Yakin akan menghapus data? \n\nRole: " + name);
            if (r == true) {
                txt = "You pressed OK!"; 
                $('#delButton'+id).click();
            } else {
                txt = "You pressed Cancel!";
            }
        }

        </script>
        @stack('scripts')
    </div>
</div>

@endsection
