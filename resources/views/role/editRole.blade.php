@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Edit Role
                <div class="page-title-subheading">Edit role.
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('listRole') }}'>Daftar Role</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Edit Role</span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.Role') }}">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" name="id" value="{{ $roles->id }}">                        
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" maxlength="100" value="{{ $roles->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">  

function konfirmasi() {
    if(confirm('Data sudah benar?') ){
        return true;
    } else {
        return false;
    }
}

</script>

@endsection
