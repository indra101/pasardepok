@extends('layouts.app')

@section('content')

<style>

@media only screen and (max-width: 700px){
    .btn_simpan {
        width: 100%;
    }
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Buat Promosi
                <div class="page-title-subheading">Buat Promosi Terbaru.
                </div>
            </div>
        </div>
         
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('home') }}'>Beranda</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Buat Promosi</span>
        </div>

        <div class="card-body">
            
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ ($mode == 'add') ? route('addPromosi') : route('editPromosi') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" name="id" value="{{ (empty($promosi)) ? '' : $promosi->id }}">

                        <div class="form-group">
                            <div class="col-md-6">
                                <span style="color: red; font-style: italic;">*Anda dapat membuat Promosi maksimal 1x sehari. Promosi akan ditayangkan selama 3 hari.</span>
                                <label for="phone" class="mt-2 control-label">Gambar Promosi <br>(mohon pilih dari daftar image/foto, belum mendukung foto langsung dari kamera HP)</label>
                                <img id="image_preview_container" class="img-fluid" src="{{ (empty($promosi)) ? asset('assets/images/promosis/no-image.png') : asset('assets/images/promosis/'.$promosi->image) }}" height="300px" style="cursor: pointer;">
                            </div>
                            <div class="col-md-4 mt-4">
                                <input type="file" name="image" placeholder="Choose image" id="image" {{ ($mode == 'add') ? 'required' : '' }}>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>

                            <div class="col-md-8">
                                <div id="deskripsi_div">
                                    
                                </div>
                                <textarea id="deskripsi" class="form-control" name="deskripsi" rows="10" value="" style="" required autofocus>{{ (empty($promosi)) ? '' : urldecode($promosi->deskripsi) }}</textarea>

                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('deskripsi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button id="btnSimpan" class="btn btn-primary btn_simpan">
                                    Simpan
                                </button>
                                <input type="submit" id="btnSubmit" style="display:none;"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">  

$(function(){

    $('#btnSimpan').click(function() {
        if(confirm('Data sudah benar?') ){
            tinyMCE.triggerSave();
            $('#btnSubmit').click();

            return true;
        } else {
            return false;
        }
    });
});

$('#image_preview_container').click(function() {
    $('#image').click();
});

function konfirmasi() {
    
}

function numFormat() {
    var harga = $("#hargaf").val().replace(/[.,\s]/g,'');
    harga = harga.replace(/[^0-9]/, '');
    harga = harga.replace(/\b0+/g, '');
    $("#harga").val(harga);        
    harga = harga.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    $("#hargaf").val(harga);
}

var fnum = 1;
var num = 0;

function klik() {
    if(num <= 5) {
        var html = '<div id="file' + fnum + '" style="padding-top: 10px;"><img id="image_preview_container' + fnum + '" class="" src="#" width="55px" height="55px" style="margin: auto;"><span data-no="' + fnum + '" style="cursor: pointer;" class="metismenu-icon pe-7s-trash" title="Hapus"></span><input type="file" id="file_upload' + fnum + '" name="file_upload[]" data-no="' + fnum + '" style="padding-left: 10px; display: inline-block; border: 0; width: 500px" onchange="cek_file(this);" multiple></div>';
        $("#file_div").append(html);
        $("#file_upload"+fnum).click();
        fnum++;
        num++;
    }
} 

$(document).on('click', '.pe-7s-trash', function() {
    let no = $(this).data('no');
    $("#file"+no).remove();
    num--;
});

$(document).on('click', '.pe-7s-close-circle', function() {
    let no = $(this).data('no');
    $("#img"+no).remove();
});

function cek_file(a) {        
    var size = Math.round(((a.files[0].size/1024/1024) + 0.00001) * 100) / 100;
    var name = a.files[0].name;
    var name_arr = name.split(".");
    var type = name_arr[name_arr.length - 1];
    var allowed = ["jpg", "jpeg", "png", "webp", "JPG", "JPEG", "PNG", "WEBP"];
    var pesan_size = "\nMohon masukkan file dengan ukuran max. 1 MB";
    var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
    var cek_size = size >= 1; // max file size 500 MB
    var cek_tipe = allowed.indexOf(type) == -1;

    if(cek_size || cek_tipe) {
        var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
        if(cek_size)
            pesan += pesan_size;

        if(cek_tipe)
            pesan += pesan_tipe;

        alert(pesan);
        a.value = '';
    } else {

        let n = $(a).data('no');
        let reader = new FileReader();
        reader.onload = (function(e) {
            $('#image_preview_container' + n).attr('src', e.target.result); 
        });
        reader.readAsDataURL(a.files[0]);

    }

}

$(function() {
    numFormat();
});

$('#image').change(function(){
            
    let reader = new FileReader();
    reader.onload = (e) => { 
    $('#image_preview_container').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 

});

</script>

@endsection

