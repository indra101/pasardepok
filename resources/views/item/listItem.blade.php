@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Produk
                <div class="page-title-subheading">Daftar produk barang dan jasa.
                </div>
            </div>
        </div>
          
    </div>
</div>


@include('notif')

<div class="main-card mb-3 card">
    <div class="card-body">

    <a href="{{route('addItem.page')}}" class="btn btn-xs btn-alternate"><i class="fa fa-plus-circle"></i> Tambah Barang</a>
    {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
    <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
    <br/><br/>
    {{-- <div class="overflow-auto"> --}}
    <div class="overflow-auto">
        <table class="table table-striped table-hover" id="items-table" style="background-color: white;">
            <thead>
                <tr>
                    <th style="max-width: 20px;">No</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Gambar</th>
                    <th style="max-width: 150px;">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

    <script>
    $(function() {

        var table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('getItems.datas') !!}',
                data: {
                    "id_seller": "{{ Auth::user()->id }}"
                },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: 'deskripsi', name: 'deskripsi' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'stok', name: 'stok' , searchable: true}, 
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    return "<img src='{{ asset('assets/images/items/') }}/" + data.foto + "'  height='100px'>";
                } },               
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<a href="editItemPage/' + data.id + '" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a>' +  
                    '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger" style="margin-left: 5px;" onclick=\'confirmDel(' + data.id + ',"' + data.nama + '")\' title="Delete"><i class="fa fa-trash"></i></a>' +
                    '<form class="delete" action="delItem/' + data.id + '" method="get">' + 
                    '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                    '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                    '</form></div>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

    
</div>
@endsection
