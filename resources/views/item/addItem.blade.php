@extends('layouts.app')

@section('content')

<style>

@media only screen and (max-width: 700px){
    .btn_simpan {
        width: 100%;
    }
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Tambah Produk
                <div class="page-title-subheading">Tambah produk baru.
                </div>
            </div>
        </div>
          
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">

        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('profilToko') }}'>Profil Toko</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Tambah Produk</span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.Item') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('jenis') ? ' has-error' : '' }}">
                            <label for="jenis" class="col-md-4 control-label">Jenis<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <select id="jenis" class="form-control" name="jenis" required>
                                    <option value="">Pilih Jenis Produk</option>
                                    <option value="Barang">Barang</option>
                                    <option value="Jasa">Jasa</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
                            <label for="kategori" class="col-md-4 control-label">Kategori<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <select id="kategori" class="form-control" name="kategori" required>
                                    <option value="">Pilih Kategori</option>
                                    @foreach($kategoris as $kategori)
                                        <option value="{{$kategori->id}}">{{$kategori->nama}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('kategori'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kategori') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label for="nama" class="col-md-4 control-label">Nama<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" maxlength="100" value="{{ old('nama') }}" required>

                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                            <label for="harga" class="col-md-4 control-label">Harga (Rp.)<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="hargaf" type="text" class="form-control" name="hargaf" value="{{ old('harga') }}" onkeyup="numFormat()" required>
                                <input type="hidden" name="harga" id="harga">

                                @if ($errors->has('hargaf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hargaf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                            <label for="satuan" class="col-md-4 control-label">Satuan<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <select id="satuan" class="form-control" name="satuan" required>
                                    <option value="">Pilih Satuan</option>
                                    @foreach($satuan as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('satuan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('satuan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Stok<font color="red"> *</font></label>

                            <div class="col-md-6">
                                {{ Form::radio('stok_stat', 'ready', 'true') }} Ready
                                <input type="number" id="stok" name="stok" step="1" min="0" width="20px" style="width: 50px; padding-left: 3px;" value="{{ (old('stok')) ? old('stok') : 0 }}" >

                                {{ Form::radio('stok_stat', 'pre_order', false, array('class' => 'ml-4')) }} Pre-Order

                                @if ($errors->has('stok'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stok') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <textarea id="deskripsi" class="form-control" name="deskripsi" value="{{ old('deskripsi') }}" maxlength="1000" required></textarea>

                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('deskripsi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
                            <label for="gambar" class="col-md-4 control-label">Gambar<br>
                                <span style="font-size: smaller; color: grey;">(maksimal 5 gambar, mohon pilih dari daftar image/foto, belum mendukung foto langsung dari kamera HP)</span>
                            </label>

                            <div class="col-md-10" style="padding-bottom: 10px">
                                <a class= "btn btn-warning" href="javascript:void(0)" onclick="klik()"><i class="fa fa-plus-circle mr-1"></i>Tambah Gambar</a>
                                <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                                <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 5 MB</i></span></div>
                                <div id="file_div"></div>

                                @if ($errors->has('file_upload'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file_upload') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn_simpan" onclick="return konfirmasi();">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">  

    function konfirmasi() {
        numFormat();

        if(confirm('Data sudah benar?') ){
            tinyMCE.triggerSave();
            return true;
        } else {
            return false;
        }
    }

    function numFormat() {
        var harga = $("#hargaf").val().replace(/[.,\s]/g,'');
        harga = harga.replace(/[^0-9]/, '');
        harga = harga.replace(/\b0+/g, '');
        $("#harga").val(harga);        
        harga = harga.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        
        $("#hargaf").val(harga);
    }

    var fnum = 1;
    var num = 1;

    function klik() {
        if(num <= 5) {
            var html = '<div id="file' + fnum + '" style="padding-top: 10px;"><img id="image_preview_container' + fnum + '" class="" src="' + "{{ asset('assets/images/items/no-image.png') }}" + '" height="100px" style="margin: auto;"><span data-no="' + fnum + '" style="cursor: pointer; font-weight: bold; font-size: large;" class="metismenu-icon pe-7s-close-circle ml-2" title="Hapus"></span><input type="file" id="file_upload' + fnum + '" name="file_upload[]" data-no="' + fnum + '" style="padding-left: 10px; display: inline-block; border: 0; width: 500px" onchange="cek_file(this);" multiple></div>';
            $("#file_div").append(html);
            $("#file_upload"+fnum).click();
            fnum++;
            num++;
        }
    } 

    $(document).on('click', '.pe-7s-close-circle', function() {
        let no = $(this).data('no');
        $("#file"+no).remove();
        num--;
    });

    function cek_file(a) {        
        var size = Math.round(((a.files[0].size/1024/1024) + 0.00001) * 100) / 100;
        var name = a.files[0].name;
        var name_arr = name.split(".");
        var type = name_arr[name_arr.length - 1];
        var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
        var pesan_size = "\nMohon masukkan file dengan ukuran max. 5 MB";
        var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
        var cek_size = size >= 5; // max file size 5 MB
        var cek_tipe = allowed.indexOf(type) == -1;

        if(cek_size || cek_tipe) {
            var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
            if(cek_size)
                pesan += pesan_size;

            if(cek_tipe)
                pesan += pesan_tipe;

            alert(pesan);
            a.value = '';
        } else {

            let n = $(a).data('no');
            let reader = new FileReader();
            reader.onload = (function(e) {
                $('#image_preview_container' + n).attr('src', e.target.result); 
            });
            reader.readAsDataURL(a.files[0]);

        }

    }

    </script>
</div>

@endsection

