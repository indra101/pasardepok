@extends('layouts.app')

@section('content')

<style>

.dataTables_filter{
    display: none;
}

.items {
    text-align: left; 
    box-shadow: 0 1px 6px 0 rgba(0,0,0,0.1); 
    border-radius: 8px;
    margin-left: 10px;
    margin-right: 10px;
    width: 230px;
}

.item-pict {
    text-align: center; 
    font-size: 60px; 
    background-color: white; 
    border-radius: 8px; 
    margin-bottom: 10px; 
    height: 200px;
}

@media only screen and (max-width: 700px){
    .btn_pesan {
        width: 100%;
    }

    .img-penjual {
        width: 80px;
    }

    .div-img-penjual {
        margin-bottom: 10px;
    }
}

</style>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@else
    @if ($message = Session::get('fail'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
            <p>{{ $message }}</p>
        </div>
    @endif
@endif

<div class="main-card mb-3 card">

    <div class="card-header">
        <span style="font-size: smaller;"><a href='{{ route('listShop') }}'>Daftar Katalog</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> <a href="{{route('listShop', ['kategori' => $item->id_kategori])}}">{{ $item->nama_kategori }}</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> {{ $item->nama }}</span>
    </div>

    <div class="card-body">

        @if(Auth::user()->id == $item->id_seller || Auth::user()->role_id == 1)
        <div class="row mb-2">
            <div class="col-md-12">
                <div class="float-right">
                    <a href="../editItemPage/{{ $item->id }}" class="btn btn-xs btn-warning" title="Edit" style="padding: .2rem .4rem;"><i class="fa fa-edit"></i> Ubah</a>
                    <a href="../delItem/{{ $item->id }}" class="btn btn-xs btn-danger" style="margin-left: 5px; padding: .2rem .4rem;" onclick="confirmDel()" title="Delete"><i class="fa fa-trash"></i> Hapus</a>
                </div>
            </div>
        </div>

        @endif

        <div class="row">
            <div class="col-md-4">
                
                <div id="item_fotos" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @php
                        $n = 0
                        @endphp
                        @foreach($item->fotos as $foto)
                            @if(!empty($foto))
                                @if($n == 0)
                                <li id="slide{{ $n }}" data-target="#item_fotos" data-slide-to="{{ $n++ }}" class="active"></li>
                                @else
                                <li id="slide{{ $n }}" data-target="#item_fotos" data-slide-to="{{ $n++ }}"></li>
                                @endif
                            @endif
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @php
                        $i = 0
                        @endphp
                        @foreach($item->fotos as $foto)
                            @if(!empty($foto))
                                @if($i++ == 0)
                                <div class="carousel-item active">
                                @else
                                <div class="carousel-item">
                                @endif
                                    <div style="max-width: 350px; max-height: 350px;">
                                        <img class="img-fluid d-block" src="{{ asset('assets/images/items/thumb_view/') }}/{{ $foto }}" alt="slide" style="margin: auto;">
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#item_fotos" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#item_fotos" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="mt-2 mb-4">
                    @php
                    $j = 0
                    @endphp
                    @foreach($item->fotos as $foto)
                        @if(!empty($foto))
                            <a href="#" data-target="#item_fotos" data-slide-to="{{ $j++ }}"> <img src="{{ asset('assets/images/items/thumb/') }}/{{ $foto }}" alt="slide" width="55px" height="55px" style="margin-right: 7px; margin-top: 5px;"></a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 pt-2 pb-2" style="border-bottom: 1px solid rgb(214, 214, 214);">
                <h5><b>{{ $item->nama }}</b></h5>

                @if($item->stok_stat == 'pre_order')
                    <div style="font-weight: bold;">Pre-Order</div>
                @else
                    @if($item->stok <= 0)
                        <div style="font-weight: bold; color: red;">Stok Habis</div>
                    @else
                        <div style="font-weight: bold;">Stok tersedia {{ $item->stok }}</div>
                    @endif
                @endif

                <div style="margin-top: 20px;"><span style=" color: red; font-weight: bold; font-size: xx-large;">Rp{{ number_format($item->harga) }}</span> / {{ $item->satuan }}</div>

                @if($item->stok > 0 || $item->stok_stat == 'pre_order')
                    <div class="mt-4 mb-4">
                        <button type="submit" class="btn btn-success btn_pesan" data-toggle="modal" data-target="#modal_shop" onclick="pilih({{ $item->id }}, '', '{{ $item->nama }}', {{ $item->harga }}, {{ $item->stok }});" >
                            <b>Pesan Sekarang</b>
                        </button>
                    </div>
                @endif
            </div>
            <div class="col-md-4 pt-2 pb-2" style="border-left: 1px solid rgb(214, 214, 214); border-bottom: 1px solid rgb(214, 214, 214);">
                <div class="row">
                    <div class="col-md-8 mb-2">
                        <h6><b>INFORMASI PENJUAL</b></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 pr-0 div-img-penjual"><img class="img-fluid rounded-circle img-penjual" src="{{ asset('assets/images/users/thumb/'.$item->penjual->foto) }}" alt="" style=""></div>
                    <div class="col-md-8">
                        <div>
                            <a href="../listShopPenjual/{{$item->penjual->id}}"><b>{{ $item->penjual->nama_toko }}</b></a>
                        </div>
                        <div>
                            {{ $item->penjual->name }}
                        </div>
                        <div>
                            {{ $item->penjual->get_kelurahan->nama }}, {{ $item->penjual->get_kelurahan->kecamatan->nama }}
                        </div>
                        <div>
                            <img width="30" src="{{ asset('assets/images/wa.png') }}"><a class="ml-2" href="https://wa.me/62{{ $item->penjual->hp }}">{{ $item->penjual->hp }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="main-card mb-3 card">

    <div class="card-header">
        <h6><b>Informasi Produk</b></h6>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <span style="font-size: medium;"><b>Spesifikasi</b></span>
                <div class="row mt-2">
                    <span style="width: 100px; padding-left: 15px;">
                        Jenis
                    </span>
                    <span>
                        :  <span class="ml-2">{{ $item->jenis }}</span>
                    </span>
                </div>
                <div class="row">
                    <span style="width: 100px; padding-left: 15px;">
                        Kategori
                    </span>
                    <span>
                        :  <span class="ml-2">{{ $item->nama_kategori }}</span>
                    </span>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <span style="font-size: medium;"><b>Deskripsi</b></span>
                <div class="mt-2">
                    {!!html_entity_decode(urldecode($item->deskripsi))!!}
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="modal fade" id="modal_shop" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Pesan Produk</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('beli.Item') }}">
            {{ csrf_field() }}
        
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class='item-pict'><img src='{{ asset('assets/images/items/') }}/{{ $item->fotos[0] }}' style="width: 150px;"></div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-xs-4" ><b>{{ $item->nama }}</b></label>
                            <div>
                                <span style=" color: red; font-weight: bold; font-size: large;">Rp{{ number_format($item->harga) }}</span> / {{ $item->satuan }}
                            </div>
                            <div class="pt-2">
                                <span style="margin-right: 10px;">Jumlah</span><input type="number" id="jumlah" name="jumlah" value="1" step="1" min="1" max="{{ ($item->stok_stat == 'pre_order') ? '' : $item->stok }}" width="20px" style="width: 50px; padding-left: 3px;">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                        <label for="catatan" class="control-label">Catatan Tambahan</label>
                        {{ Form::text('catatan', '', array('class' => 'form-control')) }}
                        {{-- <input id="catatan" type="text" class="form-control" name="catatan"> --}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mt-2"><span>Tekan Tombol <b>Pesan</b> untuk melanjutkan pesanan</span></div>
                    </div>

                    <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="id_items" id="id_items" value="{{ $item->id }}">

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnPesan" class="btn btn-primary" type="submit" value="Pesan"/>
                    <button id="btnLoadPesan" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>

    $('#btnPesan').click(function() {

        if(confirm('Yakin akan memesan barang ini?') ){
            $('#btnPesan').hide()
            $('#btnLoadPesan').show()
            return true;
        } else {
            return false;
        }
    });
    
    $('#modal_shop').appendTo("body");

    function pilih(id_items, id_user, nama, harga, stok) {
        // $("#nama").html(nama);
        // $("#harga").html("Rp." + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        // $("#stok").html(stok);
        // $("#jumlah").val(1);
        // $("#jumlah").attr('max', stok);
        // $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    </script>
    @stack('scripts')

@endsection
