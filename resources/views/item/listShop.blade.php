@extends('layouts.app')

@section('content')

<style>

.dataTables_filter{
    display: none;
}

.form-group {
    margin-bottom: 1px;
}

a:link {
    color: #495057;
}

.nav-pills .nav-link.active {
    background: #fb501a;
}

</style>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-shopbag icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Katalog
                <div class="page-title-subheading">Katalog produk dan jasa.
                </div>
            </div>
        </div>
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    {{-- <div class="card-header">
        @if(empty($search))
        <span style="font-size: smaller;"><a href='{{ route('listShop') }}'>Daftar Katalog</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> {{ $nama_kategori }}</span>
        @else
        <span style="font-size: smaller;"><a href='{{ route('listShop') }}'>Daftar Katalog</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Cari: "{{ $search }}"</span>
        @endif
    </div> --}}

    <div class="card-body">

        <div class="border-bottom mb-2 overflow-auto" style="overflow-x: auto">
            <ul class="nav nav-pills mb-2" style="flex-wrap: nowrap;">

                <li class="nav-item border-right" style="font-size: smaller;">
                    <a data-toggle="tab" href="#tab-eg13-0" class="nav-link {{ (empty($kategori)) ? 'active' : '' }} show text-nowrap" onclick="ubah_kategori('')">Semua</a>
                </li>
                
                @foreach($kategoris as $kt)

                <li class="nav-item border-right" style="font-size: smaller;">
                    <a data-toggle="tab" href="#tab-eg13-0" class="nav-link {{ ($kategori == $kt->id) ? 'active' : '' }} show text-nowrap" onclick="ubah_kategori({{ $kt->id }})">{{ $kt->nama }}</a>
                </li>

                @endforeach

            </ul>
        </div>

        <div class="row">
            <div id="filterKat" class="dataTables_length"></div>
        </div>    

        <table class="table table-striped table-hover" id="items-table" style="background-color: white; display: none;">
            <thead>
                <tr>
                    <th style="max-width: 20px;">No</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Kategori</th>
                    <th>Foto</th>
                    <th>Seller</th>
                    <th>Updated</th>
                    <th style="max-width: 100px;">Action</th>
                </tr>
            </thead>
        </table>

        <div class="row justify-content-center" id="katalog" style="margin: auto; padding-top: 10px;"></div>
        <div class="float-right" id="paginat"></div>

    </div>
</div>


<script>
    var table;

    $(function() {

        table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('getItems.datas') !!}?kategori={{ $kategori }}&value={{ $search }}',
                // data: {
                //     kategori: "{{ $kategori }}",
                //     search: { "value": "{{ $search }}" }
                // },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: 'deskripsi', name: 'deskripsi' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    katalog(data, data.tot);
                    //alert(row);
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'stok', name: 'stok' , searchable: true},   
                { data: 'id_kategori', name: 'id_kategori' , searchable: true},
                { data: 'foto', name: 'foto' , searchable: true},
                { data: 'id_seller', name: 'id_seller' , searchable: true},
                { data: 'updated_at', name: 'updated_at' , searchable: true},            
                { data: null, name: 'action', render: function ( data, type, row ) {
                    if(data.stok == 0)
                        return "<b>Stok Habis</b>";
                    else 
                        return '<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_shop" onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> Beli</button>';
                } },
            ],
            order: [[8, "desc"]],
            oLanguage: {
                oPaginate: {
                    sNext: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-right" style="color: #8d8d8d;"></i></span>',
                    sPrevious: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-left" style="color: #8d8d8d;"></i></span>'
                }
            },
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }],
            iDisplayLength: 20,
            initComplete: function(settings, json) {                
                //var page = $('.dataTables_paginate').clone()
                //page.attr('id', 'page1')
                //page.appendTo("#paginat");
                $("#paginat").append($('.dataTables_paginate'));
            },
        });
        
    });

    var x = 0;
    var n = 1;
    var len = 0;
    var init = true;

    function katalog(data, tot) {
        //alert(JSON.stringify(table.page.info()));
        //var len = table.page.info().length;
        var harga = data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var tombol;
        if(data.stok == 0)
            tombol = '<button type="button" class="btn btn-sm btn-danger btn-block" disabled><b>Stok Habis</b></button>';
        else 
            tombol = '<button type="button" class="btn btn-sm btn-success btn-block" data-toggle="modal" data-target="#modal_shop" ' + 
                    'onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + 
                    '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> <b>Beli</b></button>';
                        
        var cnt = $('select[name="items-table_length"] option:selected').val();
        //alert(cnt);
        var html = "<div class='pb-3'>" + 
                        "<div class='panel panel-default items'>" + 
                            "<div class='item-pict'><a href='viewItem/" + data.id + "' title='Lihat Detail'><img src='{{ asset('assets/images/items/thumb/') }}/" + data.foto + "' ></a></div>" + 
                            "<div style='padding: 10px;'>" + 
                                "<span style='font-weight: 600;'>" + data.nama.substr(0, 44) + "</span><br><b>" + 
                                "<span style='color: red;'>Rp " + harga + "</span></b><br>" + 
                                //"<span style=''>Stok: " + data.stok + "</span>" + 
                                "<span style='color: rgba(0, 0, 0, 0.54);'>" + data.nama_kategori + "</span>" + 
                            "</div>" + 
                            //"<div style='margin-top: 20px; text-align: center;'>" + tombol + "</div>" + 
                        "</div>" + 
                    "</div>";

        if(init) {
            if(x >= cnt || x >= tot) {
                if(n > len) {
                    $("#katalog").html('');
                    n = 1;
                    len = tot;
                }
                $("#katalog").append(html);
                n++;
                init = false;
            }
        } else {
            if(n > len) {
                $("#katalog").html('');
                n = 1;
                len = tot;
            }
            $("#katalog").append(html);
            n++;
        }
                
        x++;        
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function pilih(id_items, id_user, nama, harga, stok) {
        $("#nama").html(nama);
        $("#harga").html("Rp." + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $("#stok").html(stok);
        $("#jumlah").val(1);
        $("#jumlah").attr('max', stok);
        $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    function copyKat() {
        var html = $("#items-table_length").html();
        //alert(html);        
        $("#filterKat").html(html);
    }

    function konfirmasi() {
        if(confirm('Yakin akan membeli barang ini?') ){
            return true;
        } else {
            return false;
        }
    }

    function ubah_kategori(id) {
        //alert(id)
        //$("#katalog").html('');
        table.ajax.url("{!! route('getItems.datas') !!}" + "?kategori=" + id);
        table.ajax.reload();
        $("#katalog").html('');
    }

    $(document).ready(function() {
        $('select[name="items-table_length"]').html('<option value="20">20</option><option value="40">40</option><option value="100">100</option>');
        $('.dataTables_info').hide();
    });    

    </script>
    @stack('scripts')

@endsection
