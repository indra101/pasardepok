@if ($message = Session::get('success'))
    <div class="alert alert-success fade show">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times" style="font-size: medium;"></span></a>
        <p>{!!html_entity_decode($message)!!}</p>
    </div>
@else
    @if ($message = Session::get('fail'))
        <div class="alert alert-danger fade show">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times" style="font-size: medium;"></span></a>
            <p>{!!html_entity_decode($message)!!}</p>
        </div>
    @endif
@endif