@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Kategori
                <div class="page-title-subheading">Kategori Produk
                </div>
            </div>
        </div>
        
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    <div class="card-body">

    <a href="{{route('addKategori.page')}}" class="btn btn-xs btn-alternate"><i class="fa fa-plus-circle"></i> Tambah Kategori</a>    
    <br/><br/>
    <div class="overflow-auto">
        <table class="table table-striped table-hover" id="kategoris-table" style="background-color: white;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Icon</th>
                    <th>Urutan</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <script>
    $(function() {

        var table = $('#kategoris-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getKategoris.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true}, 
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<img src="{{ asset("assets/images/icons/") }}/' + data.icon + '" >';
                } },    
                { data: 'urutan', name: 'urutan' , searchable: true},                
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<a href="editKategoriPage/' + data.id + '" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a> ' +
                            '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger" onclick=\'confirmDel(' + data.id + ',"' + data.nama + '")\' title="Delete"><i class="fa fa-trash"></i></a>' +
                            '<form class="delete" action="delKategori/' + data.id + '" method="get">' +
                            '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                            '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' +
                            '</form></div>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, nama) {
        var txt;
        var r = confirm("Yakin akan menghapus kategori? \n\Kategori: " + nama);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

    </div>
</div>
@endsection
