@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Ubah Kategori
                <div class="page-title-subheading">Kategori Produk
                </div>
            </div>
        </div>
          
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('listKategori') }}'>Daftar Kategori</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Ubah Kategori</span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.Kategori') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" name="id" value="{{ $kategoris->id }}">
                        
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label for="nama" class="col-md-4 control-label">Nama Kategori</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" maxlength="20" value="{{ $kategoris->nama }}" required autofocus>

                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('urutan') ? ' has-error' : '' }}">
                            <label for="urutan" class="col-md-4 control-label">Urutan</label>

                            <div class="col-md-6">
                                <input id="urutan" type="number" name="urutan" step="1" min="0" width="20px" style="width: 50px; padding-left: 3px;" value="{{ $kategoris->urutan }}" required>

                                @if ($errors->has('urutan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('urutan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div>
                                    <label for="phone" class="control-label">Icon</label>
                                </div>
                                <div class="mt-4">
                                    <img id="image_preview_container" class="img-fluid" src="{{ asset('assets/images/icons/'.$kategoris->icon) }}"  style="margin: auto; max-width: 60px; cursor: pointer;">
                                </div>
                                <div class="mt-2">
                                    <input type="file" name="image" placeholder="Choose image" id="image">
                                </div>
                                <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                                <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 50 KB</i></span></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">  

function konfirmasi() {
    if(confirm('Data sudah benar?') ){
        return true;
    } else {
        return false;
    }
}

$('#image').change(function(e){

    var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
    var name = this.files[0].name;
    var name_arr = name.split(".");
    var type = name_arr[name_arr.length - 1];
    var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
    var pesan_size = "\nMohon masukkan file dengan ukuran max. 50 KB";
    var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
    var cek_size = size >= 0.05; // max file size 50 KB
    var cek_tipe = allowed.indexOf(type) == -1;

    if(cek_size || cek_tipe) {
        var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
        if(cek_size)
            pesan += pesan_size;

        if(cek_tipe)
            pesan += pesan_tipe;

        alert(pesan);
        a.value = '';

    } else {
        
        let reader = new FileReader();
        reader.onload = (e) => { 
            $('#image_preview_container').attr('src', e.target.result); 
        }
        reader.readAsDataURL(this.files[0]); 

    }

});

$('#image_preview_container').click(function() {
    $('#image').click();
});

</script>
@endsection
