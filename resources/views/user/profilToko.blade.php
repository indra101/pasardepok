@extends('layouts.app')

@section('content')

<style>

.dataTables_filter{
    display: none;
}

.form-group {
    margin-bottom: 1px;
}

.items {
    height: 310px;
}

#logout-form {
    display: none;
}

.buttonProfil {
    text-align: right;
}

@media only screen and (max-width: 700px){
    .items {
        height: 250px;
    }

    #logout-form {
        display: block;
    }

    .buttonProfil {
        text-align: center;
    }
}

</style>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-id icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Profil Toko Anda
                <div class="page-title-subheading">Informasi profil dan produk toko Anda.
                </div>
            </div>
        </div>
        
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    <div class="card-header">
        <div class="row" style="width: 100%;">  
            <div class="col-md-8 mb-2 mt-2">
                <span style="">Profil Toko</span>
            </div>
            <div class="col-md-4 buttonProfil">
                <a href="editUserPage/{{ Auth::user()->id }}" class="btn btn-xs btn-warning" title="Edit" style="border-radius: 30px;"><i class="fa fa-edit mr-2"></i>Ubah Profil</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="margin-top: 5px;">
                    {{ csrf_field() }}
                    <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out-alt mr-2"></i>SIGN OUT</a>
                </form>
            </div>
        </div>
    </div>

    <div class="card-body p-4">

        <div class="row">
            <div class="col-md-2">
                <img class="" src="{{ asset('assets/images/users/thumb/') }}/{{ $penjual->foto }}" alt="slide" width="100px" height="100px">
            </div>
            <div class="col-md-5">
                <span style="font-size: medium;"><b>{{ $penjual->nama_toko }}</b></span>
                <div class="row mt-2">
                    <div class="col-md-4" style="color: #999">
                        Nama Pemilik
                    </div>
                    <div class="col-md-8">
                        {{ $penjual->name }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="color: #999">
                        Alamat
                    </div>
                    <div class="col-md-8">
                        {{ $penjual->address }}<br>Kel. {{ $penjual->get_kelurahan->nama }}, Kec. {{ $penjual->get_kelurahan->kecamatan->nama }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="color: #999">
                        No. HP
                    </div>
                    <div class="col-md-8">
                        <img width="25" src="{{ asset('assets/images/wa.png') }}"><a class="ml-2" href="https://wa.me/62{{ $penjual->hp }}">{{ $penjual->hp }}</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5" style="border-left: 1px solid #d2d2d2">
                <div class="row mt-2">
                    <div class="col-md">
                    &nbsp;
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="color: #999">
                        Bergabung
                    </div>
                    <div class="col-md">
                        {{ date_format(date_create($penjual->created_at),"j F Y") }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="color: #999">
                        Terakhir Online
                    </div>
                    <div class="col-md">
                        {{ date_format(date_create($penjual->last_login),"j F Y") }}
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-header">
        <div class="row" style="width: 100%;">  
            <div class="col-md-10 mb-2 mt-2">
                <span style="">Daftar Produk</span>
            </div>
            <div class="col-md-2" style="text-align: center;">
                <a href="{{ route('addItem.page') }}" class="btn btn-warning" style="border-radius: 30px;"><i class="fa fa-plus-circle mr-1"></i>Tambah Produk</a>
            </div>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div id="filterKat" class="dataTables_length"></div>
        </div>    

        <table class="table table-striped table-hover" id="items-table" style="background-color: white; display: none;">
            <thead>
                <tr>
                    <th style="max-width: 20px;">No</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Kategori</th>
                    <th>Foto</th>
                    <th>Seller</th>
                    <th>Updated</th>
                    <th style="max-width: 100px;">Action</th>
                </tr>
            </thead>
        </table>

        <div class="row justify-content-center" id="katalog" style="margin: auto; padding-top: 10px;"></div>
        <div class="float-right" id="paginat"></div>

    </div>
</div>


<div class="modal fade" id="modal_shop" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 400px; margin: auto;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Beli Barang</h4>
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('beli.Item') }}">
            {{ csrf_field() }}
        
                <div class="modal-body" style="margin-bottom: 20px;">
                    <div style='text-align: center; font-size: 60px; background-color: lightgrey; border-radius: 8px; margin-bottom: 10px;'><i class='fa fa-shopping-bag'></i>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" >Nama Barang</label>
                        <div class="col-xs-8" style="margin-top: 5px;">
                            <span id="nama"></span>
                        </div>
                    </div>                                        
                                                        

                    <div class="form-group">
                        <label class="control-label col-xs-4" >Harga</label>
                        <div class="col-xs-8" style="margin-top: 5px;">
                            <span id="harga"></span>
                        </div>                                    
                                                                
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-4" >Stok</label>
                        <div class="col-xs-8" style="margin-top: 5px;">
                            <span id="stok"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-4" >Jumlah Pembelian</label>
                        <div class="col-xs-8" style="margin-top: 5px;">
                            <input type="number" id="jumlah" name="jumlah" step="1" min="1" width="20px" style="width: 50px; padding-left: 3px;">
                        </div>
                    </div>

                    <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="id_items" id="id_items">

                    <div style=" margin-left: 30px;"><span>Tekan <b>Beli</b> untuk melanjutkan pembelian</span></div>

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input class="btn btn-primary" type="submit" value="Beli" onclick="return konfirmasi()"/>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>
    var table;

    $(function() {

        table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('getItems.datas') !!}',
                data: {
                    "id_seller": "{{ $penjual->id }}"
                },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: 'deskripsi', name: 'deskripsi' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    katalog(data, data.tot);
                    //alert(row);
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'stok', name: 'stok' , searchable: true},
                { data: 'id_kategori', name: 'id_kategori' , searchable: true},
                { data: 'foto', name: 'foto' , searchable: true},
                { data: 'id_seller', name: 'id_seller' , searchable: true},
                { data: 'updated_at', name: 'updated_at' , searchable: true},                
                { data: null, name: 'action', render: function ( data, type, row ) {
                    if(data.stok == 0)
                        return "<b>Stok Habis</b>";
                    else 
                        return '<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_shop" onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> Beli</button>';
                } },
            ],
            order: [[8, "desc"]],
            oLanguage: {
                oPaginate: {
                    sNext: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-right" style="color: #8d8d8d;"></i></span>',
                    sPrevious: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-left" style="color: #8d8d8d;"></i></span>'
                }
            },
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }],
            iDisplayLength: 20,
            initComplete: function(settings, json) {
                //alert( 'DataTables has finished its initialisation.' );
                //copyKat();
                $("#paginat").append($('.dataTables_paginate'));
            }
        });
        
    });

    var x = 0;
    var n = 1;
    var len = 0;
    var init = true;

    function katalog(data, tot) {
        //alert(JSON.stringify(table.page.info()));
        //var len = table.page.info().length;
        var harga = data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var tombol;
        if(data.stok == 0)
            tombol = '<button type="button" class="btn btn-sm btn-danger btn-block" disabled><b>Stok Habis</b></button>';
        else 
            tombol = '<button type="button" class="btn btn-sm btn-success btn-block" data-toggle="modal" data-target="#modal_shop" ' + 
                    'onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + 
                    '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> <b>Beli</b></button>';
                        
        var cnt = $('select[name="items-table_length"] option:selected').val();
        //alert(cnt);
        var html = "<div class='pb-3'>" + 
                        "<div class='panel panel-default items'>" + 
                            "<div class='item-pict'><a href='viewItem/" + data.id + "' title='Lihat Detail'><img src='{{ asset('assets/images/items/thumb/') }}/" + data.foto + "' ></a></div>" + 
                            "<div class='p-1'>" + 
                                "<span style='font-weight: 600;'>" + data.nama.substr(0, 44) + "</span><br><b>" + 
                                "<span style='color: red;'>Rp " + harga + "</span></b><br>" + 
                                //"<span style=''>Stok: " + data.stok + "</span>" + 
                                "<span style='color: rgba(0, 0, 0, 0.54); font-size: 0.857143rem;'>" + data.nama_kategori + "</span>" + 
                            "</div>" + 
                            "<div class='p-1' style='text-align: center;'>" + 
                                '<a href="editItemPage/' + data.id + '" class="btn btn-xs btn-warning" title="Edit" style="padding: .2rem .4rem;"><i class="fa fa-edit"></i></a>' +  
                                '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger" style="margin-left: 5px; padding: .2rem .4rem;" onclick=\'confirmDel(' + data.id + ',"' + data.nama + '")\' title="Delete"><i class="fa fa-trash"></i></a>' +
                                '<form class="delete" action="delItem/' + data.id + '" method="get">' + 
                                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                                '</form>' +
                            "</div>" + 
                            //"<div style='margin-top: 20px; text-align: center;'>" + tombol + "</div>" + 
                        "</div>" + 
                    "</div>";

        if(init) {
            if(x >= cnt || x >= tot) {
                if(n > len) {
                    $("#katalog").html('');
                    n = 1;
                    len = tot;
                }
                $("#katalog").append(html);
                n++;
                init = false;
            }
        } else {
            if(n > len) {
                $("#katalog").html('');
                n = 1;
                len = tot;
            }
            $("#katalog").append(html);
            n++;
        }
                
        x++;        
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus produk? \n\n" + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function pilih(id_items, id_user, nama, harga, stok) {
        $("#nama").html(nama);
        $("#harga").html("Rp." + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $("#stok").html(stok);
        $("#jumlah").val(1);
        $("#jumlah").attr('max', stok);
        $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    function copyKat() {
        var html = $("#items-table_length").html();
        //alert(html);        
        $("#filterKat").html(html);
    }

    function konfirmasi() {
        if(confirm('Yakin akan membeli barang ini?') ){
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function() {
        $('select[name="items-table_length"]').html('<option value="20">20</option><option value="40">40</option><option value="100">100</option>');
        $('.dataTables_info').hide();
    });    

    </script>
    @stack('scripts')

@endsection
