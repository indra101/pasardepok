@extends('layouts.app')

@section('content')

<style>

@media only screen and (max-width: 700px){
    .btn_simpan {
        width: 100%;
    }
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Tambah User
                <div class="page-title-subheading">Tambah user baru.
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="container">

    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('home') }}'>Daftar User</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Tambah User</span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.User') }}">
                        {{ csrf_field() }}

                        {{-- <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username<font color="red"> *</font></label>

                             <div class="col-md-6">
                                <input type="text" class="form-control" name="username" maxlength="50" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div> --}}
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" maxlength="100" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" maxlength="100" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" maxlength="100" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" maxlength="100" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" maxlength="200">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
                            <label for="kelurahan" class="col-md-4 control-label">Kelurahan</label>

                            <div class="col-md-6">
                                <select id="kelurahan" class="form-control" name="kelurahan" required>
                                    <option value="">Pilih Kelurahan</option>
                                    @foreach($kelurahans as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('kelurahan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelurahan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control" name="phone">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ketua_uppa" class="col-md-4 control-label">Nama Ketua DPRa</label>

                            <div class="col-md-6">
                                <input id="ketua_uppa" type="text" class="form-control" name="ketua_uppa" maxlength="200">
                            </div>
                        </div>

                        {{-- <div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
                            <label for="npwp" class="col-md-4 control-label">NPWP<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="npwp" type="text" class="form-control" name="npwp" required>
                                @if ($errors->has('npwp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('npwp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Jabatan<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <select id="jabatan" type="jabatan" class="form-control" name="jabatan" required>
                                    @foreach($jabatans as $jabatan)
                                        <option value="{{$jabatan->id}}">{{$jabatan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Role<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <select id="role" type="role" class="form-control" name="role" required>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn_simpan" onclick="return konfirmasi();">
                    Simpan
                </button>
            </div>
        </div>
    </div>
    <script type="text/javascript">  

    function konfirmasi() {
        if(confirm('Data sudah benar?') ){
            return true;
        } else {
            return false;
        }
    }

    </script>
</div>

@endsection


