@extends('layouts.app')

@section('content')

<style>

#logout-form {
    display: none;
}

.card-header {
    display: none;
}

@media only screen and (max-width: 700px){
    .btn_simpan {
        width: 100%;
    }

    #logout-form {
        display: block;
    }

    .card-header {
        display: block;
    }
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Profil Pengguna
                {{-- <div class="page-title-subheading">Profil user.
                </div> --}}
            </div>
        </div>
         
    </div>
</div>


<div class="container">

    <div class="main-card mb-3 card">
        
        <div class="card-header">
            @php $role = Auth::user()->role_id; @endphp
            @if($role == 2)
            <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal_toko"><i class="fa fa-store"></i> Buka Toko</a>
            @endif
        </div>

        <div class="card-body">
            
            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">
                    <div class="col-md-6">

                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        {{-- <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="control-label">Username</label>

                             <div class="">
                                <input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" readonly>

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div> --}}
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Nama Lengkap</label>

                            <div class="">
                                <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" readonly>
                            </div>
                        </div>

                        @if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Nama Toko</label>

                            <div class="">
                                <input id="nama_toko" type="text" class="form-control" name="nama_toko" value="{{ Auth::user()->nama_toko }}" readonly>
                            </div>
                        </div>

                        @endif

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label">Alamat</label>

                            <div class="">
                                <input id="address" type="text" class="form-control" name="address" value="{{ Auth::user()->address }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="kecamatan" class="control-label">Kecamatan</label>

                            <div class="">
                                <input id="kecamatan" type="text" class="form-control" name="address" value="{{ Auth::user()->get_kelurahan->kecamatan->nama }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="kelurahan" class="control-label">Kelurahan</label>

                            <div class="">
                                <input id="kelurahan" type="text" class="form-control" name="address" value="{{ Auth::user()->get_kelurahan->nama }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">Nomor Handphone</label>

                            <div class="">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ Auth::user()->hp }}" readonly>
                            </div>
                        </div>
                                                
                    </form>

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="phone" class="control-label">Profile Picture</label>
                            <img class="d-block img-fluid" src="{{ asset('assets/images/users/'.Auth::user()->foto) }}" style="margin: auto; max-width: 100%;">
                        </div>
                    </div>

                    </div>
                </div>
                
            </div>
        </div>

        <div class="card-footer">
            <div class="col-md-2">
                <a href="editUserPage/{{ Auth::user()->id }}"  class="btn btn-xs btn-primary btn_simpan"><i class="fa fa-edit mr-2"></i>Ubah</a>
            </div>
            <div class="col-md-2">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="margin-top: 5px;">
                    {{ csrf_field() }}
                    <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out-alt mr-2"></i>Sign Out</a>
                </form>
            </div>
        </div>
    </div>

    

</div>
@endsection
