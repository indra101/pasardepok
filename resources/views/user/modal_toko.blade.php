<div class="modal fade" id="modal_toko" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelTerima">Buka Toko Anda</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('bukaToko') }}">
            {{ csrf_field() }}
        
                <div class="modal-body">

                    <div class="row justify-content-md-center mb-4">
                        <div class='item-pict mb-4'><img id="img_item_toko" src='{{ asset('assets/images/shop_icon.png') }}' style="width: 300px;"></div>
                    </div>        
                    
                    <div class="row">
                        <div class="col-md-12">
                        <label for="nama_toko" class="control-label">Nama Toko<font color="red">*</font></label>
                        <input id="nama_toko" type="text" class="form-control" name="nama_toko" required autofocus>
                        </div>
                    </div>
                    
                    <input type="hidden" name="id" id="id" value="{{ Auth::user()->id }}">

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-secondary" type="button" value="Kembali" data-dismiss="modal"/>
                    <input id="btnKirim" class="btn btn-danger" type="submit" value="Buka Toko" onclick=""/>
                    <button id="btnLoadKirim" class="btn btn-danger" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>
    
    $('#btnKirim').click(function() {
        if($('#nama_toko').val()) {
            $('#btnKirim').hide()
            $('#btnLoadKirim').show()
        }
    });
    
</script>