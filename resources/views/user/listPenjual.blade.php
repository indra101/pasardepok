@extends('layouts.app')

@section('content')

<style>

.dataTables_filter{
    display: none;
}

.form-group {
    margin-bottom: 1px;
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-id icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Penjual
                <div class="page-title-subheading">Daftar Penjual Barang dan Jasa.
                </div>
            </div>
        </div>
        
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@else
    @if ($message = Session::get('fail'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
            <p>{{ $message }}</p>
        </div>
    @endif
@endif

<div class="main-card mb-3 card">
    {{-- <div class="card-header">
        <span style="font-size: smaller;"><a href='{{ route('listShop') }}'>Daftar Katalog</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> {{ $nama_kategori }}</span>
    </div> --}}

    <div class="card-body">

        <div class="row">
            <div id="filterKat" class="dataTables_length"></div>
        </div>    

        <table class="table table-striped table-hover" id="items-table" style="background-color: white; display: none;">
            <thead>
                <tr>
                    <th style="max-width: 20px;"></th>
                </tr>
            </thead>
        </table>

        <div class="row justify-content-center" id="katalog" style="margin: auto; padding-top: 10px;"></div>
        <div class="float-right" id="paginat"></div>

    </div>
</div>



<script>
    var table;

    $(function() {

        table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('get.penjuals') !!}',
                data: {
                    "role_id": "3"
                },
            },
            columns: [
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    penjual(data, data.tot);
                    //alert(row);
                    //return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
            ],
            oLanguage: {
                oPaginate: {
                    sNext: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-right" style="color: #8d8d8d;"></i></span>',
                    sPrevious: '<span class="pagination-default"></span><span class="pagination-fa"><i class="fa fa-chevron-circle-left" style="color: #8d8d8d;"></i></span>'
                }
            },
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }],
            iDisplayLength: 20,
            initComplete: function(settings, json) {
                //alert( 'DataTables has finished its initialisation.' );
                //copyKat();
                $("#paginat").append($('.dataTables_paginate'));
            }
        });
        
    });

    var x = 0;
    var n = 1;
    var len = 0;
    var init = true;

    function penjual(data, tot) {
        var cnt = $('select[name="items-table_length"] option:selected').val();
        var foto = (data.foto == null) ? 'no-image.png' : data.foto;
        var html = "<div class='pb-3'>" + 
                        "<div class='panel panel-default items'>" + 
                            "<div class='item-pict'><a href='listShopPenjual/" + data.id + "' title='Lihat Detail'><img src='{{ asset('assets/images/users/thumb/') }}/" + foto + "' width='200px' height='200px'></a></div>" + 
                            "<div style='padding: 10px;'>" + 
                                "<span style='font-weight: 600;'>" + data.nama_toko + "</span><br><b>" + 
                                "<span style='font-weight: 500;'>" + data.name + "</span><br><b>" + 
                                "<span style='font-weight: 500;'>" + data.kelurahan + ", " + data.kecamatan + "</span><br>" + 
                            "</div>" + 
                        "</div>" + 
                    "</div>";

        if(init) {
            if(x >= cnt || x >= tot) {
                if(n > len) {
                    $("#katalog").html('');
                    n = 1;
                    len = tot;
                }
                $("#katalog").append(html);
                n++;
                init = false;
            }
        } else {
            if(n > len) {
                $("#katalog").html('');
                n = 1;
                len = tot;
            }
            $("#katalog").append(html);
            n++;
        }
                
        x++;        
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function pilih(id_items, id_user, nama, harga, stok) {
        $("#nama").html(nama);
        $("#harga").html("Rp." + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $("#stok").html(stok);
        $("#jumlah").val(1);
        $("#jumlah").attr('max', stok);
        $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    function copyKat() {
        var html = $("#items-table_length").html();
        //alert(html);        
        $("#filterKat").html(html);
    }

    function konfirmasi() {
        if(confirm('Yakin akan membeli barang ini?') ){
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function() {
        $('select[name="items-table_length"]').html('<option value="20">20</option><option value="40">40</option><option value="100">100</option>');
        $('.dataTables_info').hide();
        //$('.previous .page-link').html('<');
    });    

    </script>
    @stack('scripts')

@endsection
