@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Data User
                <div class="page-title-subheading">Data profil user.
                </div>
            </div>
        </div>
        {{-- <div class="page-title-actions">
            <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                <i class="fa fa-star"></i>
            </button>
            <div class="d-inline-block dropdown">
                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="fa fa-business-time fa-w-20"></i>
                    </span>
                    Buttons
                </button>
                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon lnr-inbox"></i>
                                <span>
                                    Inbox
                                </span>
                                <div class="ml-auto badge badge-pill badge-secondary">86</div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon lnr-book"></i>
                                <span>
                                    Book
                                </span>
                                <div class="ml-auto badge badge-pill badge-danger">5</div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon lnr-picture"></i>
                                <span>
                                    Picture
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a disabled href="javascript:void(0);" class="nav-link disabled">
                                <i class="nav-link-icon lnr-file-empty"></i>
                                <span>
                                    File Disabled
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>     --}}
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;"><a href='{{ route('home') }}'>Daftar User</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Data User</span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        <div class="row">
                        <div class="col-md-6">

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="control-label">Username</label>

                             <div class="">
                                <span class="form-control" name="username" >{{ $user->username }}</span>
                             </div>
                         </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name</label>

                            <div class="">
                                <span class="form-control" name="name" >{{ $user->name }}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <div class="">
                                <span class="form-control" name="email" >{{ $user->email }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label">Address</label>

                            <div class="">
                                <span class="form-control" name="address" >{{ $user->address }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">Phone Number</label>

                            <div class="">
                                <span class="form-control" name="phone" >{{ $user->hp }}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
                            <label for="npwp" class="ontrol-label">NPWP</font></label>

                            <div class="">
                                <span id='npwp' class="form-control" name="npwp" >{{ $user->npwp }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role" class="control-label">Jabatan</font></label>

                            <div class="">
                                @foreach($jabatans as $jabatan)
                                    @if ($user->jabatan_id == $jabatan->id)
                                        <span class="form-control" >{{ $jabatan->name }}</span>
                                    @endif      
                                @endforeach
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="role" class="control-label">Role</label>

                            <div class="">
                                @foreach($roles as $role)
                                    @if ($user->role_id == $role->id)
                                        <span class="form-control" >{{ $role->name }}</span>
                                    @endif      
                                @endforeach
                            </div>
                        </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="phone" class="control-label">Profile Picture</label>
                                <img class="d-block" src="{{ asset('assets/images/users/'.Auth::user()->foto) }}" width="350px" height="350px" style="margin: auto;">
                            </div>
                        </div>

                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(function() {
        //$("#npwp").mask("99.999.999.9-999.999");
        var a = $("#npwp").text();
        var x = [a.slice(0, 2), '.', a.slice(2, 5), '.', a.slice(5, 8), '.', a.slice(8, 9), '-', a.slice(9, 12), '.', a.slice(12)].join('');
        $("#npwp").text(x);        
    });
    </script>
</div>
@endsection
