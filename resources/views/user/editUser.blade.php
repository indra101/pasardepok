@extends('layouts.app')

@section('content')

<style>

@media only screen and (max-width: 700px){
    .btn_simpan {
        width: 100%;
    }
}

</style>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Ubah Profil Pengguna
                {{-- <div class="page-title-subheading">Edit data user.
                </div> --}}
            </div>
        </div>
        
    </div>
</div>

<div class="container">
    <div class="main-card mb-3 card">
        
        <div class="card-header">
            <span style="font-size: smaller;">
                <a href="{{ route('home') }}">Beranda</a> <i class="fa fa-chevron-right ml-2 mr-2"></i> Ubah Profil Pengguna
            </span>
        </div>

        <div class="card-body">

            <div class="panel panel-default">

                <div class="panel-body"> 

                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row">
                        <div class="col-md-6">

                        @php
                        $role = Auth::user()->role_id;
                        @endphp

                        <input type="hidden" class="form-control" name="id" value="{{ $user->id }}">

                        {{-- <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="control-label">Username</label>

                             <div class="">
                                <input type="text" class="form-control" name="username" maxlength="50" value="{{ $user->username }}">

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div> --}}
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Nama Lengkap</label>

                            <div class="">
                                <input id="name" type="text" class="form-control" name="name" maxlength="100" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if($user->role_id == 3 || $user->role_id == 1)

                        <div class="form-group{{ $errors->has('nama_toko') ? ' has-error' : '' }}">
                            <label for="name_toko" class="control-label">Nama Toko</label>

                            <div class="">
                                <input id="name_toko" type="text" class="form-control" name="nama_toko" maxlength="100" value="{{ $user->nama_toko }}" required autofocus>

                                @if ($errors->has('nama_toko'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nama_toko') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @else 

                        <input type="hidden" class="form-control" name="nama_toko" value="">

                        @endif

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" maxlength="100" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password<br>
                                <span style="font-size: smaller; color: grey;">(kosongkan bila tidak berubah)</span>
                            </label>
                            

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" maxlength="100">
                                <span id="msg_pass" style="display: none; color: red; font-size: small;">Password tidak sama!</span>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label">Konfirmasi Password</label>

                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label">Alamat</label>

                            <div class="">
                                <input id="address" type="text" class="form-control" name="address" maxlength="200" value="{{ $user->address }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('id_kecamatan') ? ' has-error' : '' }}">
                            <label for="id_kecamatan">Kecamatan</label>

                            <div class="">
                                <select id="id_kecamatan" class="form-control" name="id_kecamatan" onchange="get_kelurahan()" required>
                                    <option value="">Pilih Kecamatan</option>
                                    @foreach($kecamatans as $key => $val)
                                        <option value="{{$key}}" {{ ($user->get_kelurahan->kecamatan->id == $key) ? 'selected' : '' }}>{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('id_kecamatan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_kecamatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('id_kelurahan') ? ' has-error' : '' }}">
                            <label for="id_kelurahan">Kelurahan</label>

                            <div class="">
                                <select id="id_kelurahan" class="form-control" name="id_kelurahan" required>
                                    <option value="">Pilih Kelurahan</option>
                                    @foreach($kelurahans as $key => $val)
                                        <option value="{{$key}}" {{ ($user->get_kelurahan->id == $key) ? 'selected' : '' }}>{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('kategori'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kategori') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">Nomor Handphone</label>

                            <div class="">
                                <input id="phone" type="number" class="form-control" name="phone" maxlength="50" value="{{ $user->hp }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ketua_uppa" class="control-label">Nama Ketua DPRa</label>

                            <div class="">
                                <input id="ketua_uppa" type="text" class="form-control" name="ketua_uppa" maxlength="200" value="{{ $user->ketua_uppa }}" required>
                            </div>
                        </div>

                        @if($role == 1)
                        <div class="form-group">
                            <label for="role" class="control-label">Role</label>

                            <div class="">
                                <select id="role" type="role" class="form-control" name="role" value="{{ $user->role_id }}" required>
                                    @foreach($roles as $role)
                                        @if ($user->role_id == $role->id)
                                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                        @else
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endif      
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @else
                            <input type="hidden" name="role" value="{{ $role }}"/>
                        @endif

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div>
                                    <label for="phone" class="control-label">Profile Picture</label>
                                    
                                    <img id="image_preview_container" class="img-fluid" src="{{ asset('assets/images/users/'.$user->foto) }}"  style="margin: auto; max-width: 100%; cursor: pointer;">
                                </div>
                                <div class="mt-4">
                                    <input type="file" name="image" placeholder="Choose image" id="image">
                                </div>
                                <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                                <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 5 MB</i></span></div>
                            </div>
                        </div>

                        </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn-block btn_simpan" onclick="return konfirmasi();">
                    Simpan
                </button>
            </div>
        </div>

        </form>
    </div>
</div>

<script type="text/javascript">  

function konfirmasi() {
    if(confirm('Data sudah benar?')){

    var pass = $('#password').val();

    var konfirmasi = $('#password-confirm').val();

    if(pass != '') {
        if(pass !== konfirmasi) {
            $('#msg_pass').show();
            return false;
        } else {
            $('#msg_pass').hide();
        }
    }

    return true;    

    } else {
        return false;
    }
}

$('#image_preview_container').click(function() {
    $('#image').click();
});

$('#image').change(function(e){

    var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
    var name = this.files[0].name;
    var name_arr = name.split(".");
    var type = name_arr[name_arr.length - 1];
    var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
    var pesan_size = "\nMohon masukkan file dengan ukuran max. 5 MB";
    var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
    var cek_size = size >= 5; // max file size 5 MB
    var cek_tipe = allowed.indexOf(type) == -1;

    if(cek_size || cek_tipe) {
        var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
        if(cek_size)
            pesan += pesan_size;

        if(cek_tipe)
            pesan += pesan_tipe;

        alert(pesan);
        a.value = '';

    } else {
        
        let reader = new FileReader();
        reader.onload = (e) => { 
            $('#image_preview_container').attr('src', e.target.result); 
        }
        reader.readAsDataURL(this.files[0]); 

    }

});

function get_kelurahan() {
    var id_kecamatan = $('#id_kecamatan').val();

    $.ajax({
        url: '{{ route("get_kelurahan") }}',
        data: 'id_kecamatan=' + id_kecamatan,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_kelurahan').empty();
            $('#id_kelurahan').append('<option value="">Pilih Kelurahan</option>');
            $.each(data, function(key, value) {
                $('#id_kelurahan').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

</script>

@endsection
