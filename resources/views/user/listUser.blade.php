@extends('layouts.app')

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Manajemen User
                <div class="page-title-subheading">Pengaturan semua user yang terdaftar pada aplikasi.
                </div>
            </div>
        </div>
          
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    <div class="card-body">
    

    @if  (Auth::user()->role_id == 4) 

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            
            <div class="panel panel-default">


                <div class="panel-heading">Data User</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username</label>

                             <div class="col-md-6">
                                <input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" readonly>

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" readonly>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" readonly>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ Auth::user()->address }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ Auth::user()->hp }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="phone" class="col-md-4 control-label"><a href="editUserPage/{{ Auth::user()->name }}"  class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> </label>
                        </div>


                    </form>
                </div>
                
            </div>
        </div>
    </div>

    @endif

    @if  (Auth::user()->role_id != 4) 


    <h5 class="card-title mb-5">Daftar User</h5>
    
    @if  (Auth::user()->role_id == 1 || Auth::user()->role_id == 3) 
        <a href="{{route('add.page')}}" class="btn btn-xs btn-alternate"><i class="fa fa-plus-circle"></i> Tambah User</a>
    @endif
    {{-- <a href="{{route('get.excel')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
    <a href="{{route('get.json')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
    <br/><br/>
    {{-- <div class="overflow-auto"> --}}
    <div class="overflow-auto">
        <table class="table table-striped table-hover" id="users-table" style="background-color: white;">
            <thead>
                <tr>
                    <th></th>
                    <th><input type="text" size="10" id="input1" style="font-size: 12px; font-weight: 500;" placeholder="Search Name" onkeyup="searchTable(1)"></th>
                    <th><input type="text" size="10" id="input2" style="font-size: 12px; font-weight: 500;" placeholder="Search Email" onkeyup="searchTable(2)"></th>
                    <th><input type="text" size="12" id="input3" style="font-size: 12px; font-weight: 500;" placeholder="Search Address" onkeyup="searchTable(3)"></th>
                    <th><input type="text" size="10" id="input4" style="font-size: 12px; font-weight: 500;" placeholder="Search Phone" onkeyup="searchTable(4)"></th>
                    <th><input type="text" size="12" id="input5" style="font-size: 12px; font-weight: 500;" placeholder="Search Jabatan" onkeyup="searchTable(5)"></th>
                    <th><input type="text" size="10" id="input6" style="font-size: 12px; font-weight: 500;" placeholder="Search Role" onkeyup="searchTable(6)"></th>
                    <th></th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Kelurahan</th>
                    <th>Phone</th>
                    <th>Referensi</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>


    @endif

    <!-- hide datatables default search bar -->
    <style type="text/css"> 
        .dataTables_filter{display: none}
    </style>

    <script>
    $(function() {
        var table = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            //bFilter: false,
            ajax: '{!! route('get.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'name', name: 'name' , searchable: true},
                { data: 'email', name: 'email' },
                { data: 'address', name: 'address' },
                { data: 'kelurahan', name: 'kelurahan' },
                { data: 'hp', name: 'hp' },
                { data: 'ketua_uppa', name: 'ketua_uppa' },
                { data: 'role', name: 'role_id' },
                { data: null, name: 'action', render: function ( data, type, row ) {
                    var btn = '<div style="text-align: center"><a href="viewUserPage/' + data.id + '" class="btn btn-xs btn-info" title="View"><i class="fa fa-search"></i></a> ';
                    if(data.role_id == 1) {
                        btn += '<a href="editUserPage/' + data.id + '" class="btn btn-xs btn-warning ml-1" title="Edit"><i class="fa fa-edit"></i></a> ' + 
                                '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger ml-1" onclick=\'confirmDel(' + data.id + ',"' + data.name + '")\' title="Delete"><i class="fa fa-trash"></i></a>' + 
                                '<form class="delete" action="delUser/' + data.id + '" method="get">' + 
                                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' + 
                                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                                '</form></div>';
                    }                        
                    return btn;
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function searchTable(col) {        
        var table = $('#users-table').DataTable();
        var input = '#input' + col;
        var val = $(input).val();
        table.columns(col).search(val).draw();
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nName: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

    </div>
</div>

@endsection
