<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Masuk PasarDepok.com</title>

    <script src="{{ asset('js/app.js') }}"></script>

    {{-- <link rel="stylesheet" href="{{ asset('assets/bootstrap') }}/css/bootstrap.min.css"> --}}
    
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/css/style.css">

    <link rel="stylesheet" href="{{ asset('assets/fontawesome-5.13.0/css/all.css') }}" >
</head>
<body>

<style>
body {
 background-color: #ff8934;
}

.my-container {
    position: absolute;
    overflow: hidden;
    background-image: url("{{ asset('assets/images/market.jpg') }}");
    width: 100%;
    height: 100%;
    opacity: 0.1;
    z-index: -1;
}

.container {
    max-width: 800px;
}

.signin-form {
    margin-right: auto;
    margin-left: auto;
    width: 70%;
}

@media only screen and (max-width: 1024px){
    .main {
        padding: auto 0 auto;
    }

    .form-group {
        margin-bottom: 10px;
    }

    .form-submit {
        margin-top: 5px;
        padding: 6px 30px;;
    }

    img {
        max-width: 170px;
    }

    .sign-img-mob {
        display: block;
        margin-bottom: 10px;
    }

    .signin-image {
        display: none;
    }

    .signin-content {
        padding-top: 25px;
        padding-bottom: 25px;
    }

    .login-title {
        font-size: x-large;
    }

    .img-mob {
        max-width: 70%;
    }
}

@media only screen and (max-width: 700px){
    .img-mob {
        max-width: 100%;
    }
}

.alert {
    position: relative;
    padding: .75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}

.alert-success {
    color: #1e6641;
    background-color: #d8f3e5;
    border-color: #c8eedb;
}

.alert-success p {
    color: #3c763d;
}

.alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
}

.alert-danger p {
    color: #a94442;
}

.close {
    float: right;
    font-size: 1.5rem;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    opacity: .5;
}

</style>


<div class="my-container">
</div>

<div class="main">

    <!-- Sing in  Form -->
    <section class="sign-in">
        <div class="container">
            <div class="signin-content">
                {{-- <div class="signin-image">
                    <figure><img src="{{ asset('assets/images/logo_new.png') }}" alt="Masuk"></figure>
                </div> --}}

                <div class="signin-form">

                    @include('notif')
                
                    <figure class="sign-img-mob"><img src="{{ asset('assets/images/logo_new_long.png') }}" class="img-mob" alt=""></figure>
                
                    <h2 class="form-title login-title">Kebijakan Privasi</h2>
                    
                    <div>

                        <p>Kebijakan Privasi PasarDepok.com adalah penjelasan terkait data dan informasi pribadi Pengguna PasarDepok.com, meliputi:</p>
                        <ol>
                        <li>Hukum dan Peraturan yang berlaku;</li>
                        <li>Perolehan dan Perlindungan Data;</li>
                        <li>Penggunaan, Penyimpanan, Pemanfaatan, dan Pengolahan Data;</li>
                        {{-- <li>Penghapusan Data;</li> --}}
                        <li>Pembatasan Tanggung Jawab PasarDepok.com;</li>
                        <li>Perubahan atas Kebijakan Privasi;</li>
                        <li>Cookies; dan</li>
                        <li>Kontak PasarDepok.com.</li>
                        </ol>
                        </p>
                        <p>Referensi PasarDepok.com dalam Kebijakan Privasi ini dapat berarti PasarDepok.com, afiliasi PasarDepok.com, termasuk Perwakilan PasarDepok.com, yaitu penasihat keuangan, asuransi, hukum, akuntansi, atau penasihat PasarDepok.com lainnya yang menyediakan layanan profesional kepada PasarDepok.com, dan pihak ketiga yang bekerja sama dengan PasarDepok.com, yakni partner, vendor, dan subkontraktor.</p>
                        <p>Kebijakan Privasi ini merupakan bagian tidak terpisahkan dan merupakan satu kesatuan dengan <a href="https://www.PasarDepok.com/terms">Aturan Penggunaan PasarDepok.com</a></p>
                        <p>PasarDepok.com dari waktu ke waktu berhak memperbaharui Kebijakan Privasi ini agar tetap relevan dan terkini dengan teknologi yang berubah, hukum yang berlaku, praktik bisnis PasarDepok.com yang terus berkembang dan kebutuhan pengguna PasarDepok.com. Jika ada perubahan yang dibuat pada Kebijakan Privasi ini, PasarDepok.com akan memberikan keterangan “Tanggal Pengkinian Terakhir”. PasarDepok.com mengimbau kepada Pengguna PasarDepok.com untuk secara berkala meninjau Kebijakan Privasi ini agar tetap mendapat informasi tentang bagaimana PasarDepok.com mengelola data dan informasi pribadi Anda. Dengan menggunakan Layanan, Pengguna dianggap memberikan persetujuan eksplisit atas Kebijakan Privasi dan dengan terus menggunakan Layanan setelah  Kebijakan Privasi, Pengguna dianggap telah menyetujui Kebijakan Privasi yang diperbarui.</p>
                        <p><strong>HUKUM DAN PERATURAN YANG BERLAKU</strong></p>
                        <p>PasarDepok.com tunduk terhadap segala peraturan perundang-undangan dan kebijakan pemerintah Republik Indonesia yang berlaku, termasuk yang mengatur tentang informasi dan transaksi elektronik, penyelenggaraan sistem elektronik, dan perlindungan data pribadi Pengguna; termasuk segala peraturan pelaksana dan peraturan perubahan dari peraturan-peraturan tersebut yang mengatur dan melindungi penggunaan data dan informasi penting para Pengguna.</p>
                        <p><strong>PEROLEHAN DAN PERLINDUNGAN DATA</strong></p>
                        <ol>
                        <li>
                        <p>PasarDepok.com berhak meminta data dan informasi Pengguna, antara lain sebagai berikut:<br>
                        a. Informasi terkait akun PasarDepok.com<br>
                        (i) Nama<br>
                        (ii) Username<br>
                        (iii) E-mail<br>
                        (iv) Nomor Telefon<br>
                        (v) Alamat dan/atau lokasi<br>
                        (vi) Profil, antara lain Tanggal lahir, Jenis kelamin<br>
                        (vii) Foto Diri (Selfie)<br>
                        </p>
                        <p>b. Perilaku Pengguna di PasarDepok.com dan/atau selama menggunakan Layanan<br>
                        Informasi pilihan produk, fitur, dan layanan<br>
                        (i) Informasi preferensi dan minat Pengguna<br>
                        (ii) Informasi pengalaman Pengguna<br>
                        (iii) Informasi foto dan media pada perangkat Pengguna<br>
                        (iv) Informasi hasil survei yang dikirimkan oleh  PasarDepok.com  atau atas nama PasarDepok.com<br>
                        (v) Rekam jejak Pengguna (log) yang  meliputi  namun tidak terbatas pada alamat IP perangkat, tanggal, dan waktu akses, fitur aplikasi, lamaan website yang  dilihat, proses kerja aplikasi dan aktivitas sistem lainnya, jenis web browser, dan/atau situs atau situs layanan dari pihak ketiga yang digunakan sebelum berinteraksi dengan situs<br>
                        (vi) Informasi lain Pengguna yang meliputi namun tidak terbatas pada aktivitas pendaftaran, login, transaksi, dan lain -  lain</p>
                        <p>c. Informasi yang berasal dari pihak ketiga<br>
                        Informasi Pribadi yang berasal dari pihak ketiga  yang bekerja sama dengan PasarDepok.com seperti vendor, agen, penasihat profesional, konsultan, dan pihak lain dapat kami kumpulkan sehubungan dengan  tujuan dari keterlibatan pihak ketiga.</p>
                        <p>Dengan memberikan informasi pada huruf a sampai c di atas, Pengguna melepaskan hak atas klaim, kerugian, tuntutan, dan gugatan yang mungkin terjadi atas perolehan, penyimpanan,penggunaan, pemanfaatan, dan/atau pengungkapan informasi, termasuk informasi pribadi, dalam Sistem PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>Pengguna dengan ini memberikan persetujuan eksplisit atas perolehan, penyimpanan, penggunaan, pemanfaatan, dan/atau pengungkapan data dan informasi pribadi yang Pengguna sampaikan (input) ke dalam Sistem PasarDepok.com, termasuk data pribadi Pengguna, foto Produk, chat dan percakapan dalam sistem PasarDepok.com dan Pengguna dengan ini menyatakan bahwa pemberian persetujuan atas data dan informasi pribadi tersebut dilakukan tanpa paksaan, dalam kondisi cakap, dan dengan keadaan sadar, dimana persetujuan tersebut Pengguna berikan saat membuat akun PasarDepok.com (“Persetujuan Pengguna”).</p>
                        </li>
                        <li>
                        <p>Atas Persetujuan Pengguna, Pengguna dengan ini menyatakan seluruh data dan informasi tersebut adalah milik PasarDepok.com. Untuk menghindari keragu-raguan, data pribadi Pengguna, baik yang dimasukkan ke dalam Sistem PasarDepok.com maupun yang tidak dimasukkan ke dalam Sistem PasarDepok.com, adalah milik Pengguna sebagai bagian dari hak pribadi secara utuh.</p>
                        </li>
                        <li>
                        <p>Pengguna dengan ini menyatakan data pribadi yang Pengguna sampaikan (input) ke dalam Sistem PasarDepok.com (termasuk dokumen yang diunggah dan/atau diberikan kepada PasarDepok.com) adalah asli, autentik, benar, akurat, lengkap, tidak menyesatkan, terbaru, dan merupakan hak pribadi Pengguna dan/atau dirinya berwenang untuk menyampaikan data pribadi Pengguna untuk menyampaikannya ke dalam Sistem PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com melindungi segala informasi yang diberikan Pengguna pada saat Pengguna menggunakan seluruh layanan PasarDepok.com, termasuk menjaga rahasia, keutuhan, dan ketersediaan data pribadi yang dikelolanya.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak dari waktu ke waktu meminta authentifikasi dan pemukhtahiran data pribadi Pengguna, sehingga data dan informasi Pengguna akurat, lengkap, dan terbaru, termasuk memberhentikan Layanan sementara dan/atau memberhentikan Layanan tetap, dalam hal Pengguna belum melakukan authentifikasi dan pemukhtahiran data pribadi Pengguna.</p>
                        </li>
                        <li>
                        <p>Pengguna berhak dari waktu ke waktu melakukan pemukhtahiran data pribadi Pengguna.</p>
                        </li>
                        <li>
                        <p>Pengguna dengan ini memahami bahwa pemanfaatan beberapa fitur tertentu dalam PasarDepok.com memerlukan proses authentifikasi data pribadi lebih lanjut dan tunduk pada Aturan Penggunaan PasarDepok.com serta syarat dan ketentuan fitur tertentu di PasarDepok.com tersebut. Pengguna dengan ini menegaskan Persetujuan Pengguna adalah persetujuan eksplisit atas perolehan, penyimpanan, penggunaan, pemanfaatan, dan/atau pengungkapan data pribadi yang Pengguna sampaikan (input) ke dalam Sistem PasarDepok.com dan fitur tertentu di PasarDepok.com tersebut, serta menyatakan bahwa pemberian penggunaan data pribadi tersebut dilakukan tanpa paksaan, dalam kondisi cakap, dan dengan keadaan sadar.</p>
                        </li>
                        </ol>
                        <p><strong>PENGGUNAAN, PENYIMPANAN, PEMANFAATAN, DAN PENGOLAHAN DATA</strong></p>
                        <ol>
                        <li>
                        <p>PasarDepok.com berhak menggunakan data dan informasi pribadi Pengguna demi meningkatkan mutu dan pelayanan di PasarDepok.com sesuai dengan ketentuan perundang-undangan yang berlaku dan berdasarkan Persetujuan Pengguna.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan informasi umum dan penggunaan layanan yang telah dikumpulkan untuk verifikasi data Pengguna</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan informasi transaksi untuk melakukan monitoring dan mengetahui pola transaksi Pengguna, serta untuk keperluan administrasi dan kepentingan penyelidikan atau kepentingan lainnya sebagaimana yang diwajibkan oleh aturan perundang-undangan</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan dan menyimpan informasi nama bank yang didalamnya termasuk nama dan nomor rekening, nama dan/atau nomor kartu kredit untuk memastikan pembayaran/penerimaan oleh Pengguna PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan informasi pribadi Pengguna secara keseluruhan untuk  keperluan know-your-customer (“KYC”), pemprosesan data internal untuk memastikan layanan berfungsi secara teknis.</p>
                        </li>
                        <li>
                        <p>Informasi Pengguna yang dikumpulkan dapat digunakan untuk mematuhi peraturan perundang-undangan yang berlaku,   penyelidikan dan penyelesaian sengketa, dan kegiatan ilegal lainnya, termasuk jika ada perselisihan dengan pihak ketiga.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan, memanfaatkan, dan mengungkapkan data dan informasi pribadi Pengguna berdasarkan Persetujuan Pengguna, yang telah Pengguna berikan pada saat perolehan data pribadi, kecuali terjadi kondisi sebagai berikut, antara lain:</p>
                        <p>a. diwajibkan dan/atau diminta oleh institusi yang berwenang berdasarkan ketentuan hukum yang berlaku, somasi, perintah resmi dari Pengadilan, dan/atau perintah resmi dari instansi atau aparat yang bersangkutan, termasuk namun tidak terbatas pada perselisihan, penyelidikan, penyidikan, proses hukum dan proses penyelesaian sengketa antara PasarDepok.com dengan Pengguna, antar-Pengguna, dan Pengguna dengan pihak lainnya serta kegiatan ilegal lainnya;</p>
                        <p>b. diwajibkan dan/atau diminta oleh institusi perbankan, keuangan yang berwenang, dan/atau pihak ketiga berdasarkan permintaan resmi dan ketentuan hukum yang berlaku;</p>
                        <p>c. untuk memproses segala bentuk aktivitas Pengguna yang termasuk namun tidak  terbatas pada memproses transaksi, verifikasi pembayaran, pengiriman produk dan lain – lain;</p>
                        <p>d. PasarDepok.com dapat bekerja sama dengan  perusahaan dan pihak ketiga lainnya yang memfasilitasi  atau memberikan bantuan dalam pengembangan aplikasi dan layanan-layanan tertentu untuk dan/atau atas nama PasarDepok.com, meliputi:<br>
                        (i) memberikan bantuan Pengguna;<br>
                        (ii) memberikan informasi geo-location;<br>
                        (iii) melaksanakan layanan-layanan terkait dengan Aplikasi (termasuk namun tidak terbatas pada layanan pemeliharaan, pengelolaan database, analisis  dan penyempurnaan fitur-fitur Aplikasi;<br>
                        (iv) membantu PasarDepok.com dalam menganalisa bagaimana Aplikasi dan Layanan digunakan serta bagaimana pengembangannya; atau<br>
                        (v) untuk membantu penasihat profesional dan auditor eksternal PasarDepok.com, termasuk penasihat hukum, penasihat keuangan, dan konsultan-konsultan. Para pihak ketiga ini hanya memiliki akses atas Informasi Pribadi Pengguna untuk melakukan tugas-tugas tersebut untuk dan/atau atas nama PasarDepok.com dan secara kontraktual terikat untuk tidak mengungkapkan atau menggunakan Informasi Pribadi tersebut untuk tujuan lain apapun;</p>
                        <p>e. Dalam pengungkapan informasi, PasarDepok.com dapat menginformasikan sebagian informasi pribadi Pengguna sehingga informasi tersebut menjadi informasi yang tidak dapat mengidentifikasi Pengguna secara keseluruhan (data agregat/data anonim) yang ditujukan untuk pihak ketiga yang digunakan untuk pengembangan Aplikasi, pengelolaan database, analisa atau peningkatan layanan, promosi dan periklanan;</p>
                        <p>f. PasarDepok.com  juga dapat mengkombinasikan Informasi Pribadi Pengguna dengan informasi-informasi lain sedemikan rupa sehingga informasi tersebut tidak lagi ter-asosiasi dengan Pengguna, kemudian mengungkapkan informasi yang telah dikombinasikan tersebut kepada pihak ketiga, untuk tujuan-tujuan seperti pada poin e;</p>
                        <p>g. PasarDepok.com  juga dapat mengkombinasikan Informasi Pribadi Pengguna dengan informasi-informasi lain sedemikan rupa sehingga informasi tersebut tidak lagi ter-asosiasi dengan Pengguna, kemudian mengungkapkan informasi yang telah dikombinasikan tersebut kepada pihak ketiga, untuk tujuan-tujuan seperti pada poin e;</p>
                        <p>h. PasarDepok.com dapat mengungkapan informasi Pengguna kepada anak perusahaan, afiliasi, partner, mitra, dan vendor  PasarDepok.com untuk membantu memberikan layanan atau pengelolaan data;</p>
                        <p>i. dalam keadaan darurat yang mengancam kehidupan, kesehatan atau keselamatan seseorang;</p>
                        <p>j. bila diperlukan untuk tujuan yang jelas untuk kepentingan Pengguna dan Persetujuan Pengguna tidak dapat diperoleh tepat waktu; atau</p>
                        <p>k. yang mungkin diperlukan atau diizinkan oleh hukum.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menggunakan, menyimpan, memanfaatkan, dan mengungkapkan data pribadi berdasarkan Persetujuan Pengguna yang telah Pengguna berikan pada saat perolehan data pribadi untuk:</p>
                        <p>a. menyediakan Layanan dan/atau Penawaran Pihak Ketiga, misalnya:<br>
                        (i) untuk keperluan yang terkait dengan pembayaran, penagihan, aktivasi, penyediaan, pemeliharaan, dukungan, pemecahan masalah, penyelesaian sengketa, penonaktifan, penggantian, peningkatan atau pembaharuan Layanan;<br>
                        (ii) untuk lebih memahami, menganalisis, dan memprediksi preferensi dan minat Pengguna, dan menggabungkan informasi tersebut dengan informasi lain untuk menyesuaikan pengalaman Pengguna;<br>
                        (iii) untuk memastikan Layanan berfungsi secara teknis sebagaimana dimaksud dan untuk membantu mengidentifikasi dan memecahkan masalah;<br>
                        (iv) untuk memfasilitasi akses Pengguna dan penggunaan Layanan dan / atau Penawaran Pihak Ketiga;<br>
                        (v)  untuk mengirim Penawaran berlangganan, pesan dan posting dalam Layanan atas nama administrator Saluran atau Akun Resmi;<br>
                        (vi) untuk menggabungkan informasi tersebut dengan informasi yang diperoleh dari sumber lain (termasuk Penawaran Pihak Ketiga) sehubungan dengan penyediaan Layanan;<br>
                        (vii) untuk memenuhi atau memberlakukan Pemberitahuan yang berlaku untuk Layanan;<br>
                        (viii) untuk mengelola atau menanggapi pertanyaan Pengguna;</p>
                        <p>b. mengembangkan Layanan baru dan meningkatkan Layanan yang ada dan memberikan Pengguna informasi tentang konten pihak ketiga atau produk terkait, layanan dan perangkat lunak termasuk untuk berkomunikasi dengan Pengguna tentang berbagai cara yang mereka gunakan, misalnya untuk menyediakan atau mengirim kepada Pengguna:<br>
                        (i) peningkatan atau pembaruan, atau pemberitahuan tentang peningkatan atau pembaruan, dari Layanan atau konten pihak ketiga atau produk, layanan dan perangkat lunak terkait;<br>
                        (ii) pemberitahuan promosi, kontes, penawaran, dan acara mendatang;<br>
                        (iii) informasi pribadi, survei, materi pemasaran, iklan atau konten yang disesuaikan;</p>
                        <p>c. mengumpulkan, menggunakan dan mengungkapkan untuk kegiatan promosi dan periklanan yang meliputi namun tidak terbatas pada pengidentifikasi iklan, termasuk yang disediakan oleh platform perangkat lunak lain, atau pengenal serupa, untuk memfasilitasi PasarDepok.com dalam melakukan analitik atau penyediaan konten promosi atau informasi lain yang mungkin relevan bagi Pengguna;</p>
                        <p>d. mengelola dan mengembangkan bisnis dan operasional PasarDepok.com, misalnya:<br>
                        (i) untuk mendeteksi, memantau, menyelidiki, mengurangi atau mencegah penipuan dan masalah teknis atau keamanan atau untuk melindungi properti dari PasarDepok.com;<br>
                        (ii) untuk memungkinkan kelangsungan bisnis dan operasi pemulihan bencana;<br>
                        (iii) untuk mendapatkan layanan hukum, untuk mencari nasihat hukum dan / atau untuk menegakkan hak hukum PasarDepok.com atau hak hukum dari anggota lain dari PasarDepok.com;<br>
                        (iv) untuk keperluan statistik;</p>
                        <p>e. memenuhi persyaratan hukum dan peraturan dan untuk menanggapi situasi darurat, misalnya:<br>
                        (i) untuk menanggapi perintah pengadilan, somasi atau permintaan yang sah atau penegakan hukum lainnya atau proses hukum;<br>
                        (ii) untuk memberikan bantuan darurat dalam situasi yang dapat mengancam kehidupan atau keselamatan fisik Anda atau orang lain; atau</p>
                        <p>f. Kepentingan internal PasarDepok.com untuk pengembangan bisnis, produk, mutu, dan pelayanan termasuk kegiatan promosi, studi, riset, dan kerja sama PasarDepok.com dengan pihak ketiga lainnya;</p>
                        <p>g. Kepentingan PasarDepok.com, afiliasi PasarDepok.com, perusahaan grup, dan anak perusahaan PasarDepok.com, untuk pengembangan bisnis, produk, mutu, dan pelayanan termasuk kegiatan promosi, studi, riset, dan kerja sama PasarDepok.com dengan pihak ketiga lainnya;</p>
                        <p>h. Kepentingan PasarDepok.com untuk memenuhi tujuan Transaksi atas nama PasarDepok.com, antara lain dalam hal penjualan semua atau sebagian aset PasarDepok.com termasuk setiap pembelian, penjualan, penyewaan, merger atau amalgamasi atau akuisisi, pembuangan atau pembiayaan PasarDepok.com, afiliasi PasarDepok.com, perusahaan grup, dan anak perusahaan PasarDepok.com;</p>
                        <p>i. Tujuan lain yang disampaikan dari waktu ke waktu oleh PasarDepok.com kepada Pengguna, serta diizinkan atau diwajibkan oleh hukum dan peraturan yang berlaku.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak menyimpan data dan informasi pribadi Pengguna untuk memenuhi tujuan Layanan, tujuan Kebijakan Privasi ini, tujuan peraturan perundang-undangan dan hukum yang berlaku, termasuk memproses, mentransfer, dan/atau menyimpan data dan informasi para Pengguna, baik di dalam wilayah Republik Indonesia maupun di luar wilayah Republik Indonesia, dengan memperhatikan Regulasi, termasuk peraturan perundang-undangan dan kebijakan Privasi di negara mana data dan informasi para Pengguna diproses, ditransfer, dan/atau disimpan.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com, afiliasi PasarDepok.com, perusahaan grup, dan anak perusahaan PasarDepok.com, termasuk Perwakilan PasarDepok.com, dan pihak ketiga yang bekerja sama dengan PasarDepok.com, dalam menyediakan Layanan berhak memanfaatkan dan mengolah data dan informasi pribadi Pengguna untuk memenuhi tujuan Layanan, tujuan Kebijakan Privasi ini, tujuan peraturan perundang-undangan dan hukum yang berlaku. Pemanfaatan dan pengolahan data tersebut dicakup oleh perjanjian dan memuat klausul kerahasiaan yang mengikat para pihak. Dalam hal ini, termasuk Perwakilan PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com berhak memanfaatkan dan mengolah data dan informasi para Pengguna untuk memenuhi tujuan Transaksi atas nama PasarDepok.com, antara lain dalam hal penjualan semua atau sebagian aset PasarDepok.com termasuk setiap pembelian, penjualan, penyewaan, merger atau amalgamasi atau akuisisi, pembuangan atau pembiayaan PasarDepok.com, afiliasi PasarDepok.com, perusahaan grup, dan anak perusahaan PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com dalam pemanfaatan dan pengolahan data, termasuk transmisi data, sesuai dengan tujuan Kebijakan Privasi ini akan menerapkan keamanan sewajarnya, sesuai dengan peraturan perundang-undangan dan hukum yang berlaku, termasuk memberikan enkripsi pada pemanfaatan dan pengolahan data tersebut.</p>
                        </li>
                        </ol>
                        {{-- <p><strong>PENGHAPUSAN DATA</strong></p>
                        <ol>
                        <li>
                        <p>Atas beragam informasi promo terbaru dan penawaran eksklusif, Pengguna dapat:<br>
                        a. berhenti berlangganan (unsubscribe) jika tidak ingin menerima informasi tersebut melalui klik tautan “berhenti berlangganan” melalui e-mail yang dikirimkan PasarDepok.com kepada Pengguna; atau<br>
                        b. melakukan pengaturan notifikasi “Promo” pada aplikasi PasarDepok.com.</p>
                        </li>
                        <li>
                        <p>Pengguna dapat menarik Persetujuan Pengguna berikan terkait dengan penggunaan data dan informasi pribadi Pengguna, termasuk berhenti menggunakan, mengakses Layanan, dan/atau menutup akun dengan memperhatikan ketentuan peraturan Perungdang-Undangan yang berlaku (termasuk ketentuan retensi yang diwajibkan oleh perundang-undangan yang berlaku). Pengguna dengan ini menyatakan bahwa Pengguna memahami konsekuensi dari penarikan Persetujuan Pengguna, termasuk namun tidak terbatas pada tidak lagi dapat menikmati Layanan.</p>
                        </li>
                        <li>
                        <p>Pengguna dapat mengajukan penghapusan data kepada PasarDepok.com melalui dengan melampirkan:<br>
                        a.salinan penetapan pengadilan;<br>
                        b. bukti diri yang sah (KTP/SIM/PASPOR) sesuai dengan identitas pribadinya di halaman akun PasarDepok.com; dan/atau<br>
                        c. alasan permintaan penghapusan</p>
                        </li>
                        <li>
                        <p>PasarDepok.com menyetujui penghapusan data yang diajukan oleh Pengguna jika telah memenuhi ketentuan pada angka 3 di atas dan telah memenuhi hak dan kewajibannya di PasarDepok.com, dalam waktu yang wajar.</p>
                        </li>
                        <li>
                        <p>PasarDepok.com atas permintaan Pengguna dapat mengumpulkan data dan informasi Pengguna dibuat tidak dikenali.</p>
                        </li>
                        </ol> --}}
                        <p><strong>PEMBATASAN TANGGUNG JAWAB PasarDepok.com</strong></p>
                        <ol>
                        <li>Pengguna bertanggung jawab atas keamanan dan mitigasi pelanggaran atas akun PasarDepok.com Pengguna sendiri, seperti menerapkan pengamanan yang tepat, membatasi akses, membuat kata sandi yang kuat, menjaga kata sandi, menjaga one time password (“OTP”).</li>
                        <li>PasarDepok.com tidak bertanggung jawab atas pertukaran dan pemberian data dan informasi pribadi Pengguna yang dilakukan sendiri oleh Pengguna, termasuk yang dilakukan antar-Pengguna. Termasuk dalam ketentuan ini adalah setiap dan segala kesalahan Pengguna yang mengakibatkan kebocoran data Pengguna.</li>
                        <li>PasarDepok.com bertanggung jawab atas Sistem PasarDepok.com, termasuk perlindungan serta pengamanan rahasia data pribadi, termasuk memberitahukan Pengguna dalam hal terjadi kegagalan dalam perlindungan data pribadi melalui sekurang-kurangnya melalui e-mail Pengguna yang terdaftar pada Sistem PasarDepok.com dan melaporkan kepada aparat penegak hukum atau Instansi Pengawas dan Pengatur Sektor terkait. Dalam hal terjadi kebocoran data, pihak yang bertanggung jawab untuk menginformasikan kepada Pengguna terkait kegagalan dalam perlindungan data tersebut adalah pihak atau institusi dimana data tersebut diproses berdasarkan Kebijakan Privasi ini.</li>
                        <li>PasarDepok.com tidak bertanggung jawab atas keaslian, keautentikan, kebenaran, keakuratan, kelengkapan data pribadi yang dimasukkan oleh Pengguna ke dalam Sistem PasarDepok.com.</li>
                        <li>Dengan memberikan Persetujuan Pengguna, Pengguna melepaskan hak atas klaim, kerugian, tuntutan, dan gugatan yang mungkin terjadi atas perolehan, penyimpanan,penggunaan, pemanfaatan, dan/atau penggungkapan data, termasuk data pribadi, dalam Sistem PasarDepok.com.</li>
                        <li>Dalam hal Pengguna adalah anak belum dewasa, PasarDepok.com tidak bertanggung jawab atas input data pribadi Pengguna anak dan menganjurkan agar orang tua/wali anak memantau penggunaan internet anak, sehingga pemberian data pribadi Pengguna anak diberikan dan/atau dalam pengawasan orang tua/wali sebagai pihak yang berwenang.</li>
                        <li>PasarDepok.com tidak bertanggung jawab atas kebocoran data yang terjadi diakibatkan dan/atau terjadi selama karena Keadaan Memaksa. Keadaan Memaksa meliputi antara lain (i) pemogokan, penutupan perusahaan, dan aksi industrial lain; (ii) huru-hara, kerusuhan, invasi, serangan atau ancaman teroris, perang (baik yang dinyatakan atau tidak) atau ancaman persiapan perang; (iii) kebakaran, ledakan, badai, banjir, gempa bumi, epidemi atau bencana alam lain; (iv)  tidak tersedianya atau terganggunya  jaringan telekomunikasi, informatika dan/atau listrik; (v) terdapatnya kegagalan sistem yang diakibatkan pihak ketiga yang terjadi di luar wewenang PasarDepok.com; (vi) tidak berfungsinya sistem dan/atau jaringan perbankan, (vii) tindakan, putusan, undang-undang, peraturan atau pembatasan yang diterbitkan pemerintah. Dalam hal terjadi Keadaan Memaksa, PasarDepok.com memberitahukan Pengguna paling lambat 14 (empat belas) Hari Kalendar setelah terjadinya Keadaan Kahar tersebut dan berusaha dengan kemampuan komersial terbaiknya memenuhi kewajiban PasarDepok.com berdasarkan Kebijakan Privasi ini.<br>
                        <br></li>
                        </ol>
                        <p><strong>COOKIES</strong></p>
                        <ol>
                        <li>Cookies adalah tempat penyimpanan data yang kecil terdapat pada komputer atau perangkat lain seperti ponsel pintar atau tablet, pada saat Pengguna menelusuri dan/atau mengunjungi Situs PasarDepok.com.</li>
                        <li>Komputer atau perangkat lain Pengguna akan secara otomatis menerima Cookies pada saat Pengguna menelusuri dan/atau mengunjungi Situs PasarDepok.com serta saat Pengguna menggunakan layanan PasarDepok.com (“Penggunaan Cookies”). Namun, Pengguna dapat  menentukan pilihan untuk melakukan modifikasi atau memilih untuk menolak Penggunaan Cookies melalui preferensi pengaturan web browser Pengguna.</li>
                        <li>PasarDepok.com menggunakan  Google Analytics (“Fitur”), dimana data yang diperoleh dari penggunaan Fitur tersebut meliputi, IP address Pengguna, jenis perangkat Pengguna, dan lain – lain (“Data”). Data tersebut digunakan oleh PasarDepok.com untuk pengembangan Situs dan Konten PasarDepok.com serta pengiklanan. Pengguna dapat memilih untuk tidak terakses oleh Fitur dilakukan dengan cara mengunduh Google Analytics Opt-Out Add-on pada web browser Pengguna.</li>
                        <li>PasarDepok.com dapat memberikan data dan informasi Pengguna yang berasal dari dari Penggunaan Cookies kepada pihak ketiga, seperti data lokasi, pengidentifikasi iklan, atau alamat email yang digunakan untuk segmentasi periklanan, termasuk namun tidak terbatas pada untuk kebutuhan pemasaran dan periklanan, dimana data dan informasi tersebut tidak dapat diidentifikasi secara pribadi.</li>
                        <li>Pengguna dapat melakukan kontrol terhadap Penggunaan Cookies melalui preferensi pengaturan web browser Pengguna yaitu dengan modifikasi atau memilih untuk menolak Penggunaan Cookies. Namun dengan melakukan pengaturan tersebut, kinerja pelayanan pada saat akses ke Situs PasarDepok.com dapat terpengaruh, seperti fungsi dan halaman tertentu pada Situs PasarDepok.com tidak dapat bekerja optimal untuk layanan kepada Pengguna.<br>
                        <br></li>
                        </ol>
                        <p><strong>KONTAK PasarDepok.com</strong></p>
                        <p>Pengguna dapat menghubungi PasarDepok.com terkait Kebijakan Privasi ini melalui email admin@PasarDepok.com</a></p>
                        <p><strong>Pengkinian Terakhir: 21 Juni 2020</strong></p>

                    </div>


                </div>
            </div>
        </div>
    </section>
    
</div>


<!-- JS -->
<script src="{{ asset('assets/colorlib-regform-7') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets/colorlib-regform-7') }}/js/main.js"></script>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>