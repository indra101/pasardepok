<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Password</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/css/style.css">

    <link rel="stylesheet" href="{{ asset('assets/fontawesome-5.13.0/css/all.css') }}" >
</head>
<body>

<style>
body {
 background-color: #ff8934;
}

.my-container {
    position: absolute;
    overflow: hidden;
    background-image: url("{{ asset('assets/images/market.jpg') }}");
    width: 100%;
    height: 100%;
    opacity: 0.1;
    z-index: -1;
}

.sign-img-mob {
    display: none;
}

@media only screen and (max-width: 1024px){
    .main {
        padding: auto 0 auto;
    }

    .form-group {
        margin-bottom: 10px;
    }

    .form-submit {
        margin-top: 5px;
        padding: 6px 30px;;
    }

    img {
        max-width: 170px;
    }

    .sign-img-mob {
        display: block;
        margin-bottom: 10px;
    }

    .signin-image {
        display: none;
    }

    .signin-content {
        padding-top: 25px;
        padding-bottom: 25px;
    }

    .login-title {
        font-size: x-large;
    }

    .img-mob {
        max-width: 70%;
    }
}

@media only screen and (max-width: 700px){
    .img-mob {
        max-width: 100%;
    }
}

</style>


<div class="my-container">
</div>

<div class="main">

    <!-- Sing in  Form -->
    <section class="sign-in">
        <div class="container">
            <div class="signin-content">
                <div class="signin-image">
                    <figure><img src="{{ asset('assets/images/logo_new.png') }}" alt="Masuk"></figure>
                </div>

                <div class="signin-form">

                    <figure class="sign-img-mob"><img src="{{ asset('assets/images/logo_new_long.png') }}" class="img-mob" alt="Masuk"></figure>
                
                    <h2 class="form-title login-title">Reset Password</h2>
                    <form method="POST" class="register-form" id="login-form" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="email" id="email" placeholder="Email Anda" value="{{ old('email') }}" required autofocus/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('email') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="password" maxlength="100" placeholder="Password"/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('password') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation"><i class="zmdi zmdi-lock-outline"></i></label>
                            <input type="password" name="password_confirmation" id="password-confirm" maxlength="100" placeholder="Ulangi password"/>
                        </div>

                        <div class="form-group form-button">
                            <input type="submit" class="form-submit" value="Reset Password"/>
                        </div>
                        <div class="form-group">
                            <div>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    Kembali ke halaman Login
                                </a>
                            </div>
                            <div>
                                <a href="{{ route('register') }}" >Daftar akun baru</a>
                            </div>  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
</div>


<!-- JS -->
<script src="{{ asset('assets/colorlib-regform-7') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets/colorlib-regform-7') }}/js/main.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>