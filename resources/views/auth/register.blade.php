<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar PasarDepok.com</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/css/style.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-209751094-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-209751094-1');
    </script>
</head>
<body>


<style>

body {
    background-color: #ff8934;
}

.my-container {
    position: absolute;
    overflow: hidden;
    background-image: url("{{ asset('assets/images/market.jpg') }}");
    width: 100%;
    height: 100%;
    opacity: 0.1;
    z-index: -1;
}

select {
    padding: 5px;
    background-color: white;
    border-radius: 5px;
    margin-left: 30px;
    width: 85%;
}

.sign-img-mob {
    display: none;
}

@media only screen and (max-width: 1024px){
    .main {
        padding: 10px 0 0 !important;
    }

    .form-group {
        margin-bottom: 10px;
    }

    .form-submit {
        margin-top: 5px;
        padding: 6px 30px;;
    }

    img {
        max-width: 170px;
    }

    .sign-img-mob {
        display: block;
        margin-bottom: 10px;
    }

    .signup-image {
        display: none;
    }

    .signup-content {
        padding-top: 25px;
        padding-bottom: 25px;
    }

    .login-title {
        font-size: x-large;
        margin-bottom: 0;
    }

    .teks-title {
        font-size: x-small;
    }

    .img-mob {
        max-width: 70%;
    }

}

@media only screen and (max-width: 700px){
    .img-mob {
        max-width: 70%;
    }
}

</style>
    
    
<div class="my-container">
</div>

<div class="main">

    <!-- Sign up form -->
    <section class="signup">
        <div class="container">
            <div class="signup-content">
                <div class="signup-form">
                    <div class="form-title">
                    <figure class="sign-img-mob"><img src="{{ asset('assets/images/logo_new.png') }}" class="img-mob" alt="Masuk"></figure>
                
                    <h2 class="form-title login-title">Pendaftaran</h2>
                    <span class="teks-title">*Khusus untuk Warga Depok</span>
                    </div>

                    <form method="POST" class="register-form" id="register-form" action="{{ route('register') }}">
                    {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="name" id="name" maxlength="50" placeholder="Nama Anda" required/>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('name') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email"><i class="zmdi zmdi-email"></i></label>
                            <input type="email" name="email" id="email" maxlength="100" placeholder="Your Email" required/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('email') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="password" maxlength="100" placeholder="Password" required/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('password') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation"><i class="zmdi zmdi-lock-outline"></i></label>
                            <input type="password" name="password_confirmation" id="password-confirm" maxlength="100" placeholder="Ulangi password" required/>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address"><i class="zmdi zmdi-home"></i></label>
                            <input type="text" name="address" id="address" maxlength="200" placeholder="Alamat Rumah" required/>
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('address') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('kecamatan') ? ' has-error' : '' }}">
                            <label for="kecamatan"><i class="zmdi zmdi-city"></i></label>
                            
                                <select id="id_kecamatan" name="id_kecamatan" onchange="get_kelurahan()" required>
                                    <option value="">Pilih Kecamatan</option>
                                    @foreach($kecamatans as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('kecamatan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kecamatan') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
                            <label for="kelurahan"><i class="zmdi zmdi-city"></i></label>
                            
                                <select id="kelurahan" name="kelurahan" required>
                                    <option value="">Pilih Kelurahan</option>
                                    @foreach($kelurahans as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('kelurahan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelurahan') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
                            <label for="hp"><i class="zmdi zmdi-phone"></i></label>
                            <input type="number" name="hp" id="hp" maxlength="20" placeholder="Nomor HP" required/>
                            @if ($errors->has('hp'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('hp') }}</font>
                                </span>
                            @endif
                        </div>

                        {{-- <div class="form-group{{ $errors->has('ketua_uppa') ? ' has-error' : '' }}">
                            <label for="ketua_uppa"><i class="zmdi zmdi-account-o"></i></label>
                            <input type="text" name="ketua_uppa" id="ketua_uppa" maxlength="20" placeholder="Nama Referensi" required/>
                            @if ($errors->has('ketua_uppa'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('ketua_uppa') }}</font>
                                </span>
                            @endif
                        </div> --}}

                        {{-- <div class="form-group">
                            <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                            <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                        </div> --}}

                        <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" class="form-submit" value="Daftar"/>
                        </div>

                        <a href="{{ route('login') }}" class="signup-image-link">Saya sudah terdaftar</a>

                    </form>

                </div>
                <div class="signup-image">
                    <figure><img src="{{ asset('assets/images/logo_new.png') }}" alt="sing up image"></figure>
                </div>
            </div>
        </div>
    </section>

    

</div>

<!-- JS -->
<script src="{{ asset('assets/colorlib-regform-7') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets/colorlib-regform-7') }}/js/main.js"></script>

<script>

function get_kelurahan() {
    var id_kecamatan = $('#id_kecamatan').val();

    $.ajax({
        url: '{{ route("getKelurahan") }}',
        data: 'id_kecamatan=' + id_kecamatan,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#kelurahan').empty();
            $('#kelurahan').append('<option value="">Pilih Kelurahan</option>');
            $.each(data, function(key, value) {
                $('#kelurahan').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

</script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
