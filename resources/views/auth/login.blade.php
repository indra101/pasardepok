<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Masuk PasarDepok.com</title>

    <script src="{{ asset('js/app.js') }}"></script>

    {{-- <link rel="stylesheet" href="{{ asset('assets/bootstrap') }}/css/bootstrap.min.css"> --}}
    
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('assets/colorlib-regform-7') }}/css/style.css">

    <link rel="stylesheet" href="{{ asset('assets/fontawesome-5.13.0/css/all.css') }}" >

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-209751094-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-209751094-1');
    </script>

</head>
<body>

<style>
body {
 background-color: #ff8934;
}

.my-container {
    position: absolute;
    overflow: hidden;
    background-image: url("{{ asset('assets/images/market.jpg') }}");
    width: 100%;
    height: 100%;
    opacity: 0.1;
    z-index: -1;
}

.sign-img-mob {
    display: none;
}

@media only screen and (max-width: 1024px){
    .main {
        padding: auto 0 auto;
    }

    .form-group {
        margin-bottom: 10px;
    }

    .form-submit {
        margin-top: 5px;
        padding: 6px 30px;;
    }

    img {
        max-width: 170px;
    }

    .sign-img-mob {
        display: block;
        margin-bottom: 10px;
    }

    .signin-image {
        display: none;
    }

    .signin-content {
        padding-top: 25px;
        padding-bottom: 25px;
    }

    .login-title {
        font-size: x-large;
    }

    .img-mob {
        max-width: 70%;
    }
}

@media only screen and (max-width: 700px){
    .img-mob {
        max-width: 70%;
    }
}

.alert {
    position: relative;
    padding: .75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}

.alert-success {
    color: #1e6641;
    background-color: #d8f3e5;
    border-color: #c8eedb;
}

.alert-success p {
    color: #3c763d;
}

.alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
}

.alert-danger p {
    color: #a94442;
}

.close {
    float: right;
    font-size: 1.5rem;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    opacity: .5;
}

</style>


<div class="my-container">
</div>

<div class="main">

    <!-- Sing in  Form -->
    <section class="sign-in">
        <div class="container">
            <div class="signin-content">
                <div class="signin-image">
                    <figure><img src="{{ asset('assets/images/logo_new.png') }}" alt="Masuk"></figure>
                </div>

                <div class="signin-form">

                    @include('notif')
                
                    <figure class="sign-img-mob"><img src="{{ asset('assets/images/logo_new_long.jpeg') }}" class="img-mob" alt="Masuk"></figure>
                
                    <h2 class="form-title login-title">Silahkan Masuk</h2>
                    <form method="POST" class="register-form" id="login-form" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="email" id="email" placeholder="Email Anda" value="{{ old('email') }}" required autofocus/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('email') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="password" placeholder="Password" required/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <font color="red">{{ $errors->first('password') }}</font>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="checkbox" name="remember" id="remember-me" class="agree-term"  {{ old('remember') ? 'checked' : '' }}/>
                            <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                        </div>
                        <div class="form-group form-button">
                            <input type="submit" name="signin" id="signinBtn" class="form-submit" value="Masuk"/>
                            <button id="signinLoadBtn" class="form-submit" type="button" style="display: none;" disabled>
                                Loading...
                            </button>
                        </div>
                        <div class="form-group">
                            <div>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Anda lupa password?
                                </a>
                            </div>
                            <div style="margin-top: 10px;">
                                <a href="{{ route('register') }}" class="">Daftar akun baru</a>
                            </div>
                        </div>
                    </form>
                    {{-- <div class="social-login">
                        <span class="social-label">Or login with</span>
                        <ul class="socials">
                            <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                            <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                            <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                        </ul>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
    
</div>

<script>

$('#signinBtn').click(function() {
    // $('#signinBtn').hide()
    // $('#signinLoadBtn').show()

});

</script>


<!-- JS -->
<script src="{{ asset('assets/colorlib-regform-7') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets/colorlib-regform-7') }}/js/main.js"></script>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>