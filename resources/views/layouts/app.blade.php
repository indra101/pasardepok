<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.min.css') }}"/>
    <link href="{{ asset('assets/architectui/main.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/architectui/assets/scripts/main.js') }}"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/fontawesome-5.13.0/css/all.css') }}" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon.png') }}">

    <!-- TinyMCE -->
    <script src="https://cdn.tiny.cloud/1/db69nyuzq2xysj34ox4nk8gk5kcumh046megrw45yfdfwq49/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-209751094-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-209751094-1');
    </script>

</head>

<style>
.app-header .app-header__content .header-menu .nav-link {
    color: #171819;
}

.items {
    text-align: left; 
    box-shadow: 0 1px 3px 0 #6c757d; 
    border-radius: 4px;
    margin-left: 10px;
    margin-right: 10px;
    width: 200px;
    height: 300px;
}

.item-pict {
    text-align: center; 
    font-size: 60px; 
    background-color: white; 
    /* border-radius: 4px; */
    /* margin-bottom: 10px;  */
    /*height: 200px;*/
}

.item-pict img {
    width: 200px;
    height: 200px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}

@media only screen and (max-width: 700px){
    .items {
        width: 175px;
        height: 260px;
        font-size: 12px; 
        margin-left: 5px;
        margin-right: 5px;
    }

    .item-pict img {
        width: 175px;
        height: 175px;
    }

    .card-body {
        padding: 0.25rem;
    }
}

@media only screen and (max-width: 400px){
    .items {
        width: 150px;
        height: 260px;
        font-size: 12px; 
        margin-left: 5px;
        margin-right: 5px;
    }

    .item-pict img {
        width: 150px;
        height: 150px;
    }

    .card-body {
        padding: 0.25rem;
    }
}

@media only screen and (max-width: 320px){
    .items {
        width: 130px;
        height: 195px;
        font-size: 12px; 
        margin-left: 5px;
        margin-right: 5px;
    }

    .item-pict img {
        width: 130px;
        height: 130px;
    }

    .card-body {
        padding: 0.25rem;
    }
}

</style>

<body>

    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header" id="app">

        @auth
            
        @include('layouts.header')

        @endauth

        <div class="app-main" style="">

            @auth
            
            @include('layouts.sidebar')

            @endauth

                <div class="app-main__outer">

                    <div class="app-main__inner">

                        @yield('content')

                    </div>

                    @auth

                    @include('layouts.footer')

                    @endauth

                </div>
        </div>
    </div>

</body>
</html>
