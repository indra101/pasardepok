<style>

.app-header {
    /* height: 45px; */
}

.item_img:hover {
  /*background-color: #c3ad5c;*/
  filter: brightness(80%);
  transition: .3s ease;
}

.item_img {
    position: relative;
}

.badge {
    background-color: red;
    color: white;
    padding: 5px 1px;
    position: absolute;
    top: 2px;
    right: 10px;
    font-size: 65%;
}

.badge-beli-mob {
    display: none;
    position: relative;
}

.badge-pesan-mob {
    display: none;
    position: relative;
}

.mobile-toggle-header-nav {
    display: none;
}


.bg-warning {
    background-color: #fb501a !important;
}

.app-header.header-text-dark .header-btn-lg .hamburger-inner, .app-header.header-text-dark .header-btn-lg .hamburger.is-active .hamburger-inner, .app-header.header-text-dark .header-btn-lg .hamburger-inner::before, .app-header.header-text-dark .header-btn-lg .hamburger-inner::after, .app-header.header-text-dark .header__pane .hamburger-inner, .app-header.header-text-dark .header__pane .hamburger.is-active .hamburger-inner, .app-header.header-text-dark .header__pane .hamburger-inner::before, .app-header.header-text-dark .header__pane .hamburger-inner::after {
    background-color: rgb(255 255 255 / 80%) !important;
}

.search-input::placeholder {
    color: whitesmoke;
}


@media only screen and (max-width: 1024px){
    .popover, .dropdown-menu {
        top: 20% !important;
    }

    .search-input::placeholder {
        color: rgb(126, 126, 126);
    }
}

@media only screen and (max-width: 800px){
    .app-header {
        /* height: 40px; */
    }

    .app-header__logo {
        display: none;
    }

    .logo-src {
        display: none;
    }

    .app-header__menu {
        padding-left: 0;
        padding-right: 5px;
    }

    .search-input {
        width: 180px;
        padding: 4px;
        border-radius: 5px;
        border-color: white;
        background-color: whitesmoke;
        font-size: small;
    }

    .item_img {
        height: 28px;
    }

    .nav .badge {
        margin-left: 2px;
    }

    .link-beli {
        padding-left: .1rem;
    }

    .link-pesan {
        padding-right: .4rem;
    }

    .badge-beli-mob {
        display: block;
    }

    .badge-pesan-mob {
        display: block;
        right: 0;
    }

    .badge-pesan {
        right: 0;
    }

    .badge-beli-des {
        display: none;
    }

    .badge-pesan-des {
        display: none;
    }

    .popover, .dropdown-menu {
        top: 30% !important;
    }

    .hamburger-inner, .hamburger-inner::before, .hamburger-inner::after {
        background-color: rgb(255 255 255 / 80%) !important;
    }
}

</style>

@php
$role = Auth::user()->role_id;
@endphp

<div class="app-header header-shadow bg-warning header-text-light">
    <div class="app-header__logo">
        <a href="{{route('home')}}"><div class="logo-src" style="width: 190px; height: 40px;"></div></a>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu" style="padding-left: 0;">
        {{-- <div class="search-wrapper active" style="width: 200px;">
            <div class="input-holder" style="width: 200px;">
                <form class="form-horizontal" method="GET" action="{{ route('listShop') }}">
                    <input type="text" id="cari" class="search-input" name="search" placeholder="Cari Produk">
                    <input type="submit" id="submitBtn" style="display: none;"/>
                </form>
                <button id="btnSearch" class="search-icon"><span></span></button>
            </div>
            <button class="close"></button>
        </div> --}}

        <form class="form-horizontal" method="GET" action="{{ route('listShop') }}">
            <input type="text" id="cari" class="search-input" name="search" placeholder="Cari Produk">
            <input type="submit" id="submitBtn" style="display: none;"/>
        </form>

        @if($role == 1 || $role == 3)
        <div class="badge-pesan-mob">
            <a href="{{route('pesanan')}}" class="nav-link link-pesan">
                <i class="fa fa-store" style="font-size: large; color: whitesmoke; padding-top: 5px;"></i>
                <span id="jml_pesan_mob" class="badge badge-pill badge-light badge-pesan" style="display: none;">{{ session()->get('jml_pesanan') }}</span>
            </a>
        </div>
        @endif
        <div class="badge-beli-mob">
            <a href="{{route('pembelian')}}" class="nav-link link-beli">
                <i class="fa fa-shopping-cart" style="font-size: large; color: whitesmoke; padding-top: 5px;"></i>
                <span id="jml_beli_mob" class="badge badge-pill badge-light badge-beli" style="display: none;">{{ session()->get('jml_pembelian') }}</span>
            </a>
        </div>
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="app-header__content">
        <div class="app-header-left">
            <div class="search-wrapper">
                <div class="input-holder">
                    <form class="form-horizontal" method="GET" action="{{ route('listShop') }}">
                        <input type="text" id="cari" class="search-input" name="search" placeholder="Cari Produk">
                        <input type="submit" id="submitBtn" style="display: none;"/>
                    </form>
                    <button id="btnSearch" class="search-icon"><span></span></button>
                </div>
                <button class="close"></button>
            </div>
        </div>
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        @guest

                        <div class="widget-content-left">
                            <ul class="header-menu nav">
                                <li class="nav-item" style="font-size: small;">
                                    <a href="{{ route('login') }}" class="nav-link">
                                        <i class="nav-link-icon fa fa-database"> </i>
                                        LOGIN
                                    </a>
                                </li>
                                <li class="nav-item" style="font-size: small;">
                                    <a href="{{ route('register') }}" class="nav-link">
                                        <i class="nav-link-icon fa fa-edit"></i>
                                        REGISTER
                                    </a>
                                </li>
                            </ul>
                        </div>

                        @else

                        <div class="widget-content-left">
                            <ul class="header-menu nav">
                                @if($role == 1 || $role == 3)
                                <li class="nav-item">
                                    <div class="badge-pesan-des" style="position: relative;">
                                        <a href="{{route('pesanan')}}" class="nav-link">
                                            <i class="fa fa-store" style="font-size: large; color: whitesmoke; padding-top: 5px;"></i>
                                            <span id="jml_pesan" class="badge badge-pill badge-light badge-pesan" style="display: none;">{{ session()->get('jml_pesanan') }}</span>
                                        </a>
                                    </div>
                                    {{-- <font size="2rem">Pesanan</font> --}}
                                </li>
                                @elseif($role == 2)
                                <li class="nav-item mr-2" style="align-self: center;">
                                    <a href="" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_toko"><i class="fa fa-store"></i> Buka Toko</a>
                                </li>
                                @endif
                                <li class="nav-item mr-2">
                                    <div class="badge-beli-des" style="position: relative;">
                                        <a href="{{route('pembelian')}}" class="nav-link">
                                            <i class="fa fa-shopping-cart" style="font-size: large; color: whitesmoke; padding-top: 5px;"></i>
                                            <span id="jml_beli" class="badge badge-pill badge-light badge-beli" style="display: none;">{{ session()->get('jml_pembelian') }}</span>
                                        </a>
                                    </div>
                                    {{-- <font size="2rem">Pembelian</font> --}}
                                </li>
                            </ul>  
                        </div>
                        
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <img width="42" class="rounded-circle" src="{{ asset('assets/images/users/'.Auth::user()->foto) }}" alt="">
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('profile') }}" tabindex="0" class="dropdown-item">Profile</a>
                                    <a href="#" tabindex="0" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                </div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                        
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                {{ Auth::user()->name }}
                            </div>
                        </div>

                        @endguest
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div>        

@include('user.modal_toko')

<script>

tinymce.init({
    selector: 'textarea',
    //plugins: 'advlist a11ychecker advcode casechange image imagetools emoticons formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    plugins: 'advlist image imagetools emoticons lists media table',
    toolbar: 'undo redo | ' +
  'bold italic forecolor emoticons  | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | image | ' +
  'removeformat',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
});

$('#btnSearch').click(function() {
    var cari = $('#cari').val()
    if(cari)
        $('#submitBtn').click()
});

function update_jml_beli() {
    $.ajax({
        type: 'GET',
        url: '{!! route('getJmlPembelian') !!}',
        success: function (html) {
            if(html !== '0') {
                $('#jml_beli').html(html);
                $('#jml_beli').show(200);
                $('#jml_beli_mob').html(html);
                $('#jml_beli_mob').show(200);
            }
        }
    });

    setTimeout(update_jml_beli, 20000); //20 second
}

function update_jml_pesan() {
    $.ajax({
        type: 'GET',
        url: '{!! route('getJmlPesanan') !!}',
        success: function (html) {
            if(html !== '0') {
                $('#jml_pesan').html(html);
                $('#jml_pesan').show(200);
                $('#jml_pesan_mob').html(html);
                $('#jml_pesan_mob').show(200);
            }
        }
    });

    setTimeout(update_jml_pesan, 20000); //20 second
}

$(function() {
    $('#btnSearch').click();
    update_jml_beli()
    update_jml_pesan()
});

</script>