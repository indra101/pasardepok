<div class="app-wrapper-footer">
    <div class="app-footer">
        <div class="app-footer__inner">
            <div class="app-footer-left">
                <ul class="nav">
                    <li class="nav-item">
                        Copyright <i class="fa fa-copyright"></i> 2021 PasarDepok.com
                    </li>
                    {{-- <li class="nav-item">
                        <a href="javascript:void(0);" class="nav-link">
                            Footer Link 2
                        </a>
                    </li> --}}
                </ul>
            </div>
            <div class="app-footer-right">
                <ul class="nav">
                    <li class="nav-item">
                        
                            Versi 1.0
                        
                    </li>
                    {{-- <li class="nav-item">
                        <a href="javascript:void(0);" class="nav-link">
                            <div class="badge badge-success mr-1 ml-0">
                                <small>NEW</small>
                            </div>
                            Footer Link 4
                        </a>
                    </li> --}}
                </ul>
            </div>
        </div>
    </div>
</div>

<style>

.nav-footer {
    visibility: hidden;
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: white;
    text-align: center;
    height: 50px;
    z-index: 99;
}

.icon-teks {
    font-size: smaller;
}

.item-footer {
    width: 25%;
    
}

@media only screen and (max-width: 1000px){
    .nav-footer {
        visibility: visible;
    }
}

</style>

<div class="nav-footer d-flex justify-content-center border">
    <div class="item-footer p-1">
        <div>
            <a href="{{route('home')}}" style="color: black;"><i class="fa fa-home"></i></a>
        </div>
        <div class="icon-teks">
            Beranda
        </div>
    </div>
    <div class="item-footer p-1">
        <div>
            <a href="{{route('listShop')}}" style="color: black;"><i class="fa fa-shopping-bag"></i></a>
        </div>
        <div class="icon-teks">
            Katalog
        </div>
    </div>
    <div class="item-footer p-1">
        <div>
            <a href="{{route('listPenjual')}}" style="color: black;"><i class="fa fa-id-card"></i></a>
        </div>
        <div class="icon-teks">
            Daftar Penjual
        </div>
    </div>
    @php
    $role = Auth::user()->role_id;
    @endphp
    <div class="item-footer p-1">
        <div>
            @if($role == 2)
            <a href="{{route('profile')}}" style="color: black;"><i class="fa fa-user"></i></a>
            @else
            <a href="{{route('profilToko')}}" style="color: black;"><i class="fa fa-user"></i></a>
            @endif
        </div>
        <div class="icon-teks">
            Profil
        </div>
    </div>
</div>

<script>

$('.item-footer').click(function() {
    var item = $(this);
    item.css('background-color', 'grey');
    setTimeout(function() {
        item.css('transition', '.4s ease');
        item.css('background-color', 'white');
    }, 100);
});

</script>