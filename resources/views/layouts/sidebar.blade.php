<style>

.vertical-nav-menu li a {
    color: #4b4b4b !important;
}

.app-sidebar.sidebar-text-light .app-sidebar__heading {
    color: #4b4b4b;
}

</style>

<div class="app-sidebar sidebar-shadow bg-dark sidebar-text-light" style="background-color: #d8d8d8 !important;">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="scrollbar-sidebar overflow-auto">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                @php
                    if(empty($menu))
                        $menu = '';
                    @endphp
                <li>
                    @if ($menu == 'home')
                    <a href="{{route('home')}}" class="mm-active">
                    @else
                    <a href="{{route('home')}}">
                    @endif
                    <i class="metismenu-icon pe-7s-home"></i>
                        Beranda
                    </a>
                </li>

                @php
                $role = Auth::user()->role_id;
                @endphp

                @if($role == 1 || $role == 3)
                    <li class="app-sidebar__heading">Pengelolaan Toko</li>
                    <li>
                        @if ($menu == 'profilToko')
                        <a href="{{route('profilToko')}}" class="mm-active">
                        @else
                        <a href="{{route('profilToko')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-rocket"></i>
                            Profil Toko
                        </a>
                    </li>
                    
                    @if($role == 1)
                    <li>
                        @if ($menu == 'transaksi')
                        <a href="{{route('listTransaksi')}}" class="mm-active">
                        @else
                        <a href="{{route('listTransaksi')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-timer"></i>
                            Riwayat Transaksi
                        </a>
                    </li>
                    @endif
                @endif

                <li class="app-sidebar__heading">Katalog Produk</li>
                <li>
                    @if ($menu == 'toko')
                    <a href="{{route('listShop')}}" class="mm-active">
                    @else
                    <a href="{{route('listShop')}}">
                    @endif
                        <i class="metismenu-icon pe-7s-shopbag"></i>
                        Daftar Katalog
                    </a>
                </li>
                <li>
                    @if ($menu == 'penjual')
                    <a href="{{route('listPenjual')}}" class="mm-active">
                    @else
                    <a href="{{route('listPenjual')}}">
                    @endif
                        <i class="metismenu-icon pe-7s-id"></i>
                        Daftar Penjual
                    </a>
                </li>

                <li class="app-sidebar__heading">Produk per Kategori</li>
                {{-- <li>
                    <a href="{{route('listShop')}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Semua
                    </a>
                </li> --}}
                <li>
                    <a href="{{route('listShop', ['kategori' => 1])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Sembako
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 2])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Makanan
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 3])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Minuman 
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 4])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Sayuran 
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 5])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Buah-buahan 
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 6])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Daging Segar 
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 7])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Kesehatan
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 8])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Herbal
                    </a>
                </li>
                <li>
                    <a href="{{route('listShop', ['kategori' => 9])}}">
                        <i class="metismenu-icon pe-7s-angle-right-circle"></i>
                        Lainnya 
                    </a>
                </li>

                @if($role == 1)
                    <li class="app-sidebar__heading">User and Roles</li>
                    <li>
                        @if ($menu == 'user')
                        <a href="{{route('listUser')}}" class="mm-active">
                        @else
                        <a href="{{route('listUser')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-users"></i>
                            Pengaturan User
                        </a>
                    </li>
                    <li>
                        @if ($menu == 'role')
                        <a href="{{route('listRole')}}" class="mm-active">
                        @else
                        <a href="{{route('listRole')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Pengaturan Role
                        </a>
                    </li>
                    <li>
                        @if ($menu == 'jabatan')
                        <a href="{{route('listJabatan')}}" class="mm-active">
                        @else
                        <a href="{{route('listJabatan')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-portfolio"></i>
                            Pengaturan Jabatan
                        </a>
                    </li>
                    <li>
                        @if ($menu == 'kategori')
                        <a href="{{route('listKategori')}}" class="mm-active">
                        @else
                        <a href="{{route('listKategori')}}">
                        @endif
                            <i class="metismenu-icon pe-7s-ticket"></i>
                            Pengaturan Kategori
                        </a>
                    </li>
                @endif

                

                
                
            </ul>
        </div>
    </div>
</div>