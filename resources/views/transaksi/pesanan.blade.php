@extends('layouts.app')

@section('content')

<style>

.pb-title {
    color: grey; 
    font-size: smaller;
}

.pb-item {
    font-size: 14px;
    margin-top: 5px;
}

</style>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Pesanan Masuk
                <div class="page-title-subheading">Riwayat seluruh pesanan barang/jasa.
                </div>
            </div>
        </div>
           
    </div>
</div>

@include('notif')

@if(!empty($pesanans))
@foreach ($pesanans as $ps)
<div class="main-card mb-3 card">
    <div class="card-body">
        <div class="row pb-3">
            <div class="col-md-3">
                <div class="pb-title">
                    TANGGAL
                </div>
                <div class="pb-item">
                    {{ $ps->created_at }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    TOTAL HARGA
                </div>
                <div class="pb-item">
                    Rp{{ number_format($ps->harga,0,",",".") }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    STATUS PEMESANAN
                </div>
                <div class="pb-item">
                    {{ $ps->status }}
                </div>
            </div>
            <div class="col-md-3">
                @if($ps->status == 'Dipesan')
                    <a href="" class="btn btn-xs btn-danger float-right" data-toggle="modal" data-target="#modal_proses" onclick="get_trans_proses({{ $ps->id }})">Proses Pesanan</a>
                @elseif($ps->status == 'Diproses Penjual')
                    <a href="" class="btn btn-xs btn-danger float-right" data-toggle="modal" data-target="#modal_kirim" onclick="get_trans_kirim({{ $ps->id }})">Kirim Pesanan</a>
                @elseif($ps->status == 'Dibatalkan')
                    <div class="pb-title">
                        ALASAN PEMBATALAN
                    </div>
                    <div class="pb-item">
                        {{ $ps->alasan_batal }}
                    </div>
                @endif
                <br><br>
                @if($ps->status != 'Dibatalkan' && $ps->status != 'Telah Diterima')
                    <a href="" class="btn btn-xs btn-secondary float-right" data-toggle="modal" data-target="#modal_batal" onclick="get_trans({{ $ps->id }})">Batalkan Pesanan</a>
                @endif
            </div>
        </div>
        <div class="row pt-3" style="border-top: 1px solid lightgrey">
            <div class="col-md-3">
                <div class="pb-title">
                    PRODUK
                </div>
                <div class="pb-item">
                    <span>
                        @php
                            if(empty($ps->foto))
                                $foto = 'no-image.png';
                            else
                                $foto = explode(';', $ps->foto)[0];
                        @endphp
                        <img class="" src="{{ asset('assets/images/items/thumb/') }}/{{ $foto }}" alt="slide" width="50px" height="50px" style="border: 1px solid #d6d6d6;">
                    </span>
                    <span style="vertical-align: top;">
                        <a href='viewItem/{{ $ps->id_items }}' title='Lihat Detail'>{{ $ps->nama_item }}</a>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    JUMLAH
                </div>
                <div class="pb-item">
                    {{ $ps->jumlah_items }}
                </div>
                <div class="pb-title pt-2">
                    CATATAN TAMBAHAN
                </div>
                <div class="pb-item">
                    {{ (empty($ps->catatan)) ? '-' : $ps->catatan }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    JENIS
                </div>
                <div class="pb-item">
                    {{ $ps->jenis }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    PEMBELI
                </div>
                <div class="pb-item">
                    <div>
                        {{ $ps->pembeli->name }}
                    </div>
                    <div>
                        {{ $ps->pembeli->address }}
                    </div>
                    <div>
                        {{ $ps->pembeli->get_kelurahan->nama }}, {{ $ps->pembeli->get_kelurahan->kecamatan->nama }}
                    </div>
                    <div>
                        <img width="30" src="{{ asset('assets/images/wa.png') }}"><a class="ml-2" href="https://wa.me/62{{ $ps->pembeli->hp }}">{{ $ps->pembeli->hp }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endforeach
@else
Tidak ada pesanan!
@endif

@include('transaksi.modal_batal')
@include('transaksi.modal_proses')
@include('transaksi.modal_kirim')


<script>

$('#modal_batal').appendTo("body");
$('#modal_proses').appendTo("body");
$('#modal_kirim').appendTo("body");


function get_trans(id) {
    $.ajax({
        type: 'GET',
        url: '{!! route("getTransaksi") !!}',
        data: 'id=' + id,
        dataType: 'json',
        success: function (data) {
            $("#id_trans").val(data.id);
            $("#nama_item").html(data.item.nama);
            $("#jml").html(data.jumlah_items);
            $("#total").html('Rp' + data.total);
            $("#mode").val('pesanan');

            var img = '';
            if(data.item.img)
                img = data.item.img;
            else
                img = 'no-image.png';

            $("#img_item").attr('src', '{{ asset("assets/images/items/") }}/' + img); 
        }
    });
}

function get_trans_proses(id) {
    $.ajax({
        type: 'GET',
        url: '{!! route("getTransaksi") !!}',
        data: 'id=' + id,
        dataType: 'json',
        success: function (data) {
            $("#id_proses").val(data.id);
            $("#nama_item_proses").html(data.item.nama);
            $("#jml_proses").html(data.jumlah_items);
            $("#total_proses").html('Rp' + data.total);
            $("#pembeli").html(data.nama_user);

            var img = '';
            if(data.item.img)
                img = data.item.img;
            else
                img = 'no-image.png';

            $("#img_item_proses").attr('src', '{{ asset("assets/images/items/") }}/' + img); 
        }
    });
}

function get_trans_kirim(id) {
    $.ajax({
        type: 'GET',
        url: '{!! route("getTransaksi") !!}',
        data: 'id=' + id,
        dataType: 'json',
        success: function (data) {
            $("#id_kirim").val(data.id);
            $("#nama_item_kirim").html(data.item.nama);
            $("#jml_kirim").html(data.jumlah_items);
            $("#total_kirim").html('Rp' + data.total);
            $("#pembeli_kirim").html(data.nama_user);

            var img = '';
            if(data.item.img)
                img = data.item.img;
            else
                img = 'no-image.png';

            $("#img_item_kirim").attr('src', '{{ asset("assets/images/items/") }}/' + img); 
        }
    });
}

</script>
@stack('scripts')

@endsection
