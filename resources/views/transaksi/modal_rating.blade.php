<div class="modal fade" id="modal_rating" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Penilaian Produk</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('batal') }}">
            {{ csrf_field() }}
        
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class='item-pict'><img id="img_item" src='' style="width: 150px;"></div>
                        </div>
                        <div class="col-md-6">
                            <b><label id="nama_item" class="control-label col-xs-4" ></label></b>
                            <div>
                                <span style="">Jumlah Pesanan: </span><span id="jml"></span>
                            </div>
                            <div>
                                <span style="">Total Harga: </span>
                                <span id="total" style=" color: red; font-weight: bold; font-size: large;"></span>
                            </div>
                            
                        </div>
                    </div>    

                    <div class="row justify-content-center mt-4" style="font-size: xx-large;">
                        <span class="rate-group">
                            <?php //echo form_hidden('rating', 0); ?>
                            <a class="rate" value="1">
                                <i class="fa fa-star"></i>
                            </a>
                            <a class="rate" value="2">
                                <i class="fa fa-star"></i>
                            </a>
                            <a class="rate" value="3">
                                <i class="fa fa-star"></i>
                            </a>
                            <a class="rate" value="4">
                                <i class="fa fa-star"></i>
                            </a>
                            <a class="rate" value="5">
                                <i class="fa fa-star"></i>
                            </a>
                        </span>          
                    </div>                          

                    <div class="row mt-4">
                        <div class="col-md-12">
                        <label for="alasan" class="control-label">Alasan Pembatalan<font color="red">*</font></label>
                        <input id="alasan" type="text" class="form-control" name="alasan_batal" required autofocus>
                        <span id="alert_alasan" style="display: none; font-size: small; font-style: italic; color: red">Alasan pembatalan harus diisi!</span>
                        </div>
                    </div>
                    
                    <input type="hidden" name="id_trans" id="id_trans">
                    <input type="hidden" name="mode" id="mode">

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-secondary" type="button" value="Kembali" data-dismiss="modal"/>
                    <input id="btnBatal" class="btn btn-danger" type="submit" value="Batalkan Pesanan" onclick=""/>
                    <button id="btnLoadBatal" class="btn btn-danger" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>
    
$('#btnBatal').click(function() {
    if($('#alasan').val() == '') {
        $('#alert_alasan').show();
        $('#alasan').focus();
    } else {
        $('#alert_alasan').hide()
        $('#btnBatal').hide()
        $('#btnLoadBatal').show()
    }
});

$('.rate').on('mouseover',function(){
        //$('#hide_feedback').show(200);
        var rating = parseInt($(this).attr('value'));
        //$('input[name=rating]').val(rating);
        //req_feed(rating);
        //rating_logic(rating);
        $('.rate').each(function(index,item){
          if(index<rating){
            console.log(index);
            console.log(item);
            $(this).addClass('active');
          }
          else{
            $(this).removeClass('active');
          }
        });
    });

</script>