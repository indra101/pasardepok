@extends('layouts.app')

@section('content')

<style>

.pb-title {
    color: grey; 
    font-size: smaller;
}

.pb-item {
    font-size: 14px;
    margin-top: 5px;
}

</style>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Pembelian
                <div class="page-title-subheading">Riwayat seluruh transaksi pembelian barang/jasa.
                </div>
            </div>
        </div>
        
    </div>
</div>

@include('notif')

@if(!empty($pembelians))
@foreach ($pembelians as $pb)
<div class="main-card mb-3 card">
    <div class="card-body">
        <div class="row pb-3">
            <div class="col-md-3">
                <div class="pb-title">
                    TANGGAL
                </div>
                <div class="pb-item">
                    {{ $pb->created_at }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    TOTAL HARGA
                </div>
                <div class="pb-item">
                    Rp{{ number_format($pb->harga,0,",",".") }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    STATUS PEMESANAN
                </div>
                <div class="pb-item">
                    {{ $pb->status }}
                </div>
            </div>
            <div class="col-md-3">
                {{-- <a href="" class="btn btn-xs btn-danger float-right">Lihat Detail</a>
                <br><br> --}}
                @if($pb->status != 'Telah Diterima')
                    @if($pb->status != 'Dibatalkan')
                        <a href="" class="btn btn-xs btn-danger float-right" data-toggle="modal" data-target="#modal_batal" onclick="get_trans({{ $pb->id }})">Batalkan Pesanan</a>
                        <br><br>
                        <a href="" class="btn btn-xs btn-primary float-right" data-toggle="modal" data-target="#modal_terima" onclick="get_trans_terima({{ $pb->id }})">Terima Barang</a>
                    @else
                        <div class="pb-title">
                            ALASAN PEMBATALAN
                        </div>
                        <div class="pb-item">
                            {{ $pb->alasan_batal }}
                        </div>
                    @endif
                @else
                    <a href="" class="btn btn-xs btn-success float-right" data-toggle="modal" data-target="#modal_rating" onclick="get_trans({{ $pb->id }})">Beri Penilaian</a>
                @endif
            </div>
        </div>
        <div class="row pt-3" style="border-top: 1px solid lightgrey">
            <div class="col-md-3">
                <div class="pb-title">
                    PRODUK
                </div>
                <div class="pb-item">
                    <span>
                        @php
                            if(empty($pb->foto))
                                $foto = 'no-image.png';
                            else
                                $foto = explode(';', $pb->foto)[0];
                        @endphp
                        <img class="" src="{{ asset('assets/images/items/') }}/{{ $foto }}" alt="slide" width="50px" height="50px" style="border: 1px solid #d6d6d6;">
                    </span>
                    <span style="vertical-align: top;">
                        <a href='viewItem/{{ $pb->id_items }}' title='Lihat Detail'>{{ $pb->nama_item }}</a>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    JUMLAH
                </div>
                <div class="pb-item">
                    {{ $pb->jumlah_items }}
                </div>
                <div class="pb-title pt-2">
                    CATATAN TAMBAHAN
                </div>
                <div class="pb-item">
                    {{ (empty($pb->catatan)) ? '-' : $pb->catatan }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    JENIS
                </div>
                <div class="pb-item">
                    {{ $pb->jenis }}
                </div>
            </div>
            <div class="col-md-3">
                <div class="pb-title">
                    PENJUAL
                </div>
                <div class="pb-item">
                    <div>
                        {{ $pb->penjual->name }}
                    </div>
                    <div>
                        <img width="30" src="{{ asset('assets/images/wa.png') }}"><a class="ml-2" href="https://wa.me/62{{ $pb->penjual->hp }}">{{ $pb->penjual->hp }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endforeach
@else
Tidak ada transaksi pembelian
@endif

@include('transaksi.modal_batal')
@include('transaksi.modal_terima')
@include('transaksi.modal_rating')

<script>
$(function() {
    $('#modal_batal').appendTo("body");
    $('#modal_terima').appendTo("body");
    $('#modal_rating').appendTo("body");
});

function get_trans(id) {
    $.ajax({
        type: 'GET',
        url: '{!! route("getTransaksi") !!}',
        data: 'id=' + id,
        dataType: 'json',
        success: function (data) {
            $("#id_trans").val(data.id);
            $("#nama_item").html(data.item.nama);
            $("#jml").html(data.jumlah_items);
            $("#total").html('Rp' + data.total);
            $("#mode").val('pembelian');

            var img = '';
            if(data.item.img)
                img = data.item.img;
            else
                img = 'no-image.png';

            $("#img_item").attr('src', '{{ asset("assets/images/items/") }}/' + img); 
        }
    });
}

function get_trans_terima(id) {
    $.ajax({
        type: 'GET',
        url: '{!! route("getTransaksi") !!}',
        data: 'id=' + id,
        dataType: 'json',
        success: function (data) {
            $("#id_terima").val(data.id);
            $("#nama_item_terima").html(data.item.nama);
            $("#jml_terima").html(data.jumlah_items);
            $("#total_terima").html('Rp' + data.total);

            var img = '';
            if(data.item.img)
                img = data.item.img;
            else
                img = 'no-image.png';

            $("#img_item_terima").attr('src', '{{ asset("assets/images/items/") }}/' + img); 
        }
    });
}

</script>
@stack('scripts')

@endsection
