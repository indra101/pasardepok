@extends('layouts.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-voicemail icon-gradient bg-arielle-smile">
                </i>
            </div>
            <div>Daftar Riwayat Transaksi
                <div class="page-title-subheading">Riwayat seluruh transaksi pengguna.
                </div>
            </div>
        </div>
        
    </div>
</div>

@include('notif')

<div class="main-card mb-3 card">
    <div class="card-body">

    <a href="{{route('get.excelTransaksi')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Download Excel</a>
    <br/><br/>
    <div class="overflow-auto">
        <table class="table table-striped table-hover" id="transaksi-table" style="background-color: white;">
            <thead>
                <tr>
                    <th style="max-width: 20px;">No</th>
                    <th>Jenis</th>
                    <th>Nama Pembeli</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th style="max-width: 100px;">Action</th>
                </tr>
            </thead>
        </table>
    </div>

    <script>
    $(function() {

        var table = $('#transaksi-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getTransaksi.datas') !!}',
            order: [[6, 'desc']],
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'jenis', name: 'jenis' , searchable: true},
                { data: 'nama_user', name: 'nama_user' , searchable: true},
                { data: 'nama_item', name: 'nama_item' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'jumlah_items', name: 'jumlah_items' , searchable: true},
                { data: 'created_at', name: 'created_at' , searchable: true},
                { data: 'status', name: 'status' , searchable: true},
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<a href="#delete-' + data.id + '" class="btn btn-xs btn-danger ml-1" style="" onclick=\'confirmDel(' + data.id + ',"' + data.nama + '")\'><i class="fa fa-trash"></i></a>' +
                    '<form class="delete" action="deleteTransaksi/' + data.id + '" method="get">' + 
                    '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                    '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                    '</form></div>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data transaksi?");
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

    </div>
</div>
@endsection
