<div class="modal fade" id="modal_proses" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelTerima">Konfirmasi Proses Pesanan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('proses') }}">
            {{ csrf_field() }}
        
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class='item-pict'><img id="img_item_proses" src='' style="width: 150px;"></div>
                        </div>
                        <div class="col-md-6">
                            <b><label id="nama_item_proses" class="control-label col-xs-4" ></label></b>
                            <div>
                                <span style="">Jumlah Pesanan: </span><span id="jml_proses"></span>
                            </div>
                            <div>
                                <span style="">Total Harga: </span>
                                <span id="total_proses" style=" color: red; font-weight: bold; font-size: large;"></span>
                            </div>
                            <div>
                                <span style="">Pembeli: </span>
                                <span id="pembeli"></span>
                            </div>
                            
                        </div>
                    </div>                                        
                    
                    <input type="hidden" name="id_trans" id="id_proses">

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-secondary" type="button" value="Kembali" data-dismiss="modal"/>
                    <input id="btnProses" class="btn btn-danger" type="submit" value="Proses Barang" onclick=""/>
                    <button id="btnLoadProses" class="btn btn-danger" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>
    
    $('#btnProses').click(function() {
        $('#btnProses').hide()
        $('#btnLoadProses').show()
    });
    
</script>