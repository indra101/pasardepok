<div class="modal fade" id="modal_kirim" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelTerima">Konfirmasi Kirim Barang</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>

            <form class="form-horizontal" method="POST" action="{{ route('kirim') }}">
            {{ csrf_field() }}
        
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class='item-pict'><img id="img_item_kirim" src='' style="width: 150px;"></div>
                        </div>
                        <div class="col-md-6">
                            <b><label id="nama_item_kirim" class="control-label col-xs-4" ></label></b>
                            <div>
                                <span style="">Jumlah Pesanan: </span><span id="jml_proses"></span>
                            </div>
                            <div>
                                <span style="">Total Harga: </span>
                                <span id="total_kirim" style=" color: red; font-weight: bold; font-size: large;"></span>
                            </div>
                            <div>
                                <span style="">Pembeli: </span>
                                <span id="pembeli_kirim"></span>
                            </div>
                            
                        </div>
                    </div>                                        
                    
                    <input type="hidden" name="id_trans" id="id_kirim">

                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-secondary" type="button" value="Kembali" data-dismiss="modal"/>
                    <input id="btnKirim" class="btn btn-danger" type="submit" value="Kirim Barang" onclick=""/>
                    <button id="btnLoadKirim" class="btn btn-danger" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<script>
    
    $('#btnKirim').click(function() {
        $('#btnKirim').hide()
        $('#btnLoadKirim').show()
    });
    
</script>