<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'jenis', 'id_user', 'id_seller', 'nama_user', 'id_items', 'nama_item', 'harga', 'jumlah_items', 'created_at', 'alasan_batal', 'updated_at'
    ];

    public function log() {
        return $this->hasMany('App\Models\Log_transaksi');
    }

    public function item() {
        return $this->hasOne('App\Models\Item', 'id', 'id_items');
    }

    public function pembeli() {
        return $this->hasOne('App\Models\User', 'id', 'id_user');
    }

    public function penjual() {
        return $this->hasOne('App\Models\User', 'id', 'id_seller');
    }
}
