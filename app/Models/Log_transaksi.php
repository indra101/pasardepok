<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log_transaksi extends Model
{
    protected $fillable = [
        'id_transaksis', 'status', 'created_at', 'updated_at'
    ];

    public function transaksi(){
        return $this->belongsTo('App\Models\Transaksi','id_transaksis');
    }
}
