<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Promosi extends Model
{
    protected $fillable = [
        'id_seller', 'deskripsi', 'image'
    ];

    public function penjual(){
        return $this->belongsTo('App\Models\User','id_seller');
    }

}
