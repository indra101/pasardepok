<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'address', 'hp', 'role_id', 'foto', 'ketua_uppa', 'kelurahan', 'id_kelurahan', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function role(){
        return $this->belongsTo('App\Models\Role','role_id');
    }

    public function jabatan(){
        return $this->belongsTo('App\Models\Jabatan','jabatan_id');
    }

    public function item() {
        return $this->hasMany('App\Models\Item');
    }

    public function promosi() {
        return $this->hasMany('App\Models\Promosi');
    }

    public function transaksi() {
        return $this->hasMany('App\Models\Transaksi');
    }

    public function get_kelurahan() {
        return $this->belongsTo('App\Models\Kelurahan','id_kelurahan');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
