<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    
    protected $fillable = [
        'name', 'icon', 'urutan'
    ];
    
    public function item() {
        return $this->hasMany('App\Models\Item');
    }
}
