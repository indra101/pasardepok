<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $fillable = [
        'nama', 'id_kecamatan'
    ];

    public function kecamatan(){
        return $this->belongsTo('App\Models\Kecamatan','id_kecamatan', 'id');
    }
    
    public function user() {
        return $this->hasMany('App\Models\User');
    }
}
