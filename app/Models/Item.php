<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Item extends Model
{
    protected $fillable = [
        'nama', 'deskripsi', 'harga', 'stok', 'jenis', 'id_kategori', 'foto', 'id_seller', 'updated_at', 'satuan', 'stok_stat'
    ];

    public function kategori(){
        return $this->belongsTo('App\Models\Kategori','id_kategori', 'id');
    }

    public function penjual(){
        return $this->belongsTo('App\Models\User','id_seller', 'id');
    }

    public static function getItems() {
        $items = DB::table ( 'items' )
                ->select ( 'id', 'nama', 'deskripsi', 'harga', 'stok', 'jenis', 'id_kategori', 'foto', 'id_seller', 'updated_at');

        return $items;
    }
}
