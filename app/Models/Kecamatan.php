<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $fillable = [
        'nama'
    ];
    
    public function kelurahan() {
        return $this->hasMany('App\Models\Kelurahan');
    }
}
