<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Transaksi;
use App\Models\Log_transaksi;
use App\Models\User;
use App\Models\Kategori;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Image;
//use Intervention\Image\Exception;

class ItemsController extends Controller
{
    private $satuan = array(
        'pcs' => 'pcs',
        'kg' => 'kg',
        'gr' => 'gr',
        'meter' => 'meter',
        'pack' => 'pack',
        'botol' => 'botol',
        'porsi' => 'porsi',
        'paket' => 'paket',
        'liter' => 'liter',
        'dus' => 'dus',      
        'box' => 'box',       
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listItem() 
    {        
        return View::make('item.listItem', ['menu' => 'item']);
    }

    public function listShop(Request $request) 
    {        
        $id_kategori = $request->input ('kategori');
        $search = $request->input ('search');
        $kategori = Kategori::find($id_kategori);
        if($kategori) {
            $nama_kategori = $kategori->nama;
        } else {
            $nama_kategori = 'Semua';
        }

        $kategoris = Kategori::orderBy('urutan', 'asc')->get();

        return View::make('item.listShop', ['menu' => 'toko', 'kategori' => $id_kategori, 'nama_kategori' => $nama_kategori, 'search' => $search, 'kategoris' => $kategoris]);
    }

    public function listShopPenjual($id) 
    {        
        $penjual = User::find($id);

        return View::make('item.listShopPenjual', ['menu' => 'penjual', 'penjual' => $penjual]);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addItem(Request $request)
    {
        $this->validate($request, [
            //'file_upload' => 'required',
            'file_upload.*' => 'mimes:jpg,png,jpeg,JPG,PNG,JPEG',
        ]);

        $item = new Item;
        $item->nama = $request->nama;
        $item->deskripsi = urlencode($request->deskripsi);
        $item->harga = $request->harga;
        $item->jenis = $request->jenis;
        $item->satuan = $request->satuan;
        $item->id_kategori = $request->kategori;
        $item->stok = $request->stok;
        $item->stok_stat = $request->stok_stat;
        $item->id_seller = Auth::user()->id;
        $item->save();

        $file_data = array();
        $file_upload = $request->file('file_upload');

        $file_names = '';
        if(!empty($file_upload)) {
            $file_names = $this->uploadFile($file_upload, $item->id);
        }

        $item->foto = $file_names;
        $item->save();

        return redirect()->route('profilToko')
                        ->with('success','Berhasil menambah produk!');
    }

    public function addItemPage() 
    {        
        $kategoris = Kategori::all();
        return View::make('item.addItem', ['menu' => 'item', 'kategoris' => $kategoris, 'satuan' => $this->satuan]);
    }

    public function editItemPage($id) 
    {
        $item = Item::find($id);

        if(Auth::user()->id == $item->id_seller || Auth::user()->role_id == 1) {
            
            $kategoris = Kategori::all();
            $fotos = $item->foto;

            if(strpos($fotos, ";"))
            $fotos = explode(';', $fotos);
            $item->fotos = $fotos;
            
            return View::make('item.editItem', ['menu' => 'item', 'items' => $item, 'kategoris' => $kategoris, 'satuan' => $this->satuan]);

        } else {

            return redirect()->route('home')
                        ->with('fail','Anda tidak memiliki akses!');
        }
    }

    public function viewItemPage($id) 
    {
        $item = Item::find($id);
        $item->nama_kategori = $item->kategori->nama;
        $fotos = $item->foto;
            
        if(empty($fotos))
            $fotos = array("no-image.png");
        else {
            if(strpos($fotos, ";"))
            $fotos = explode(';', $fotos);
        }

        $item->fotos = $fotos;

        return View::make('item.viewItem', compact('item'));
    }

    public function getItem($id)
    {
        $item = Item::find($id);
        return $item;
    }

    public function editItem(Request $request)
    {
        $item = Item::find($request->id);

        if(Auth::user()->id == $item->id_seller || Auth::user()->role_id == 1) {

            $this->validate($request, [
                'file_upload.*' => 'mimes:jpg,png,jpeg,webp'
            ]);
            
            $item->nama = $request->nama;
            $item->deskripsi = urlencode($request->deskripsi);
            $item->harga = $request->harga;
            $item->jenis = $request->jenis;
            $item->satuan = $request->satuan;
            $item->id_kategori = $request->kategori;
            $item->stok = $request->stok;
            $item->save();

            $file_data = array();
            $file_upload = $request->file('file_upload');

            $new_files = "";
            if(!empty($file_upload)) {
                $new_files = $this->uploadFile($file_upload, $item->id);
            }

            if(empty($request->old_files)) {
                $file_names = $new_files;
            } else {
                $old_files = implode(";",$request->old_files);
                $file_names = $old_files.';'.$new_files;
            }

            $item->foto = $file_names;
            $item->save();

            return redirect()->route('profilToko')
                            ->with('success','Data produk berhasil diubah');
                            
        } else {

            return redirect()->route('home')
                        ->with('fail','Anda tidak memiliki akses!');
        }
    }

    public function deleteItem($id)
    {
        $item = Item::find($id);

        if(Auth::user()->id == $item->id_seller || Auth::user()->role_id == 1) {
            $item->delete();
            return redirect()->route('profilToko')
                            ->with('success','Produk berhasil dihapus');
        } else {
            return redirect()->route('home')
                        ->with('fail','Anda tidak memiliki akses!');
        }
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'deskripsi',
                3 =>'harga',
                4 =>'stok', 
                5 =>'id_kategori', 
                6 =>'foto', 
                7 =>'id_seller',
                8 =>'updated_at',
        );
        //Getting the data
        $items = Item::getItems();
        
        $totalData = $items->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $items->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'value' )) {
            if ($request->input ( 'value' ) != '') {
                $searchTerm = $request->input ( 'value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $items->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        // echo $request->input ( 'kategori.value' );
        // die;
        
        if ($request->has ( 'kategori' )) {
            if ($request->input ( 'kategori' ) != '') {
                $searchTerm = $request->input ( 'kategori' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $items->where ( 'id_kategori', '=', $searchTerm );
            }
        }

        if ($request->has ( 'id_seller' )) {
            if ($request->input ( 'id_seller' ) != '') {
                $searchTerm = $request->input ( 'id_seller' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $items->where ( 'id_seller', '=', $searchTerm );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $items->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $items->count ();
        // Data to client
        $jobs = $items->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $items = $items->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $items as $item ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $item->id;
            $nestedData ['nama'] = $item->nama;
            $nestedData ['deskripsi'] = $item->deskripsi;
            $nestedData ['harga'] = $item->harga;
            $nestedData ['stok'] = $item->stok;
            $nestedData ['updated_at'] = $item->updated_at;
            $nestedData ['id_kategori'] = $item->id_kategori;
            $kategori = Kategori::find($item->id_kategori);
            $nestedData ['nama_kategori'] = $kategori->nama;

            $fotos = $item->foto;
            
            if(empty($fotos))
                $foto = "no-image.png";
            else {
                if(strpos($fotos, ";"))
                    $foto = explode(';', $fotos)[0];
                else
                    $foto = $fotos;
            }

            $nestedData ['foto'] = $foto;
            $nestedData ['id_seller'] = $item->id_seller;
            $nestedData ['tot'] = count($items);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    public function getBladeExcel()
    {
        $itemData = DB::table ( 'items' )
        ->select ( 'id',
            'nama',
            'deskripsi',
            'harga',
            'stok'
        )->get ();

        \Excel::create('Data Barang', function($excel) use($itemData) {

            $excel->sheet('Data Barang', function($sheet) use($itemData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Nama Barang',
                    'Deskripsi',
                    'Harga (Rp)',
                    'Stok'
                ];

                $no = 1;
                foreach ($itemData as $key => $value) {
                    $excelData[] = [
                        $no++,
                        $value->nama,
                        $value->deskripsi,
                        $value->harga,
                        $value->stok
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $itemData = Item::all();

        //return Response::json($itemData);

        $data = json_encode($itemData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }

    function uploadFile($file_upload, $id_item) {    

        $file_names = "";
        $i = 1;

        foreach($file_upload as $file)
        {
            $rand = mt_rand(0,1000);
            $name = $id_item."_".$i++."_".$rand;
            $ext = $file->getClientOriginalExtension();
            $image_name = $name.".".$ext;
            $destinationPath = public_path().'/assets/images/items/';
            $file->move($destinationPath, $image_name); 
            $file_names .= "$image_name;";

            $img = Image::make($destinationPath.$image_name);            
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'thumb/'.$image_name);

            $img = Image::make($destinationPath.$image_name);
            $img->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'thumb_view/'.$image_name);
        }

        return $file_names;
    }

    function makeThumb() {

        $items = Item::all();
        foreach($items as $i) {

            $fotos = explode(';', $i->foto);
            foreach($fotos as $f) {
                if($f) {
                    try {
                        $destinationPath = public_path().'/assets/images/items/';
                        $img = Image::make($destinationPath.$f);
                        $img->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'thumb/'.$f);

                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }

        return redirect()->route('home')
                        ->with('success','Thumbnail berhasil dibuat!');
    }

    function makeThumbView() {

        $items = Item::all();
        foreach($items as $i) {

            $fotos = explode(';', $i->foto);
            foreach($fotos as $f) {
                if($f) {
                    try {
                        $destinationPath = public_path().'/assets/images/items/';
                        $img = Image::make($destinationPath.$f);
                        $img->resize(null, 350, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'thumb_view/'.$f);

                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }

        return redirect()->route('home')
                        ->with('success','Thumbnail berhasil dibuat!');
    }
    
}
