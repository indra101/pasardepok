<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use App\Models\Jabatan;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Exception;
use Image;

class UsersController extends Controller
{
    private $kelurahans = array(
        'Beji' => 'Beji',
        'Beji Timur' => 'Beji Timur',
        'Kemiri Muka' => 'Kemiri Muka',
        'Kukusan' => 'Kukusan',
        'Pondok Cina' => 'Pondok Cina',
        'Tanah Baru' => 'Tanah Baru',
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //return DataTables::of(User::query())->make(true);
        // $users = User::select('*')->get();
        // return Datatables::of($users)->make(true);
    }

    public function listUser() 
    {        
        return View::make('user.listUser', ['menu' => 'user']);
    }

    public function addUser(Request $request)
    {
        $data = array(
            'username' => $request->email,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'hp' => $request->phone,
            'ketua_uppa' => $request->ketua_uppa,
            //'npwp' => $request->npwp,
            'role_id' => $request->role,
            //'jabatan_id' => $request->jabatan
        );

        try {
            DB::table('users')->insert($data);
        } catch(Exception $e) {
            return redirect()->route('listUser', ['menu' => 'user'])
                        ->with('fail','Gagal membuat user baru!');
        }
        
        return redirect()->route('listUser', ['menu' => 'user'])
                        ->with('success','User berhasil dibuat');
        
    }

    public function addUserPage() 
    {        
        $roles = Role::all();
        $jabatans = Jabatan::all();
        $kelurahans = $this->kelurahans;

        return View::make('user.addUser', compact('roles','jabatans','kelurahans'));
    }

    public function getKelurahan(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;
        $kelurahans = Kelurahan::where('id_kecamatan', $id_kecamatan)->pluck('nama', 'id');

        return $kelurahans->toJson();
    }

    public function editUserPage($id) 
    {
        if(Auth::user()->id == $id || Auth::user()->role_id == 1) {

            $user = User::find($id);
            $roles = Role::all();
            $jabatans = Jabatan::all();
            $kecamatans = Kecamatan::pluck('nama', 'id');
            $kelurahans = Kelurahan::where('id_kecamatan', $user->get_kelurahan->kecamatan->id)->pluck('nama', 'id');
            return View::make('user.editUser', compact('user','roles','jabatans','kelurahans', 'kecamatans'));

        } else {

            return redirect()->route('home')
                        ->with('fail','Anda tidak memiliki akses!');
        }
    }

    public function viewUserPage($id) 
    {
        $user = User::find($id);
        $roles = Role::all();
        $jabatans = Jabatan::all();
        return View::make('user.viewUser', compact('user','roles','jabatans'));
    }

    public function editUser(Request $request)
    {
        $data = array(
            //'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'id_kelurahan' => $request->id_kelurahan,
            'hp' => $request->phone,
            'ketua_uppa' => $request->ketua_uppa,
            'nama_toko' => $request->nama_toko,
            //'npwp' => $request->npwp,
            //'jabatan_id' => $request->jabatan,
            'role_id' => $request->role
        );

        if(!empty($request->password))
            $data['password'] = bcrypt($request->password);

        $file_upload = $request->file('image');

        if(!empty($file_upload)) {
            $foto = $this->uploadFile($file_upload, $request->id);
            $data['foto'] = $foto;
        }

        DB::table('users')->where('id','=',$request->id)->update($data);

        return redirect()->route('home')
                        ->with('success','User berhasil diubah');
    }

    public function deleteUser($id)
    {
        User::find($id)->delete();
        return redirect()->route('listUser', ['menu' => 'user'])
                        ->with('success','User berhasil dihapus');
    }

    public function bukaToko(Request $request)
    {
        $user = User::find($request->id);
        $user->role_id = 3;
        $user->nama_toko = $request->nama_toko;
        $user->save();

        return redirect()->route('profilToko')
                        ->with('success','Berhasil buka toko!<br>Anda dapat menambahkan produk Anda pada menu Pengelolaan Toko. <br>Anda juga dapat melakukan Promosi maksimal 1x sehari. Promosi akan ditayangkan selama 3 hari.');
    }

    public function profilToko() 
    {        
        $penjual = User::find(Auth::user()->id);

        return View::make('user.profilToko', ['menu' => 'profilToko', 'penjual' => $penjual]);
    }

    public function listPenjual(Request $request) 
    {        
        $penjuals = User::all()->where('role_id', '=', 3);

        return View::make('user.listPenjual', ['menu' => 'penjual', 'penjuals' => $penjuals]);
    }

    public function profile()
    {
        return view('user.profile');
    }

    public function isAdmin()
    {
        $users = DB::table ( 'users' )
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.created_at',
            'users.updated_at'       
        );
        //return DataTables::of(User::query())->make(true);
        // $users = User::select('*')->get();
        // return Datatables::of($users)->make(true);
    }

    public function getDatas(Request $request){
        //echo"masuk method getDatas";    

        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',
                2 =>'email',
                3 =>'address',
                4 =>'kelurahan',
                5 =>'hp',
                6 =>'foto',
                7 =>'role',
                8 =>'ketua_uppa',
                9 =>'nama_toko',
        );
        //Getting the data
        $users = User::join('roles', 'users.role_id', '=', 'roles.id')
        ->leftjoin('jabatans', 'users.jabatan_id', '=', 'jabatans.id')
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.role_id',
            'users.address',
            'users.kelurahan',
            'users.id_kelurahan',
            'users.hp',
            'users.foto',
            'users.ketua_uppa',
            'roles.name as role',
            'users.nama_toko'
        );
        
        $totalData = $users->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
            }
        }

        $i = 0;

        if ($request->has ( 'columns.1.search' )) {
            if ($request->input ( 'columns.1.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.1.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.2.search' )) {
            if ($request->input ( 'columns.2.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.2.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.email', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.3.search' )) {
            if ($request->input ( 'columns.3.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.3.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.address', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.4.search' )) {
            if ($request->input ( 'columns.4.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.4.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.kelurahan', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.5.search' )) {
            if ($request->input ( 'columns.5.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.4.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.hp', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.6.search' )) {
            if ($request->input ( 'columns.6.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.5.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.ketua_uppa', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.7.search' )) {
            if ($request->input ( 'columns.7.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.6.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'roles.name', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                //$jobs->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
                $users->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $users->count ();
        // Data to client
        $jobs = $users->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $users = $users->get ();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        $role = Auth::user()->role_id;

        foreach ( $users as $user ) {
            $nestedData = array ();
            //$nestedData [0] = $user->id;
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $user->id;
            $nestedData ['name'] = $user->name;
            $nestedData ['email'] = $user->email;
            $nestedData ['address'] = $user->address;
            $nestedData ['kelurahan'] = $user->get_kelurahan->nama;
            $nestedData ['kecamatan'] = $user->get_kelurahan->kecamatan->nama;
            $nestedData ['hp'] = $user->hp;
            $nestedData ['foto'] = $user->foto;
            $nestedData ['ketua_uppa'] = $user->ketua_uppa;
            $nestedData ['role'] = $user->role;
            $nestedData ['role_id'] = $role;
            //$nestedData ['jabatan'] = $user->jabatan;
            $nestedData ['nama_toko'] = $user->nama_toko;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        //echo"$tableContent: $tableContent";
        return $tableContent;
    }

    public function getPenjuals(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',
                2 =>'address',
                3 =>'nama_toko',
                4 =>'foto',
                5 =>'kelurahan',
        );
        //Getting the data
        $users = DB::table ( 'users' )
                    ->select ( 'id', 'name', 'address', 'nama_toko', 'foto', 'kelurahan');
        
        $totalData = $users->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'name', 'Like', '%' . $searchTerm . '%' );
                $users->orWhere ( 'nama_toko', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'role_id' )) {
            if ($request->input ( 'role_id' ) != '') {
                $searchTerm = $request->input ( 'role_id' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'role_id', $searchTerm );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $users->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $users->count ();
        // Data to client
        $jobs = $users->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $users = $users->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $users as $user ) {
            $user = User::find($user->id);
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $user->id;
            $nestedData ['name'] = $user->name;
            $nestedData ['address'] = ($user->address) ? $user->address : '-';
            // $nestedData ['kelurahan'] = ($user->kelurahan) ? $user->kelurahan : '-';
            $nestedData ['kelurahan'] = ($user->get_kelurahan->nama) ? $user->get_kelurahan->nama : '-';
            $nestedData ['kecamatan'] = ($user->get_kelurahan->kecamatan->nama) ? $user->get_kelurahan->kecamatan->nama : '-';
            $nestedData ['nama_toko'] = $user->nama_toko;

            $foto = $user->foto;
            
            if(empty($foto))
                $foto = "no-image.png";

            $nestedData ['foto'] = $foto;
            $nestedData ['tot'] = count($users);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    public function getBladeExcel()
    {
        $userData = DB::table ( 'users' )
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->leftjoin('jabatans', 'users.jabatan_id', '=', 'jabatans.id')
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.address',
            'users.hp',
            'users.npwp',
            'roles.name as role',
            'jabatans.name as jabatan'
        )->get ();

        \Excel::create('Data Karyawan', function($excel) use($userData) {

            $excel->sheet('Data Karyawan', function($sheet) use($userData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Name',
                    'E-mail',
                    'Address',
                    'Phone',
                    'NPWP',
                    'Jabatan',
                    'Role',
                ];

                $no = 1;
                foreach ($userData as $key => $value) {
                    $excelData[] = [
                        $no++,
                        $value->name,
                        $value->email,
                        $value->address,
                        $value->hp,
                        $value->npwp,
                        $value->jabatan,
                        $value->role
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $userData = User::all();

        //return Response::json($userData);

        $data = json_encode($userData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }

    function uploadFile($file, $id_user) {    

        $name = $id_user;
        $ext = $file->getClientOriginalExtension();
        $file_name = "$name.$ext";
        $destinationPath = public_path().'/assets/images/users/';
        $file->move($destinationPath, $file_name);

        $img = Image::make($destinationPath.$file_name);            
        $img->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'thumb/'.$file_name);

        return $file_name;
    }

    function makeThumbUser() {

        $users = User::all();
        foreach($users as $u) {

            $f = $u->foto;
            
            if($f) {
                try {
                    
                    $destinationPath = public_path().'/assets/images/users/';
                    $img = Image::make($destinationPath.$f);
                    $img->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'thumb/'.$f);

                } catch (\Exception $e) {
                    
                }
            }
        
        }

        return redirect()->route('home')
                        ->with('success','Thumbnail berhasil dibuat!');
    }
    
}
