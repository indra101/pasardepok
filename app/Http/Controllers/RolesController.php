<?php

namespace App\Http\Controllers;

use App\Models\Role;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listRole() 
    {        
        return View::make('role.listRole', ['menu' => 'role']);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addRole(Request $request)
    {
        $data = array(
            'name' => $request->name
        );

        DB::table('roles')->insert($data);

        return redirect()->route('listRole')
                        ->with('success','Role created successfully');
    }

    public function addRolePage() 
    {        
        return View::make('role.addRole', ['menu' => 'role']);
    }

    public function editRolePage($id) 
    {
        $roles = Role::find($id);
        return View::make('role.editRole', compact('roles'));
    }

    public function viewRolePage($id) 
    {
        
    }

    public function editRole(Request $request)
    {
        $data = array(
            'name' => $request->name
        );

        DB::table('roles')->where('id','=',$request->id)->update($data);

        return redirect()->route('listRole')
                        ->with('success','Role edited successfully');
    }

    public function deleteRole($id)
    {
        Role::find($id)->delete();
        return redirect()->route('listRole')
                        ->with('success','Role deleted successfully');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',                
        );
        //Getting the data
        $roles = DB::table ( 'roles' )
        ->select ( 'id',
            'name'
        );
        
        $totalData = $roles->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on role_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $roles->where ( 'name', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $roles->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $roles->count ();
        // Data to client
        $jobs = $roles->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $roles = $roles->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $roles as $role ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['name'] = $role->name;
            $nestedData ['id'] = $role->id;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }
    
}
