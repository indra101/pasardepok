<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Exception;
use Image;

class KategorisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listKategori() 
    {        
        return View::make('kategori.listKategori', ['menu' => 'kategori']);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addKategori(Request $request)
    {
        $kategori = new Kategori;
        $kategori->nama = $request->nama;
        $kategori->urutan = $request->urutan;
    
        $kategori->save();

        $file_upload = $request->file('image');
        $icon = $this->uploadFile($file_upload, $kategori->id);
        $kategori->icon = $icon;

        $kategori->save();

        return redirect()->route('listKategori')
                        ->with('success','Berhasil menambah kategori');
    }

    public function addKategoriPage() 
    {
        $maks = Kategori::max('urutan') + 1;
        return View::make('kategori.addKategori', ['maks' => $maks]);
    }

    public function editKategoriPage($id) 
    {
        $kategoris = Kategori::find($id);
        return View::make('kategori.editKategori', compact('kategoris'));
    }

    public function viewKategoriPage($id) 
    {
        
    }

    public function editKategori(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'urutan' => $request->urutan,
        );

        $file_upload = $request->file('image');

        if(!empty($file_upload)) {
            $icon = $this->uploadFile($file_upload, $request->id);
            $data['icon'] = $icon;
        }

        DB::table('kategoris')->where('id','=',$request->id)->update($data);

        return redirect()->route('listKategori')
                        ->with('success','Berhasil mengubah kategori');
    }

    public function deleteKategori($id)
    {
        Kategori::find($id)->delete();
        return redirect()->route('listKategori')
                        ->with('success','Berhasil menghapus kategori');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'icon',
                3 =>'urutan',
        );
        //Getting the data
        $kategoris = DB::table ( 'kategoris' )
        ->select ( 'id','nama','icon','urutan');
        
        $totalData = $kategoris->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on kategori_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $kategoris->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $kategoris->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $kategoris->count ();
        // Data to client
        $jobs = $kategoris->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $kategoris = $kategoris->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $kategoris as $kategori ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['nama'] = $kategori->nama;
            $nestedData ['id'] = $kategori->id;
            $nestedData ['icon'] = $kategori->icon;
            $nestedData ['urutan'] = $kategori->urutan;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    function uploadFile($file, $id) {    

        $name = $id;
        $ext = $file->getClientOriginalExtension();
        $file_name = "$name.$ext";
        $destinationPath = public_path().'/assets/images/icons/';
        $file->move($destinationPath, $file_name);

        $img = Image::make($destinationPath.$file_name);            
        $img->resize(60, 60, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.$file_name);

        return $file_name;
    }
    
}
