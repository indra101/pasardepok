<?php

namespace App\Http\Controllers;

use App\Models\Promosi;
use App\Models\Item;
use App\Models\Kategori;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class PromosisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home', ['menu' => 'home']);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addPromosi(Request $request)
    {
        $promosi = new Promosi;
        $promosi->deskripsi = urlencode($request->deskripsi);
        $promosi->id_seller = Auth::user()->id;
        $promosi->save();

        $file_data = array();
        $file_upload = $request->file('image');

        if(!empty($file_upload)) {
            $file_names = $this->uploadFile($file_upload, $promosi->id);
        }

        $promosi->image = $file_names;
        $promosi->save();

        return redirect()->route('home')
                        ->with('success','Berhasil membuat promosi');
    }

    public function editPromosi(Request $request)
    {
        $promosi = Promosi::find($request->id);
        $promosi->deskripsi = urlencode($request->deskripsi);

        $file_data = array();
        $file_upload = $request->file('image');

        // print_r($file_upload);
        // die;

        $file_names = $promosi->image;

        if(!empty($file_upload)) {
            $file_names = $this->uploadFile($file_upload, $promosi->id);
        }

        $promosi->image = $file_names;
        $promosi->save();

        return redirect()->route('home')
                        ->with('success','Berhasil mengubah promosi');
    }

    public function editPromoPage($id) 
    {
        if(Auth::user()->id == $id || Auth::user()->role_id == 1) {
                
            $promosi = Promosi::find($id);
            
            return View::make('editPromo', ['menu' => 'home', 'promosi' => $promosi, 'mode' => 'edit']);

        } else {

            return redirect()->route('home')
                        ->with('fail','Anda tidak memiliki akses!');
        }
    }

    public function addPromoPage() 
    {
        return View::make('editPromo', ['menu' => 'home', 'promosi' => null, 'mode' => 'add']);
    }

    function uploadFile($file, $id_promo) {

        $name = $id_promo;
        $ext = $file->getClientOriginalExtension();
        $file_name = "$name.$ext";
        $destinationPath = public_path().'/assets/images/promosis/';
        $file->move($destinationPath, $file_name);

        $img = Image::make($destinationPath.$file_name);
        $img->resize(null, 350, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'thumb/'.$file_name);

        return $file_name;
    }

    function makeThumbPromo() {

        $promosis = Promosi::all();

        foreach($promosis as $i) {

            $fotos = explode(';', $i->image);
            foreach($fotos as $f) {
                if($f) {
                    try {
                        $destinationPath = public_path().'/assets/images/promosis/';
                        $img = Image::make($destinationPath.$f);
                        $img->resize(null, 350, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'thumb/'.$f);

                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }

        return redirect()->route('home')
                        ->with('success','Thumbnail berhasil dibuat!');
    }
    
}
