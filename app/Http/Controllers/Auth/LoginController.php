<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Models\Transaksi;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    //protected $redirectTo = '/listShop';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $now = date('Y-m-d H:i:s');

        $user->last_login = $now;
        $user->save();
        
        // $jml_pembelian = Transaksi::where('id_user', '=', $user->id)
        //                         ->where('status', '!=', 'Telah Diterima')
        //                         ->where('status', '!=', 'Dibatalkan')
        //                         ->count();

        // $jml_pesanan = Transaksi::where('id_seller', '=', $user->id)
        //                         ->where('status', '!=', 'Telah Diterima')
        //                         ->where('status', '!=', 'Dibatalkan')
        //                         ->count();

        // $request->session()->put('jml_pembelian', $jml_pembelian);
        // $request->session()->put('jml_pesanan', $jml_pesanan);
    }

    /**
     * Get the needed authorization credentials from the request.
     * see: https://sujipthapa.co/blog/laravel-v55-login-register-with-username-or-email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }
}
