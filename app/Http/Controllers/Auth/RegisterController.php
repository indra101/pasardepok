<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Role;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    //protected $redirectTo = '/listShop';

    private $kelurahans = array(
        'Beji' => 'Beji',
        'Beji Timur' => 'Beji Timur',
        'Kemiri Muka' => 'Kemiri Muka',
        'Kukusan' => 'Kukusan',
        'Pondok Cina' => 'Pondok Cina',
        'Tanah Baru' => 'Tanah Baru',
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Override function showRegistrationForm() from RegistersUsers.
    */
    public function showRegistrationForm()
    {
        $kecamatans = Kecamatan::pluck('nama', 'id');
        $kelurahans = Kelurahan::pluck('nama', 'id');
        return view('auth.register', ['kecamatans' => $kecamatans, 'kelurahans' => $kelurahans]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            //'username' => 'required|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string|max:255',
            //'kelurahan' => 'required|string|max:255',
            'hp' => 'required|string|max:255',
            //'ketua_uppa' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // echo $data['kelurahan'];
        // die;
        //$role = Role::find(2);
        $now = date('Y-m-d H:i:s');

        return User::create([
            'name' => $data['name'],
            //'username' => $data['username'],
            'username' => $data['email'],
            'email' => $data['email'],
            'address' => $data['address'],
            'kelurahan' => $data['kelurahan'],
            'id_kelurahan' => $data['kelurahan'],
            'hp' => $data['hp'],
            //'ketua_uppa' => $data['ketua_uppa'],
            //'role_id' => $role['id'],
            'role_id' => 2, //pembeli
            'foto' => 'no-image.png',
            'password' => bcrypt($data['password']),
            'last_login' => $now,
        ]);
    }

    public function getKelurahan(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;
        $kelurahans = Kelurahan::where('id_kecamatan', $id_kecamatan)->pluck('nama', 'id');

        return $kelurahans->toJson();
    }
}
