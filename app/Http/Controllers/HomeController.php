<?php

namespace App\Http\Controllers;

use App\Models\Promosi;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date('Y-m-d');

        $promosis = Promosi::select()
                            ->whereRaw('created_at >= DATE_ADD(NOW(), INTERVAL -2 DAY)')
                            ->orderBy('id', 'desc')->get();

        $kategoris = Kategori::orderBy('urutan', 'asc')->get();
        
        $promo_btn = 1;

        if(Auth::user()->role_id == 3) {
            $promo = Promosi::select()
                            ->where('id_seller','=',Auth::user()->id)
                            ->whereDate('created_at','=',$today)->get();

            if(!$promo->isEmpty())
                $promo_btn = 0;
        }

        $n = 0;
        foreach($promosis as $p) {
            if(empty($p->image))
                $promosis[$n]->image = 'no-image.png';
            
            $n++;      
        }

        return view('home', ['menu' => 'home', 'promosis' => $promosis, 'kategoris' => $kategoris, 'promo_btn' => $promo_btn]);
    }
}
