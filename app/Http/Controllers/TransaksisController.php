<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\Log_transaksi;
use App\Models\User;
use App\Models\Item;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Mail;

class TransaksisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listTransaksi() 
    {        
        return View::make('transaksi.listTransaksi', ['menu' => 'transaksi']);
    }

    public function pembelian() 
    {   
        $id_user = Auth::user()->id;
        $transaksi = $this->getTransaksiByUser($id_user, 'desc'); 
        return View::make('transaksi.pembelian', ['menu' => 'transaksi', 'pembelians' => $transaksi]);
    }

    public function pesanan() 
    {   
        $id_user = Auth::user()->id;
        $transaksi = $this->getPesananByUser($id_user, 'desc'); 
        return View::make('transaksi.pesanan', ['menu' => 'transaksi', 'pesanans' => $transaksi]);
    }

    public function email(Request $request, $id_transaksi) 
    {   
        $id_user = Auth::user()->id;
        $transaksi = $this->getTransaksi($request, $id_transaksi); 
        $pemesan = User::find($transaksi->id_user);
        return View::make('transaksi.emailPesan', ['menu' => 'transaksi', 'transaksi' => $transaksi, 'pemesan' => $pemesan]);
    }

    public function batal(Request $request) 
    {   
        $id = $request->id_trans;
        $mode = $request->mode;
        $transaksi = Transaksi::find($id);
        $transaksi->status = 'Dibatalkan';
        $transaksi->alasan_batal = $request->alasan_batal;
        $transaksi->save();

        $item = $transaksi->item;
        $item->stok = $item->stok + $transaksi->jumlah_items;
        $item->save();

        $log = new Log_transaksi;
        $log->id_transaksis = $transaksi->id;
        $log->status = $transaksi->status;
        $log->save();

        $trans = $this->getTransaksi($request, $id); 
        $this->send_mail($trans, 'batal');

        return redirect()->route($mode)
                        ->with('success','Transaksi telah dibatalkan!');
    }

    public function terima(Request $request) 
    {   
        $id = $request->id_trans;
        $transaksi = Transaksi::find($id);
        $transaksi->status = 'Telah Diterima';
        $transaksi->save();

        $log = new Log_transaksi;
        $log->id_transaksis = $transaksi->id;
        $log->status = $transaksi->status;
        $log->save();

        $trans = $this->getTransaksi($request, $id); 
        $this->send_mail($trans, 'terima');

        return redirect()->route('pembelian')
                        ->with('success','Barang telah diterima!');
    }

    public function proses(Request $request) 
    {   
        $id = $request->id_trans;
        $transaksi = Transaksi::find($id);
        $transaksi->status = 'Diproses Penjual';
        $transaksi->save();

        $log = new Log_transaksi;
        $log->id_transaksis = $transaksi->id;
        $log->status = $transaksi->status;
        $log->save();

        $trans = $this->getTransaksi($request, $id); 
        $this->send_mail($trans, 'proses');

        return redirect()->route('pesanan')
                        ->with('success','Transaksi diproses penjual!');
    }

    public function kirim(Request $request) 
    {   
        $id = $request->id_trans;
        $transaksi = Transaksi::find($id);
        $transaksi->status = 'Dalam Pengiriman';
        $transaksi->save();

        $log = new Log_transaksi;
        $log->id_transaksis = $transaksi->id;
        $log->status = $transaksi->status;
        $log->save();

        $trans = $this->getTransaksi($request, $id); 
        $this->send_mail($trans, 'kirim');

        return redirect()->route('pesanan')
                        ->with('success','Barang telah dikirim!');
    }

    public function beliItem(Request $request)
    {        
        $now = date('Y-m-d H:i:s');

        $item = Item::find($request->id_items);
        $user = User::find($request->id_user);

        //DB::transaction(function () {
            
            $transaksi = new Transaksi;
            $transaksi->jenis = 'Pembelian';
            $transaksi->id_user = $request->id_user;
            $transaksi->nama_user = $user->name;
            $transaksi->id_seller = $item->id_seller;
            $transaksi->nama_seller = $item->penjual->name;
            $transaksi->id_items = $request->id_items;
            $transaksi->nama_item = $item->nama;
            $transaksi->jumlah_items = $request->jumlah;
            $transaksi->harga = $item->harga;
            $transaksi->catatan = $request->catatan;
            $transaksi->status = 'Dipesan';
            $transaksi->save();

            $log = new Log_transaksi;
            $log->id_transaksis = $transaksi->id;
            $log->status = $transaksi->status;
            $log->save();

        //});

        $stok = $item->stok;
        $jml = $request->jumlah;

        $item->stok = ($stok - $jml);
        $item->updated_at = $now;
        $item->save();

        $item->foto = explode(';',$item->foto);
        $item->img = $item->foto[0];
        $transaksi->item = $item;
        $transaksi->total = number_format($transaksi->harga * $transaksi->jumlah_items, 0);

        $this->send_mail($transaksi, 'pesan');

        $pesan_wa = urlencode("Assalamu'alaikum, saya pesan *$item->nama* melalui aplikasi PasarDepok.com, mohon difollowup ya \xF0\x9F\x99\x8F \n\nDetilnya dapat dilihat pada menu Pesanan. Terima kasih\n\nPesan dari Aplikasi PasarDepok.com");
        $pesan = 'Barang telah dipesan!<br>E-mail notifikasi pesanan telah dikirim ke penjual!<br>Bila Anda ingin konfirmasi pesanan pada penjual melalui WhatsApp silahkan tekan tombol berikut<br><a href="https://wa.me/62'.$transaksi->penjual->hp.'?text='.$pesan_wa.'" class="btn btn-success mt-2"><img width="25" class="mr-2" src="'.asset('assets/images/wa.png').'">Konfirmasi via Whatsapp</a>';

        return redirect()->route('listShop')
                        ->with('success',$pesan);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */    

    public function getTransaksi(Request $request, $id = null)
    {
        $id_trans = $request->id;
        
        if(empty($id_trans))
            $id_trans = $id;

        $transaksi = Transaksi::find($id_trans);
        $item = Item::find($transaksi->id_items);
        $item->foto = explode(';',$item->foto);
        $item->img = $item->foto[0];
        $transaksi->item = $item;
        $transaksi->total = number_format($transaksi->harga * $transaksi->jumlah_items, 0);

        return $transaksi;
    }

    public function getTransaksiByUser($id, $order = 'asc')
    {
        $transaksi = Transaksi::select('transaksis.*', 'items.foto', 'items.jenis', 'users.name as nama_penjual')
                    ->where('id_user', $id)
                    ->leftJoin('items', 'items.id', '=', 'transaksis.id_items')
                    ->leftJoin('users', 'users.id', '=', 'items.id_seller')
                    ->orderBy('transaksis.id', $order)
                    ->get();

        return $transaksi;
    }

    public function getPesananByUser($id, $order = 'asc')
    {
        $transaksi = Transaksi::select('transaksis.*', 'items.foto', 'items.jenis', 'users.name as nama_penjual')
                    ->where('items.id_seller', $id)
                    ->leftJoin('items', 'items.id', '=', 'transaksis.id_items')
                    ->leftJoin('users', 'users.id', '=', 'items.id_seller')
                    ->orderBy('transaksis.id', $order)
                    ->get();

        return $transaksi;
    }

    public function getJmlPembelian() 
    {
        $user_id = Auth::user()->id;

        $jml_pembelian = Transaksi::where('id_user', '=', $user_id)
                                ->where('status', '!=', 'Telah Diterima')
                                ->where('status', '!=', 'Dibatalkan')
                                ->count();

        return $jml_pembelian;
    }

    public function getJmlPesanan() 
    {
        $user_id = Auth::user()->id;

        $jml_pesanan = Transaksi::where('items.id_seller', '=', $user_id)
                                ->where('status', '!=', 'Telah Diterima')
                                ->where('status', '!=', 'Dibatalkan')
                                ->leftJoin('items', 'items.id', '=', 'transaksis.id_items')
                                ->count();

        return $jml_pesanan;
    }

    public function deleteTransaksi($id)
    {
        Log_transaksi::where('id_transaksis', '=', $id)->delete();
        Transaksi::find($id)->delete();
        return redirect()->route('listTransaksi')
                        ->with('success','Transaksi berhasil dihapus');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'jenis',
                2 =>'nama_item',
                3 =>'nama_user',
                4 =>'harga',
                5 =>'jumlah_items',
                6 =>'created_at',
        );
        //Getting the data
        $transaksis = DB::table ( 'transaksis' )
        ->select ( '*');
        
        $totalData = $transaksis->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on transaksi_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $transaksis->where ( 'nama_item', 'Like', '%' . $searchTerm . '%' )
                            ->orWhere ( 'nama_user', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if(Auth::user()->role_id == 4) {
            $transaksis->where ( 'id_user', Auth::user()->id );
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $transaksis->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $transaksis->count ();
        // Data to client
        $jobs = $transaksis->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $transaksis = $transaksis->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $transaksis as $transaksi ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $transaksi->id;
            $nestedData ['jenis'] = $transaksi->jenis;
            $nestedData ['id_user'] = $transaksi->id_user;
            $nestedData ['id_items'] = $transaksi->id_items;
            $nestedData ['jumlah_items'] = $transaksi->jumlah_items;
            $nestedData ['nama_user'] = $transaksi->nama_user;
            $nestedData ['nama_item'] = $transaksi->nama_item;
            $nestedData ['harga'] = $transaksi->harga;
            $nestedData ['created_at'] = $transaksi->created_at;
            $nestedData ['status'] = $transaksi->status;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );

        return $tableContent;
    }

    public function getBladeExcel()
    {
        $transaksiData = DB::table ( 'transaksis' )
        ->select('*')->get();

        \Excel::create('Riwayat Transaksi', function($excel) use($transaksiData) {

            $excel->sheet('Riwayat Transaksi', function($sheet) use($transaksiData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Jenis',
                    'Nama User',
                    'Nama Barang',
                    'Nama Penjual',
                    'Harga (Rp)',
                    'Jumlah',
                    'Tanggal'
                ];

                $no = 1;
                foreach ($transaksiData as $key => $value) {
                    $excelData[] = [
                        $no++,
                        $value->jenis,
                        $value->nama_user,
                        $value->nama_item,
                        $value->nama_seller,
                        $value->harga,
                        $value->jumlah_items,
                        $value->created_at
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $transaksiData = Transaksi::all();

        //return Response::json($transaksiData);

        $data = json_encode($transaksiData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }

    //public function send_mail(Request $request) { 
    public function send_mail($transaksi, $mode) { 

        $pemesan = $transaksi->pembeli;
        $penjual = User::find($transaksi->item->id_seller);

        try{
            //Mail::send('email', ['nama' => $request->nama, 'pesan' => $request->pesan], function ($message) use ($request)
            if($mode == 'batal') {
                Mail::send('transaksi.emailBatal', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($pemesan)
                {
                    $message->subject('Pesanan Dibatalkan');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($pemesan->email);
                });
                Mail::send('transaksi.emailBatal', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($penjual)
                {
                    $message->subject('Pesanan Dibatalkan');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($penjual->email);
                });
            } else if($mode == 'proses') {
                Mail::send('transaksi.emailProses', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($pemesan)
                {
                    $message->subject('Pesanan Diproses');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($pemesan->email);
                });
                Mail::send('transaksi.emailProses', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($penjual)
                {
                    $message->subject('Pesanan Diproses');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($penjual->email);
                });
            } else if($mode == 'kirim') {
                Mail::send('transaksi.emailKirim', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($pemesan)
                {
                    $message->subject('Barang Telah Dikirim');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($pemesan->email);
                });
                Mail::send('transaksi.emailKirim', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($penjual)
                {
                    $message->subject('Barang Telah Dikirim');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($penjual->email);
                });
            } else if($mode == 'terima') {
                Mail::send('transaksi.emailTerima', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($pemesan)
                {
                    $message->subject('Barang Telah Diterima');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($pemesan->email);
                });
                Mail::send('transaksi.emailTerima', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($penjual)
                {
                    $message->subject('Barang Telah Diterima');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($penjual->email);
                });
            } else if($mode == 'pesan') {
                Mail::send('transaksi.emailPesan', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($pemesan)
                {
                    $message->subject('Barang Telah Dipesan');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($pemesan->email);
                });
                Mail::send('transaksi.emailPesan', ['pemesan' => $pemesan, 'transaksi' => $transaksi], function ($message) use ($penjual)
                {
                    $message->subject('Barang Telah Dipesan');
                    $message->from('admin@pasardepok.com', 'PasarDepok.com');
                    $message->to($penjual->email);
                });
            }
            return back()->with('alert-success','Berhasil Mengirim Email');
        }
        catch (Exception $e){
            return back()->with('alert-danger','Gagal Mengirim Email');
            //return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
    
}
