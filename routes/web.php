<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/privacy', function () {
    return view('auth.privacy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// User -----------------------------------------------------------------------------------

Route::get('addUserPage', 'UsersController@addUserPage')->name('addUserPage');
Route::get('addPage',['as'=>'add.page','uses'=>'UsersController@addUserPage']);
Route::get('editUserPage/{id}',['as'=>'edit.page','uses'=>'UsersController@editUserPage']);
Route::get('viewUserPage/{id}',['as'=>'view.page','uses'=>'UsersController@viewUserPage']);
Route::get('delUser/{id}', ['as'=>'delete.User','uses'=>'UsersController@deleteUser']);
Route::get('get-users', ['as'=>'get.data','uses'=>'UsersController@getData']);
Route::get('get-users1', ['as'=>'get.datas','uses'=>'UsersController@getDatas']);
Route::get('get-excel', ['as'=>'get.excel','uses'=>'UsersController@getBladeExcel']);
Route::get('get-json', ['as'=>'get.json','uses'=>'UsersController@getJson']);
Route::post('addUser', ['as'=>'add.User','uses'=>'UsersController@addUser']);
Route::post('editUser', ['as'=>'edit.User','uses'=>'UsersController@editUser']);
Route::get('profile', ['as'=>'profile','uses'=>'UsersController@profile']);
Route::get('listUser', 'UsersController@listUser')->name('listUser');
Route::get('listPenjual', 'UsersController@listPenjual')->name('listPenjual');
Route::get('get-penjuals', ['as'=>'get.penjuals','uses'=>'UsersController@getPenjuals']);
Route::post('bukaToko', 'UsersController@bukaToko')->name('bukaToko');
Route::get('profilToko', 'UsersController@profilToko')->name('profilToko');
Route::get('get_kelurahan', 'UsersController@getKelurahan')->name('get_kelurahan');

Route::get('getKelurahan', 'Auth\RegisterController@getKelurahan')->name('getKelurahan');

// Role -----------------------------------------------------------------------------------

Route::get('listRole', 'RolesController@listRole')->name('listRole');
Route::get('addRolePage', 'RolesController@addRolePage')->name('addRole.page');
Route::get('editRolePage/{id}',['as'=>'editRole.page','uses'=>'RolesController@editRolePage']);
Route::get('delRole/{id}', ['as'=>'deleteRole.User','uses'=>'RolesController@deleteRole']);
Route::get('get-roles', ['as'=>'getRoles.datas','uses'=>'RolesController@getDatas']);
Route::post('addRole', ['as'=>'add.Role','uses'=>'RolesController@addRole']);
Route::post('editRole', ['as'=>'edit.Role','uses'=>'RolesController@editRole']);

// Jabatan -----------------------------------------------------------------------------------

Route::get('listJabatan', 'JabatansController@listJabatan')->name('listJabatan');
Route::get('addJabatanPage', 'JabatansController@addJabatanPage')->name('addJabatan.page');
Route::get('editJabatanPage/{id}',['as'=>'editJabatan.page','uses'=>'JabatansController@editJabatanPage']);
Route::get('delJabatan/{id}', ['as'=>'deleteJabatan.User','uses'=>'JabatansController@deleteJabatan']);
Route::get('get-jabatans', ['as'=>'getJabatans.datas','uses'=>'JabatansController@getDatas']);
Route::post('addJabatan', ['as'=>'add.Jabatan','uses'=>'JabatansController@addJabatan']);
Route::post('editJabatan', ['as'=>'edit.Jabatan','uses'=>'JabatansController@editJabatan']);

// Item -----------------------------------------------------------------------------------

Route::get('listItem', 'ItemsController@listItem')->name('listItem');
Route::get('addItemPage', 'ItemsController@addItemPage')->name('addItem.page');
Route::get('editItemPage/{id}',['as'=>'editItem.page','uses'=>'ItemsController@editItemPage']);
Route::get('delItem/{id}', ['as'=>'deleteItem.User','uses'=>'ItemsController@deleteItem']);
Route::get('viewItem/{id}', ['as'=>'viewItem','uses'=>'ItemsController@viewItemPage']);
Route::get('get-items', ['as'=>'getItems.datas','uses'=>'ItemsController@getDatas']);
Route::post('addItem', ['as'=>'add.Item','uses'=>'ItemsController@addItem']);
Route::post('editItem', ['as'=>'edit.Item','uses'=>'ItemsController@editItem']);
Route::get('get-excelItem', ['as'=>'get.excelItem','uses'=>'ItemsController@getBladeExcel']);
Route::get('get-jsonItem', ['as'=>'get.jsonItem','uses'=>'ItemsController@getJson']);

Route::get('listShop', 'ItemsController@listShop')->name('listShop');
Route::get('listShopPenjual/{id}', 'ItemsController@listShopPenjual')->name('listShopPenjual/{id}');

Route::post('beliItem', ['as'=>'beli.Item','uses'=>'TransaksisController@beliItem']);

// Transaksi -----------------------------------------------------------------------------------

Route::get('listTransaksi', 'TransaksisController@listTransaksi')->name('listTransaksi');
Route::get('getTransaksi', 'TransaksisController@getTransaksi')->name('getTransaksi');
Route::get('pembelian', 'TransaksisController@pembelian')->name('pembelian');
Route::get('pesanan', 'TransaksisController@pesanan')->name('pesanan');
Route::post('batal', 'TransaksisController@batal')->name('batal');
Route::post('terima', 'TransaksisController@terima')->name('terima');
Route::post('proses', 'TransaksisController@proses')->name('proses');
Route::post('kirim', 'TransaksisController@kirim')->name('kirim');
Route::get('email/{id}', 'TransaksisController@email')->name('email');
Route::get('get-transaksis', ['as'=>'getTransaksi.datas','uses'=>'TransaksisController@getDatas']);
Route::get('get-excelTransaksi', ['as'=>'get.excelTransaksi','uses'=>'TransaksisController@getBladeExcel']);
Route::get('getJmlPembelian', 'TransaksisController@getJmlPembelian')->name('getJmlPembelian');
Route::get('getJmlPesanan', 'TransaksisController@getJmlPesanan')->name('getJmlPesanan');
Route::get('deleteTransaksi/{id}', 'TransaksisController@deleteTransaksi')->name('deleteTransaksi');


Route::get('editPromoPage/{id}', 'PromosisController@editPromoPage')->name('editPromoPage');
Route::get('addPromoPage', 'PromosisController@addPromoPage')->name('addPromoPage');
Route::post('addPromosi', 'PromosisController@addPromosi')->name('addPromosi');
Route::post('editPromosi', 'PromosisController@editPromosi')->name('editPromosi');

// Web Server -----------------------------------------------------------------------------------
Route::get('getAllUser', ['as'=>'getAllUser','uses'=>'ApiController@getAllUser']);
Route::get('makeThumb', 'ItemsController@makeThumb')->name('makeThumb');
Route::get('makeThumbView', 'ItemsController@makeThumbView')->name('makeThumbView');
Route::get('makeThumbUser', 'UsersController@makeThumbUser')->name('makeThumbUser');
Route::get('makeThumbPromo', 'PromosisController@makeThumbPromo')->name('makeThumbPromo');

// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
// Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');

// Kategori -----------------------------------------------------------------------------------

Route::get('listKategori', 'KategorisController@listKategori')->name('listKategori');
Route::get('addKategoriPage', 'KategorisController@addKategoriPage')->name('addKategori.page');
Route::get('editKategoriPage/{id}',['as'=>'editKategori.page','uses'=>'KategorisController@editKategoriPage']);
Route::get('delKategori/{id}', ['as'=>'deleteKategori.User','uses'=>'KategorisController@deleteKategori']);
Route::get('get-kategoris', ['as'=>'getKategoris.datas','uses'=>'KategorisController@getDatas']);
Route::post('addKategori', ['as'=>'add.Kategori','uses'=>'KategorisController@addKategori']);
Route::post('editKategori', ['as'=>'edit.Kategori','uses'=>'KategorisController@editKategori']);